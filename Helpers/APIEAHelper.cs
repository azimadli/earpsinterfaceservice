﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using SAAIDI.Models;
using EARPSInterfaceService.Helpers;
//using Microsoft.Extensions.Hosting;
//using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EARPSInterfaceService.Processors;


namespace EARPSInterfaceService
{
    public static class APIEAHelper
    {
        public static HttpClient ApiEAClient { get; set; }
        public static void InitializeApiEAClient(string atoken)
        {
            ApiEAClient = new HttpClient();
            ApiEAClient.BaseAddress = new Uri(ServiceSettings.apiEAuriBase);
            ApiEAClient.DefaultRequestHeaders.Accept.Clear();
            ApiEAClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            ApiEAClient.DefaultRequestHeaders.Add("authorization", "Bearer " + atoken);

        }
    }
    public static class initEAClientHelper 
    {
        public static HttpClient initEAClient { get; set; }
        public static void InitializeinitClient()
        {
            initEAClient = new HttpClient();
            initEAClient.BaseAddress = new Uri(ServiceSettings.apiEAuriBase);
            initEAClient.DefaultRequestHeaders.Accept.Clear();
            initEAClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

    }

    public  class eaTokens
    {
       public string atokenEA { get; set;}
    }

}
