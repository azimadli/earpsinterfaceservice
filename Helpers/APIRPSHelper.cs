﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using SAAIDILibrary.Models;
using EARPSInterfaceService.Helpers;
//using Microsoft.Extensions.Logging;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService
{
public class APIRPSHelper
    {
        public static HttpClient ApiRPSClient { get; set; }

        //public readonly ILogger<APIRPSHelper> _logger;

        //public APIRPSHelper(ILogger<APIRPSHelper> logger)
        //{
        //    _logger = logger;
        //}

        public static void InitializeApiRPSClient(string atoken, string CustomerName)
        {
            ApiRPSClient = new HttpClient();
            ApiRPSClient.BaseAddress = new Uri(ServiceSettings.apiRPSuriBase);
            ApiRPSClient.DefaultRequestHeaders.Accept.Clear();
            ApiRPSClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            ApiRPSClient.DefaultRequestHeaders.Add("authorization", "Bearer " + atoken);
            ApiRPSClient.DefaultRequestHeaders.Add("X-Customer", CustomerName);

            if (String.Equals((CustomerName).Trim(), "Symphony Industrial AI", StringComparison.OrdinalIgnoreCase) )
            {
                    ApiRPSClient.DefaultRequestHeaders.Add("X-Customer-Db", "symphony_industrial_ai");
            }
            else
            {
                ApiRPSClient.DefaultRequestHeaders.Add("X-Customer-Db", CustomerName.ToLower().Replace(" ", "_"));
            }
            EARPSInterfaceServiceLogger.AddLogEntry($"CustomerName: {CustomerName}",null);

        }

    }
    public static class initRPSClientHelper 
    {
        public static HttpClient initRPSClient { get; set; }
        public static void InitializeinitClient()
        {
            initRPSClient = new HttpClient();
            initRPSClient.BaseAddress = new Uri(ServiceSettings.apiRPSuriBase);
            initRPSClient.DefaultRequestHeaders.Accept.Clear();
            initRPSClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

    }
    public class rpsTokens
    {
        public string atokenRPS { get; set; }
    }
}
