﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Odbc;
//using Microsoft.Extensions.Caching.Memory;
using SAAIDI.Models;
using SAAIDI.Helpers;
using EARPSInterfaceService.Helpers;
//using Microsoft.Extensions.Logging;
using EARPSInterfaceService.Logger;
using  SAAIDILibrary.Models;
using System.IO;
using System.Text.RegularExpressions;

namespace EARPSInterfaceService.Helpers
{
   
    public class Helper
    {
        public static Dictionary<K, V> CreateDictionaryFor<K, V>(K key, V value)
        {
            return new Dictionary<K, V>();
        }

        public static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
        public static string RemoveSpecialCharacters(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }
            string my_String = s.Replace(".", "_");
            my_String = Regex.Replace(my_String, @"[^\w\d\s-]", "");
            return my_String;
        }

        private static string _dbKey = string.Empty;
        const string PUBLIC_TOKEN = "e244e56b-71bb-4b16-a6a0-f192fd018912";

        /// <summary>
        /// encode binary data into a string so that it can be sent over 
        /// </summary>
        /// <param name="inputBytes"></param>
        /// <returns></returns>
        public static string Base64EncodeBytes(byte[] inputBytes)
        {
            //a byte[] may be encoded to a char[] equivalent, and the char[] can then be converted to a string: 
            // Each 3-byte sequence in inputBytes must be converted to a 4-byte 
            // sequence 
            long arrLength = (long)(4.0d * inputBytes.Length / 3.0d);
            if ((arrLength % 4) != 0)
            {
                // increment the array length to the next multiple of 4 
                //    if it is not already divisible by 4
                arrLength += 4 - (arrLength % 4);
            }
            char[] encodedCharArray = new char[arrLength];
            Convert.ToBase64CharArray(inputBytes, 0, inputBytes.Length, encodedCharArray, 0);

            return (new string(encodedCharArray));
        }

    }

    public class DebuggingHelper 
    { 
        public static void WriteJsonToFile(string serializedstring, string filedir, string filename)
        {
            Boolean writefile = true;
            if (writefile)
            {
                try
                {
                    Directory.CreateDirectory(@"jsonfiles\" + filedir);
                    File.WriteAllText(@"jsonfiles\" + filedir + @"\" + filename + ".json", serializedstring);
                    //JsonSerializer.Serialize(JsonSerializer.Serialize(location), new JsonSerializerOptions { WriteIndented = true }));
                }
                catch (Exception ex)
                {
                    throw new Exception("Unable to write json to file", ex);
                }
            }
        }

        public static string Changelevelname(string existinglevel, string levelpart, string levelaction)
        {
            string newlevel = "";
            if (levelaction == "add")
            {
                newlevel = existinglevel + levelpart;
            };
            if (levelaction == "remove")
            {
                newlevel = existinglevel.Remove(existinglevel.LastIndexOf(@"\"));
            }
            return newlevel;
        }
    }
}

