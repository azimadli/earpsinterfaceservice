﻿using System;
using System.Collections.Generic;
using System.Text;
using NLog;

namespace EARPSInterfaceService.Helpers
{
    public interface ILoggerManager
    {
        void LogInfo(string message);
        void LogWarn(string message);
        void LogDebug(string message);
        void LogError(string message);
    }

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public class LoggerManager : ILoggerManager
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();
        public void LogDebug(string message)
        {
            logger.Debug(message);
        }
        public void LogError(string message)
        {
            logger.Error(message);
        }
        public void LogInfo(string message)
        {
            logger.Info(message);
        }
        public void LogWarn(string message)
        {
            logger.Warn(message);
        }
    }
}
