﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EARPSInterfaceService.Helpers
{
    class ServiceSettings
    {
        public static Guid TenantID { get; set; }
        public static string CustomerName { get; set; }
        public static Guid PlantUUID { get; set; }
        public static string PlantName { get; set; }
        public static Guid JobID { get; set; }
        public static string apiEAuriBase { get; set; }
        public static string apiEAuriBase2 { get; set; }
        public static string apiRPSuriBase { get; set; }
        public static string apiRPSuriBase2 { get; set; }
        public static int delayingMaxMinutes { get; set; }
        public static int JobStatus { get; set; }
        public static string azureBlogContainer { get; set; }
        public static string azureBlogAccountKey { get; set; }

    }

}
