﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EARPSInterfaceService.Helpers
{
    public class WorkerOptions
    {
        public string apiEAuriBase { get; set; }
        public string apiEAuriBase2 { get; set; }
        public string apiRPSuriBase { get; set; }
        public string apiRPSuriBase2 { get; set; }
        public int delayingMaxMinutes { get; set; }
        public string azureBlogContainer { get; set; }
        public string azureBlogAccountKey { get; set; }
    }
}
