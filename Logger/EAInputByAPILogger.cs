﻿//using Microsoft.Extensions.Logging;
using NLog.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EARPSInterfaceService.Logger
{
    public static class EARPSInterfaceServiceLogger
    {
        public static NLog.Logger logger= NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
        public static void AddLogEntry(string info,Exception exception)
        {
            if(info != string.Empty)
                logger.Info(info, "Log information details");

            Console.WriteLine($"{info}");
            if (exception != null)
            {
                string exceptionMessage = !string.IsNullOrEmpty(exception.Message) ? string.Concat("Exception message : ", exception.Message.ToString()) : string.Empty;
                string innerexceptionMessage = exception.InnerException != null ? !string.IsNullOrEmpty(exception.InnerException.Message) ? string.Concat("Inner exception : ", exception.InnerException.Message.ToString()) : string.Empty : string.Empty;
                string stacktraceMessage = !string.IsNullOrEmpty(exception.StackTrace) ? string.Concat("StackTrace message : ", exception.StackTrace.ToString()) : string.Empty;
                logger.Error(string.Concat("Exception message : ", exceptionMessage + innerexceptionMessage + stacktraceMessage), "Log error details");
            }
        }
    }
}
