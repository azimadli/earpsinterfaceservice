﻿using System;
using System.Collections.Generic;
using System.Text;
using SAAIDILibrary.Models;

namespace EARPSInterfaceService.Models
{
    public class AreaResponse
    {
        public int statuscode { get; set; }
        public string message { get; set; }
        public SAAIDILibrary.Models.Area data { get; set; }
    }
}
