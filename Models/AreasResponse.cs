﻿using System;
using System.Collections.Generic;
using System.Text;
using SAAIDILibrary.Models;

namespace EARPSInterfaceService.Models
{
    public class AreasResponse
    {
        public int statuscode { get; set; }
        public string message { get; set; }
        public List<AreaEA> data { get; set; }
    }

    public class AreaEA
    {
        public SAAIDILibrary.Models.Area data { get; set; }
    }
}
