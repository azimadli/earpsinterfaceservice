﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EARPSInterfaceService.Models
{
    public class ComponentInfo
    {
        public string componentstuff { get; set; }
        public string Aop { get; set; } //boolean
        public Boolean Aop_boolean { get; set; } //boolean
        public string CCcpl { get; set; }
        public string closeCoupledType { get; set; }
        public int closeCoupledLocations_int { get; set; }
        public string closeCoupledDriven { get; set; }
        public Boolean closeCoupleddrivenBallBearings_boolean { get; set; } // boolean
        public string couplingLocation { get; set; } //int
        public string cpl1 { get; set; }
        public double cpl1rationumerator { get; set; }
        public double cpl1ratiodenominator { get; set; }
        public string cpl1speedchange { get; set; }
        public string cpl2 { get; set; }
        public double cpl2rationumerator { get; set; }
        public double cpl2ratiodenominator { get; set; }
        public string cpl2speedchange { get; set; }
        public string cyl { get; set; } //int
        public int cyl_int { get; set; } //int
        public string DEOrientation { get; set; }
        public string DEOrientation_brg_1 { get; set; }
        public string DEOrientation_brg_2 { get; set; }
        public string DEOrientation_brg_3 { get; set; }
        public string DEOrientation_brg_4 { get; set; }
        public string DEOrientation_brg_5 { get; set; }
        public string DEOrientation_brg_6 { get; set; }
        public string DEOrientation_brg_7 { get; set; }
        public string DEOrientation_brg_8 { get; set; }
        public string DEOrientation_brg_9 { get; set; }
        public string DEOrientation_brg_10 { get; set; }
        public string drive { get; set; }
        public string drivenbal { get; set; } //boolean
        public Boolean drivenbal_boolean { get; set; } //boolean
        public string drivenbb { get; set; } //boolean
        public Boolean drivenbb_boolean { get; set; } //boolean
        public string drivenbrgs { get; set; }
        public string drivenc { get; set; }
        public string drivencextra { get; set; }
        public string driven { get; set; }
        public string Driven { get; set; } //fix duplicate
        public string Driven_DEOrientation { get; set; }
        public string Driven_NDEOrientation { get; set; }
        public string Driven_locations { get; set; } //int
        public int Driven_locations_int { get; set; } //int
        public string Driven_locationType { get; set; } // fix duplicate
        public string driven_locationType { get; set; }
        public string Driven_Threads { get; set; } //int
        public int Driven_Threads_int { get; set; } //int
        public string drivenDEOrientation { get; set; }
        public string Driver { get; set; }
        public string Driver_DEOrientation { get; set; }
        public string Driver_NDEOrientation { get; set; }
        public string Driver_locations { get; set; } //int
        public int Driver_locations_int { get; set; } //int
        public string Driver_locationType { get; set; }
        public string DriverTbrg { get; set; } // actually a driven answer - should be DrivenTbrg
        public string exc { get; set; } //boolean
        public Boolean exc_boolean { get; set; } //boolean
        public string excOH { get; set; }
        public string fanBlades { get; set; } //int
        public int fanBlades_int { get; set; } //int
        public string gearbox { get; set; }
        public string gendriver { get; set; }
        public string Idler_threads { get; set; } //int
        public int Idler_threads_int { get; set; } //int
        public string intermediate { get; set; }
        public string ISC { get; set; } //int
        public int ISC_int { get; set; } //int
        public string isCloseCoupled { get; set; } //boolean
        public Boolean isCloseCoupled_boolean { get; set; } //boolean
        public string LOC1Orientation { get; set; }
        public string LOC2Orientation { get; set; }
        public string LOC3Orientation { get; set; }
        public string LOC4Orientation { get; set; }
        public string location_1 { get; set; }
        public string location_2 { get; set; }
        public string location_3 { get; set; }
        public string location_4 { get; set; }
        public double LOC1rationumerator { get; set; }
        public double LOC2rationumerator { get; set; }
        public double LOC3rationumerator { get; set; }
        public double LOC4rationumerator { get; set; }
        public double LOC1ratiodenominator { get; set; }
        public double LOC2ratiodenominator { get; set; }
        public double LOC3ratiodenominator { get; set; }
        public double LOC4ratiodenominator { get; set; }
        public string LOC1speedchange { get; set; }
        public string LOC2speedchange { get; set; }
        public string LOC3speedchange { get; set; }
        public string LOC4speedchange { get; set; }
        public string motorbars { get; set; } //int
        public int motorbars_int { get; set; } //int
        public string motorbb { get; set; } // boolean
        public Boolean motorbb_boolean { get; set; } // boolean
        public string motorfan { get; set; } //boolean
        public Boolean motorfan_boolean { get; set; } //boolean
        public string motorpoles { get; set; } //int
        public int motorpoles_int { get; set; } //int
        public string motorspeed { get; set; } //float
        public float motorspeed_float { get; set; } //float
        public string speed { get; set; } //float
        public string driverspeed { get; set; } //float
        public float driverspeed_float { get; set; } //float
        public string NDEOrientation_brg_1 { get; set; }
        public string NDEOrientation_brg_2 { get; set; }
        public string NDEOrientation_brg_3 { get; set; }
        public string NDEOrientation_brg_4 { get; set; }
        public string NDEOrientation_brg_5 { get; set; }
        public string NDEOrientation_brg_6 { get; set; }
        public string NDEOrientation_brg_7 { get; set; }
        public string NDEOrientation_brg_8 { get; set; }
        public string NDEOrientation_brg_9 { get; set; }
        public string NDEOrientation_brg_10 { get; set; }
        public string NOL { get; set; } //int
        public int NOL_int { get; set; } //int
        public string pistons { get; set; } //pistons
        public int pistons_int { get; set; } //pistons
        public string propellerBlades { get; set; } //int
        public int propellerBlades_int { get; set; } //int
        public string ratios { get; set; } // decimal
        public decimal ratios_decimal { get; set; } // decimal
        public string rotorOH { get; set; } //boolean
        public Boolean rotorOH_boolean { get; set; } //boolean
        public string redgear { get; set; } //boolean
        public Boolean redgear_boolean { get; set; } //boolean
        public string turbinebb { get; set; } //boolean
        public Boolean turbinebb_boolean { get; set; } //boolean
        public string turbinesupported { get; set; } //boolean
        public Boolean turbinesupported_boolean { get; set; } //boolean
        public string turbineTbrg { get; set; } //boolean
        public Boolean turbineTbrg_boolean { get; set; } //boolean
        public string Tbrgbb { get; set; } //boolean
        public Boolean Tbrgbb_boolean { get; set; } //boolean
        public string vanes { get; set; } //int
        public int vanes_int { get; set; } //int
        public string input_lobes { get; set; } //int
        public int input_lobes_int { get; set; } //int
        public string Idler_lobes { get; set; } //int
        public int Idler_lobes_int { get; set; } //int
        public List<OrientationBRG> orientationbrgs { get; set; }
    }
    public class OrientationBRG
    {
        public int orientationbrgKey { get; set; }
        public string DEOrientation_brg { get; set; }
        public string NDEOrientation_brg { get; set; }
    }

}
