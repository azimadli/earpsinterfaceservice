﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EARPSInterfaceService.Models
{
    public class ComponentLocations
    {
        public int componentLocationsLine { get; set; }
        public string componentType { get; set; } // Driver, Intermediate, Driven
        public string componentTypeSub { get; set; } // Motor, Gearbox, Pump, etc.
        public string componentTypeSub2 { get; set; } // Centrifugal, Screw, Overhung Rotor, etx.
        public string speedchangedirection { get; set; } // increase or decrease
        public int speedchangenumberoftimes { get; set; } // 1 through 4
        public int location { get; set; } // 1 through 4
        public string locationlorkey1 { get; set; } // A, B, C, etc.
        public string locationlorkey2 { get; set; } // A, B, C, etc.
        public string locationlorkey3 { get; set; } // A, B, C, etc.
        public string locationlorkey4 { get; set; } // A, B, C, etc.
    }
}
