﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace EARPSInterfaceService.Models
{
    public class ComponentPickupCodesInCompRec
    {
        public short PickupOne { get; set; }
        public short PickupTwo { get; set; }
        public short PickupThree { get; set; }
        public short PickupFour { get; set; }
    }

}
