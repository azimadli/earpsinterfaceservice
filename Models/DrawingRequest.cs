﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SAAIDI.Helpers;

namespace EARPSInterfaceService.Models
{
    public class DrawingRequest
    {
        public int drawingID { get; set; }
        public Guid drawingUUID { get; set; }
        public string drawingName { get; set; }
        public short drawingType { get; set; }
        public string originalFiletype { get; set; }
        public string originalFileName { get; set; }
        public int drawingSize { get; set; }
        public string drawingimage { get; set; }
        public int createdBySiteID { get; set; }
        public int modifiedBySiteID { get; set; }
        public DateTime recordLastUpdated { get; set; }
    }
}
