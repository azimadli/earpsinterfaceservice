﻿using System;
using System.Collections.Generic;
using System.Text;
using SAAIDILibrary.Models;

namespace EARPSInterfaceService.Models
{
    public class GetIDFromGuidRequest
    {
        public Guid assetUUID { get; set; }
        public int assetTypeID { get; set; }
     }
}
