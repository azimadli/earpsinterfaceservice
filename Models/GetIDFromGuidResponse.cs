﻿using System;
using System.Collections.Generic;
using System.Text;
using SAAIDILibrary.Models;

namespace EARPSInterfaceService.Models
{
    public class GetIDFromGuidResponse
    {
        public int assetID { get; set; }
        public Guid assetUUID { get; set; }
        public int assetTypeID { get; set; }
        public string assetTypeDescription { get; set; }
        public Boolean uuidExists { get; set; }
    }
}
