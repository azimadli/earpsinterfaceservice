﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace EARPSInterfaceService.Models
{
    public class JobInfo
    {
        public Guid JobID { get; set; }
        public string CustomerName { get; set; }
        public Guid TenantID { get; set; }
        public string PlantName { get; set; }
        public Guid PlantUUID { get; set; }
        public string Type { get; set; }
    }

}
