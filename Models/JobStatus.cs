﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace EARPSInterfaceService.Models
{
    public class JobStatus
    {
        public Guid plantID { get; set; }
        public Guid jobId { get; set; }
        public string exportStatus { get; set; }
    }

}
