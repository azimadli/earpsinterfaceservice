﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EARPSInterfaceService.Models
{
    public class LocationLOR
    {
        public string lorKey { get; set; }
        public double lowrangefactorFMAX { get; set; }
        public double lowrangeFMAX { get; set; }
        public double lowrangefactorLOR { get; set; }
        public int lowrangeLOR { get; set; }
        public double highrangefactorFMAX { get; set; }
        public double highrangeFMAX { get; set; }
        public double highrangefactorLOR { get; set; }
        public int highrangeLOR { get; set; }
        public double demodlowrangefactorFMAX { get; set; }
        public double demodlowrangeFMAX { get; set; }
        public double demodlowrangefactorLOR { get; set; }
        public int demodlowrangeLOR { get; set; }

    }
}
