﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EARPSInterfaceService.Models
{
    public class LocationRatios
    {
        public int location { get; set; }
        public double numerator { get; set; }
        public double denominator { get; set; }
        public string speedchange { get; set; }
    }
}
