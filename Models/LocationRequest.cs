﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SAAIDI.Helpers;

namespace EARPSInterfaceService.Models
{
    public class LocationReqest
    {
        public Guid LocationUUID { get; set; }
        public Guid MachineUUID { get; set; }
        public string LocationName { get; set; }
        public float LocationRPM { get; set; } = 0;
        /// <summary>
        /// The bearing poisition
        /// </summary>
        public short PositionNumber { get; set; } = 1;
        public string Orientation { get; set; }

        public string UnitLabel { get; set; }
        /// <summary>
        /// Low Range Lines Of Resolution
        /// </summary>
        public int LowRangeLOR { get; set; }
        /// <summary>
        /// High Range Lines Of Resolution
        /// </summary>
        public int HighRangeLOR { get; set; }
        /// <summary>
        /// Demod Low Range Lines Of Resolution
        /// </summary>
        public int DemodLowRangeLOR { get; set; }
        public int LineFrequency { get; set; }
    }
}
