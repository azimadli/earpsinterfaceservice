﻿using System;
using System.Collections.Generic;
using System.Text;
using SAAIDILibrary.Models;

namespace EARPSInterfaceService.Models
{
    public class LocationResponse
    {
        public int statuscode { get; set; }
        public string message { get; set; }
        public SAAIDILibrary.Models.Location data { get; set; }
    }
}
