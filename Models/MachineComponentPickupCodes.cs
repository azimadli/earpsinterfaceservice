﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace EARPSInterfaceService.Models
{
    public class machinelocs
    {
        public string drvrpickupcode { get; set; }
        public short drvrpickup1 { get; set; }
        public short drvrpickup2 { get; set; }
        public short drvrpickup3 { get; set; }
        public short drvrpickup4 { get; set; }

        public short drvr_loc_01 { get; set; }
        public short drvr_loc_02 { get; set; }
        public short drvr_loc_extra { get; set; }

        public string cpl1pickupcode { get; set; }
        public short cpl1pickup1 { get; set; }
        public short cpl1pickup2 { get; set; }
        public short cpl1pickup3 { get; set; }
        public short cpl1pickup4 { get; set; }

        public short cpl1_loc_01 { get; set; }
        public short cpl1_loc_02 { get; set; }

        public string intmpickupcode { get; set; }
        public short intmpickup1 { get; set; }
        public short intmpickup2 { get; set; }
        public short intmpickup3 { get; set; }
        public short intmpickup4 { get; set; }

        public short intm_loc_01 { get; set; }
        public short intm_loc_02 { get; set; }
        public short intm_loc_03 { get; set; }
        public short intm_loc_04 { get; set; }

        public string cpl2pickupcode { get; set; }
        public short cpl2pickup1 { get; set; }
        public short cpl2pickup2 { get; set; }
        public short cpl2pickup3 { get; set; }
        public short cpl2pickup4 { get; set; }

        public short cpl2_loc_01 { get; set; }
        public short cpl2_loc_02 { get; set; }

        public string drvnpickupcode { get; set; }
        public short drvnpickup1 { get; set; }
        public short drvnpickup2 { get; set; }
        public short drvnpickup3 { get; set; }
        public short drvnpickup4 { get; set; }

        public short drvn_loc_01 { get; set; }
        public short drvn_loc_02 { get; set; }
        public short drvn_loc_extra { get; set; }

        public short lastlocvalueUsed { get; set; } = 0;
        public short firstlocvalueUseddrvr { get; set; } = 0;
        public short lastlocvalueUseddrvr { get; set; } = 0;
        public short firstlocvalueUsedintm { get; set; } = 0;
        public short lastlocvalueUsedintm { get; set; } = 0;
        public short firstlocvalueUseddrvn { get; set; } = 0;
        public short lastlocvalueUseddrvn { get; set; } = 0;
    }
}
