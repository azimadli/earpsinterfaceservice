﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EARPSInterfaceService.Models
{
    public class MachineComponents
    {
        public short ComponentOrder { get; set; }
        public decimal ComponentCode { get; set; }
        public short PickupOne { get; set; }
        public string PickupOneString { get; set; }
        public short PickupTwo { get; set; }
        public string PickupTwoString { get; set; }
        public short PickupThree { get; set; }
        public string PickupThreeString { get; set; }
        public short PickupFour { get; set; }
        public string PickupFourString { get; set; }
        public int Pickupscount { get; set; }
        public string ComponentType { get; set; }
        public string ComponentSubType { get; set; }
        public int CouplingPosition { get; set; }
        public string CoupledComponentType1 { get; set; }
        public string CoupledComponentType2 { get; set; }
    }
}
