﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EARPSInterfaceService.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class PullPlant
    {
        public Guid id { get; set; } // jcm 20220317
        //public int ea_plantID { get; set; }
        public string name { get; set; }
        public string rbacId { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; }
        public int serialNumber { get; set; }
        public int version { get; set; }
        public Customer customer { get; set; }
        public int lineFrequency { get; set; }
        public string sensorType { get; set; }
        public int midStart { get; set; }
        public List<Area> areas { get; set; }
        public bool exported { get; set; }
        public bool reviewCompleted { get; set; }
        public bool plantLocked { get; set; }
        public bool conflicted { get; set; }
        public bool importCSV { get; set; }
    }

    public class Customer
    {
        public Guid id { get; set; } // jcm 20220317
        //public int ea_companyID { get; set; }
        public string name { get; set; }
        public string rbacId { get; set; }
        public int serialNumber { get; set; }
        public int version { get; set; }
        public int dbversion { get; set; }
        public bool dbInitialized { get; set; }
        public bool importCSV { get; set; }
    }

    public class Plant
    {
        public Guid id { get; set; }//jcm 20220317
        //public int ea_plantID { get; set; }
        public string name { get; set; }
        public string rbacId { get; set; }
        public int serialNumber { get; set; }
        public int version { get; set; }
        public int lineFrequency { get; set; }
        public int midstart { get; set; }
        public bool exported { get; set; }
        public bool reviewCompleted { get; set; }
        public bool plantLocked { get; set; }
        public bool conflicted { get; set; }
        public bool importCSV { get; set; }
        public string sensorType { get; set; } // not in json
    }

    public class Picklist
    {
        public string id { get; set; }
        public string name { get; set; }
        public bool importCSV { get; set; }
    }

    public class Manufacturer
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool importCSV { get; set; }
//        public Picklist picklist { get; set; } // not in json
    }

    public class Attachments
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; }
        public int serialNumber { get; set; }
        public int version { get; set; }
        public string type { get; set; }
        public string uri { get; set; }
        public string downStatus { get; set; }
        public decimal height { get; set; }
        public decimal width { get; set; }
        public string mimeType { get; set; }
        public int size { get; set; }
        public bool importCSV { get; set; }
    }

    public class Driver
    {
        public string id { get; set; }
        public Attachments attachments { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; } // not in json
        public int serialNumber { get; set; }
        public int version { get; set; }
        public Manufacturer manufacturer { get; set; }
        public string techManual { get; set; } // not in json
        public string model { get; set; } // not in json
        public double rpm { get; set; }
        public double hp { get; set; }
        public string ndeBrg { get; set; }
        public string deBrg { get; set; }
        public string driverFrame { get; set; } // not in json
        public Machine machine { get; set; }
        public bool importCSV { get; set; }
    }

    public class Intermediate
    {
        public string id { get; set; }
        public Attachments attachments { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; } // not in model
        public int serialNumber { get; set; }
        public int version { get; set; }
        public Manufacturer manufacturer { get; set; }
        public string techManual { get; set; } // not in model
        public string model { get; set; }
        public string ratio { get; set; }
        public bool increaser { get; set; }
        public decimal beltLength { get; set; }
        public Machine machine { get; set; }
        public decimal d1 { get; set; }
        public decimal d2 { get; set; }
        public decimal l { get; set; }
        public bool importCSV { get; set; }
    }

    public class Gearbox
    {
        public string id { get; set; }
        public Attachments attachments { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; } // not in model
        public int serialNumber { get; set; }
        public int version { get; set; }
        public Manufacturer manufacturer { get; set; }
        public string techManual { get; set; } // not in model
        public string model { get; set; }
        public string ratio { get; set; }
        public bool increaser { get; set; }
        public decimal beltLength { get; set; }
        public Machine machine { get; set; }
        public decimal d1 { get; set; }
        public decimal d2 { get; set; }
        public decimal l { get; set; }
        public bool importCSV { get; set; }
    }

    public class Driven
    {
        public string id { get; set; }
        public Attachments attachments { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; } // not in model
        public int serialNumber { get; set; }
        public int version { get; set; }
        public Manufacturer manufacturer { get; set; }
        public string techManual { get; set; } // not in model
        public string model { get; set; }
        public double rpmOutput { get; set; }
        public Machine machine { get; set; }
        public bool importCSV { get; set; }
    }

    public class MachineComponentMap
    {
        public string id { get; set; }
        public int serialNumber { get; set; }
        public int version { get; set; }
        public string key { get; set; }
        public string value { get; set; }
        public Machine machine { get; set; }
        public bool importCSV { get; set; }
    }

    public class Area2
    {
        public string id { get; set; }
        public string name { get; set; }
        public string rbacId { get; set; }
        public int serialNumber { get; set; }
        public int version { get; set; }
        public int lineFrequency { get; set; }
        public bool conflicted { get; set; }
        public bool importCSV { get; set; }
    }

    public class Machine
    {
        public Guid id { get; set; } // jcm 20220317
        //public int ea_machineID { get; set; }
        public string name { get; set; }
        public string rbacId { get; set; }
        public List<Attachments> attachments { get; set; } // not in json
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; } // not in json
        public int serialNumber { get; set; }
        public int version { get; set; }
        public string assetId { get; set; }
        public bool guard { get; set; }
        public bool modify { get; set; }
        public bool elevated { get; set; }
        public bool ppe { get; set; }
        public bool ooc { get; set; }
        public bool remote { get; set; }
        public string machineOrientation { get; set; }
        public bool approved { get; set; }
        public bool superView { get; set; }
        public bool synched { get; set; }
        public bool driverVfd { get; set; }
        public bool hasBelt { get; set; }
        public bool hasGbx { get; set; }
        public Driver driver { get; set; }
        //public Intermediate intermediate { get; set; } // not in json
        public Gearbox gearbox { get; set; }
        public Driven driven { get; set; }
        public Foundation foundation { get; set; }
        public Environment environment { get; set; }
        public string comment { get; set; }
        public List<MachineComponentMap> machineComponentMap { get; set; }
        public bool imported { get; set; }
        public string sensorType { get; set; }
        public string cmmsId { get; set; }
        public bool manualMIDBuild { get; set; }
        public List<LocationsInformation> locationsInformation { get; set; }
        public Area area { get; set; }
        public bool conflicted { get; set; }
        public bool importCSV { get; set; }

    }

    public class LocationsInformation
    {
        public Guid id { get; set; } // jcm 20220317
        //public int ea_locationID { get; set; }
        public int serialNumber { get; set; }
        public int version { get; set; }
        public string locationType { get; set; }
        public string position { get; set; }
        public string brgNo { get; set; }
        public string orientation { get; set; }
        public string componentType { get; set; }
        public bool scanned { get; set; }
        public string sensorSerialNumber { get; set; }
        public Machine machine { get; set; }
        public bool importCSV { get; set; }
        //public string ea_locationname { get; set; } //jcm 20220317

    }

    public class Foundation
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool importCSV { get; set; }
    }

    public class Environment
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool importCSV { get; set; }
    }

    public class Area
    {
        public Guid id  { get; set; } //jcm 20220317
        //public int ea_areaID { get; set; }
        public string name { get; set; }
        public string rbacId { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; } // not in json
        public int serialNumber { get; set; }
        public int version { get; set; }
        public int lineFrequency { get; set; }
        public string sensorType { get; set; }
        public Plant plant { get; set; }
        public List<Machine> machines { get; set; }
        public bool conflicted { get; set; }
        public bool importCSV { get; set; }

    }

}
