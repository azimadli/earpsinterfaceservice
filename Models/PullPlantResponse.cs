﻿using System;
using System.Collections.Generic;
using System.Text;
using SAAIDILibrary.Models;

namespace EARPSInterfaceService.Models
{
    public class PullPlantResponse
    {
        public int status { get; set; }
        public string message { get; set; }
    }
}
