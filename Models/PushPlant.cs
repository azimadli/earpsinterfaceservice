﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EARPSInterfaceService.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class PushPlant
    {
        public Guid id { get; set; } // jcm 20220317
        public string name { get; set; }
        //public int lineFrequency { get; set; }
        public string sensorType { get; set; }
        public int midStart { get; set; }
        public string title { get; set; }
        public List<AreaPush> areas { get; set; }
    }

    public class CustomerPush
    {
        public Guid id { get; set; } // jcm 20220317
        public string name { get; set; }
        public string rbacId { get; set; }
        public int serialNumber { get; set; }
        public int version { get; set; }
        public int dbversion { get; set; }
        public bool dbInitialized { get; set; }
        public bool importCSV { get; set; }
    }

    public class PlantPush
    {
        public Guid id { get; set; }//jcm 20220317
        public string name { get; set; }
        public int midstart { get; set; }
        public string sensorType { get; set; } // not in json
    }

    public class ManufacturerPush
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool importCSV { get; set; }
    }

    public class AttachmentsPush
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; }
        public int serialNumber { get; set; }
        public int version { get; set; }
        public string type { get; set; }
        public string uri { get; set; }
        public string downStatus { get; set; }
        public decimal height { get; set; }
        public decimal width { get; set; }
        public string mimeType { get; set; }
        public int size { get; set; }
        public bool importCSV { get; set; }
    }

    public class DriverPush
    {
        public string id { get; set; }
        //public AttachmentsPush attachments { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; } // not in json
        public int serialNumber { get; set; }
        public int version { get; set; }
        public ManufacturerPush manufacturer { get; set; }
        public string techManual { get; set; } // not in json
        public string model { get; set; } // not in json
        public double rpm { get; set; }
        public double hp { get; set; }
        public string ndeBrg { get; set; }
        public string deBrg { get; set; }
        public string driverFrame { get; set; } // not in json
        //public MachinePush machine { get; set; }
        public bool importCSV { get; set; }
    }

    public class GearboxPush
    {
        public string id { get; set; }
        //public AttachmentsPush attachments { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; } // not in model
        public int serialNumber { get; set; }
        public int version { get; set; }
        public ManufacturerPush manufacturer { get; set; }
        public string techManual { get; set; } // not in model
        public string model { get; set; }
        public string ratio { get; set; }
        public bool increaser { get; set; }
        public decimal beltLength { get; set; }
        //public Machine machine { get; set; }
        public decimal d1 { get; set; }
        public decimal d2 { get; set; }
        public decimal l { get; set; }
        public bool importCSV { get; set; }
    }

    public class DrivenPush
    {
        public string id { get; set; }
        //public AttachmentsPush attachments { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; } // not in model
        public int serialNumber { get; set; }
        public int version { get; set; }
        public ManufacturerPush manufacturer { get; set; }
        public string techManual { get; set; } // not in model
        public string model { get; set; }
        public double rpmOutput { get; set; }
        //public MachinePush machine { get; set; }
        public bool importCSV { get; set; }
    }

    public class MachineComponentMapPush
    {
        public string id { get; set; }
        public int serialNumber { get; set; }
        public int version { get; set; }
        public string key { get; set; }
        public string value { get; set; }
        //public MachinePush machine { get; set; }
        public bool importCSV { get; set; }
    }

    public class MachinePush
    {
        public Guid id { get; set; } // jcm 20220317
        //public int ea_machineID { get; set; }
        public string name { get; set; }
        //public string rbacId { get; set; }
        //public List<AttachmentsPush> attachments { get; set; } // not in json
        public string assetId { get; set; }
        public string cmmsId { get; set; }
        public string comment { get; set; }
        public Boolean conflicted { get; set; }
        public bool elevated { get; set; }
        public bool guard { get; set; }
        public bool ooc { get; set; }
        public string machineOrientation { get; set; }
        public bool manualMIDBuild { get; set; }
        public bool ppe { get; set; }
        public string sensorType { get; set; }
        //public bool approved { get; set; }
        //public bool hasGbx { get; set; }
        public FoundationPush foundation { get; set; }
        public EnvironmentPush environment { get; set; }
        public DriverPush driver { get; set; }
        public GearboxPush gearbox { get; set; }
        public DrivenPush driven { get; set; }
        public List<LocationsInformationPush> locationsInformation { get; set; }
        //public List<MachineComponentMapPush> machineComponentMap { get; set; }
        //public Area area { get; set; }
        //public bool importCSV { get; set; }
    }

    public class LocationsInformationPush
    {
        public Guid id { get; set; } // jcm 20220317
        public string locationType { get; set; }
        public string position { get; set; }
        public string brgNo { get; set; }
        public string orientation { get; set; }
        public string componentType { get; set; }
        public bool scanned { get; set; }
        public string sensorSerialNumber { get; set; }
        //public Machine machine { get; set; }
        //public string ea_locationname { get; set; } //jcm 20220317

    }

    public class FoundationPush
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class EnvironmentPush
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class AreaPush
    {
        public Guid id  { get; set; } //jcm 20220317
        public string name { get; set; }
        public string sensorType { get; set; }
        public List<MachinePush> machines { get; set; }
        //public bool conflicted { get; set; }

    }

}
