﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDILibrary.Models;
using EARPSInterfaceService.Logger;
using MIDCodeGenerationLibrary.Models;

namespace EARPSInterfaceService.Processors
{
    public class CreateLocationToPost
    {
        public static async Task<HttpResponseMessage> BuildtoPostLocation(
            List<ComponentLocations> Comploc,
            List<LocationLOR> LocLOR,
            string comptype, 
            string compsubtype, 
            string compsubtype2, 
            string locationtype,
            double locationrpm,
            string speedchange,
            int numberofspeedchanges,
            short LocationNo,
            short PositionNo,
            string Orientation,
            Guid machineUUID, 
            string machineName, 
            Guid tenantID,
            string PlantUnits, 
            int PlantLineFrequency, 
            machinelocs componentpickups, 
            string levelname,
            string levelnametop)
        {
            //machineMID.machineComponentsForMIDgeneration.driver = new MIDCodeGeneration.Models.DriverModels.Driver();
            Models.LocationReqest newlocation_current = new Models.LocationReqest();
            var postlocationresult = new HttpResponseMessage();
            short locationposition = 0;
            if (Orientation == null) { Orientation = "VHA";  } //fix what should be the default
            if (comptype == "Intermediate") { locationposition = (short)(int.Parse(locationtype.Substring(9,1))); }
            ComponentLocations currentcomponent = ProcessSelections.SelectComponentLocation(Comploc, comptype, compsubtype, compsubtype2, speedchange, numberofspeedchanges, locationposition);
            if (currentcomponent.componentLocationsLine != 0)
            {

                ProcessOtherFileWrites.WriteOtherFileLine(levelname, "ComponentLocations.txt",
                    currentcomponent.componentLocationsLine + " " +
                    $">>> " +
                    currentcomponent.componentType + " " +
                    currentcomponent.componentTypeSub + " " +
                    currentcomponent.componentTypeSub2 + " " +
                    currentcomponent.speedchangedirection + " " +
                    currentcomponent.speedchangenumberoftimes + " " +
                    currentcomponent.location + " " +
                    currentcomponent.locationlorkey1 + "  " +
                    currentcomponent.locationlorkey2 + " " +
                    currentcomponent.locationlorkey3 + " " +
                    currentcomponent.locationlorkey4 + "  " +
                    $"<<<"
                    );

                LocationLOR currentlocLOR = ProcessSelections.SelectLocationLOR(LocLOR, currentcomponent.locationlorkey1.ToUpper(), locationrpm);

                ProcessOtherFileWrites.WriteOtherFileLine(levelname, "LocationLOR.txt",
                    currentlocLOR.lorKey +
                    $">>> " +
                    currentlocLOR.lowrangefactorFMAX + " " +
                    currentlocLOR.highrangefactorFMAX + " " +
                    currentlocLOR.demodlowrangefactorFMAX + " " +
                    currentlocLOR.lowrangefactorLOR + " " +
                    currentlocLOR.highrangefactorLOR + " " +
                    currentlocLOR.demodlowrangefactorLOR + " " +
                    currentlocLOR.lowrangeLOR + " " +
                    currentlocLOR.highrangeLOR + " " +
                    currentlocLOR.demodlowrangeLOR + " " +
                    $"<<<"
                    );

                string subname;
                if (comptype == "Intermediate") { subname = locationtype.Substring(6); } else { subname = locationtype.Substring(14); }
                newlocation_current.PositionNumber = PositionNo;
                newlocation_current.MachineUUID = machineUUID;
                string compsubtypename = compsubtype.Substring(0, 1).ToUpper() + compsubtype.Substring(1);
                if (compsubtype.Contains("_") == true)
                {
                    compsubtypename = compsubtype.Substring(0, 1).ToUpper() + compsubtype.Substring(1,compsubtype.IndexOf("_") - 1);
                }
                newlocation_current.LocationName = compsubtypename + " Bearing " + PositionNo;
                //locationtype.Substring(14);
                newlocation_current.LocationUUID = Guid.NewGuid();
                newlocation_current.LocationRPM = (float)locationrpm;

                newlocation_current.LowRangeLOR = currentlocLOR.lowrangeLOR;
                newlocation_current.HighRangeLOR = currentlocLOR.highrangeLOR;
                newlocation_current.DemodLowRangeLOR = currentlocLOR.demodlowrangeLOR;
                newlocation_current.Orientation = Orientation;
                newlocation_current.UnitLabel = PlantUnits;
                newlocation_current.LineFrequency = PlantLineFrequency;
                // Instead send: Orientation, calculate the Resolution, etc (from Dan W.) to calculate the RangeSetupID's)

                try
                {
                    postlocationresult = await ProcessPostLocations.PostLocation(newlocation_current, tenantID, levelname);
                }
                catch
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"Call to InsertLocationAPI failed.", null);
                }
            }
            else
            {
                ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machineName}; LocationType: {locationtype} - Error in location logic - location not written.");
                EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: { machineName}; LocationType: { locationtype}; comptype: {comptype}; compsubtype: {compsubtype}; compsubtype2: {compsubtype2}; speedchange: {speedchange}; numberofspeedchanges: {numberofspeedchanges}; locationposition: {locationposition}; - Error in location logic -location not written.", null);
            }

            return postlocationresult;
        }
    }
}
