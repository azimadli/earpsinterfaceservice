﻿using EARPSInterfaceService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EARPSInterfaceService.Processors
{
    public class LoadData
    {
        public static List<LocationLOR> LoadLORdata()
        {
            List<LocationLOR> output = new List<LocationLOR>();
            output.Add(new LocationLOR { lorKey = "A", lowrangefactorFMAX = 1, highrangefactorFMAX = 40, demodlowrangefactorFMAX = 4, lowrangefactorLOR = 25, highrangefactorLOR = 250, demodlowrangefactorLOR = 25 });
            output.Add(new LocationLOR { lorKey = "B", lowrangefactorFMAX = 2, highrangefactorFMAX = 40, demodlowrangefactorFMAX = 4, lowrangefactorLOR = 25, highrangefactorLOR = 250, demodlowrangefactorLOR = 25 });
            output.Add(new LocationLOR { lorKey = "C", lowrangefactorFMAX = 4, highrangefactorFMAX = 40, demodlowrangefactorFMAX = 8, lowrangefactorLOR = 25, highrangefactorLOR = 250, demodlowrangefactorLOR = 25 });
            output.Add(new LocationLOR { lorKey = "D", lowrangefactorFMAX = 4, highrangefactorFMAX = 80, demodlowrangefactorFMAX = 8, lowrangefactorLOR = 25, highrangefactorLOR = 250, demodlowrangefactorLOR = 25 });
            output.Add(new LocationLOR { lorKey = "E", lowrangefactorFMAX = 8, highrangefactorFMAX = 80, demodlowrangefactorFMAX = 16, lowrangefactorLOR = 25, highrangefactorLOR = 250, demodlowrangefactorLOR = 25 });
            output.Add(new LocationLOR { lorKey = "F", lowrangefactorFMAX = 8, highrangefactorFMAX = 80, demodlowrangefactorFMAX = 8, lowrangefactorLOR = 25, highrangefactorLOR = 250, demodlowrangefactorLOR = 25 });
            output.Add(new LocationLOR { lorKey = "G", lowrangefactorFMAX = 4, highrangefactorFMAX = 40, demodlowrangefactorFMAX = 8, lowrangefactorLOR = 25, highrangefactorLOR = 250, demodlowrangefactorLOR = 25 });

            return output;
        }
        public static List<ComponentLocations> Loadcomplocdata()
        {
            List<ComponentLocations> output = new List<ComponentLocations>();
            int linenbr = 1;
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driver", componentTypeSub = "motor", componentTypeSub2 = "", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driver", componentTypeSub = "diesel", componentTypeSub2 = "", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driver", componentTypeSub = "turbine", componentTypeSub2 = "", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });

            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "decrease", speedchangenumberoftimes = 1, location = 1, locationlorkey1 = "C", locationlorkey2 = null, locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "decrease", speedchangenumberoftimes = 1, location = 2, locationlorkey1 = "C", locationlorkey2 = "B", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "decrease", speedchangenumberoftimes = 2, location = 1, locationlorkey1 = "C", locationlorkey2 = null, locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "decrease", speedchangenumberoftimes = 2, location = 2, locationlorkey1 = "C", locationlorkey2 = "B", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "decrease", speedchangenumberoftimes = 2, location = 3, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = "B", locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "decrease", speedchangenumberoftimes = 2, location = 4, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = "B", locationlorkey4 = "A" });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "decrease", speedchangenumberoftimes = 3, location = 1, locationlorkey1 = "C", locationlorkey2 = null, locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "decrease", speedchangenumberoftimes = 3, location = 2, locationlorkey1 = "C", locationlorkey2 = "B", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "decrease", speedchangenumberoftimes = 3, location = 3, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = "B", locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "decrease", speedchangenumberoftimes = 3, location = 4, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = "B", locationlorkey4 = "A" });

            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "increase", speedchangenumberoftimes = 1, location = 1, locationlorkey1 = "E", locationlorkey2 = null, locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "increase", speedchangenumberoftimes = 1, location = 2, locationlorkey1 = "D", locationlorkey2 = "E", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "increase", speedchangenumberoftimes = 2, location = 1, locationlorkey1 = "D", locationlorkey2 = null, locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "increase", speedchangenumberoftimes = 2, location = 2, locationlorkey1 = "D", locationlorkey2 = "E", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "increase", speedchangenumberoftimes = 2, location = 3, locationlorkey1 = "D", locationlorkey2 = "E", locationlorkey3 = "E", locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "increase", speedchangenumberoftimes = 2, location = 4, locationlorkey1 = "D", locationlorkey2 = "E", locationlorkey3 = "E", locationlorkey4 = "E" });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "increase", speedchangenumberoftimes = 3, location = 1, locationlorkey1 = "D", locationlorkey2 = null, locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "increase", speedchangenumberoftimes = 3, location = 2, locationlorkey1 = "D", locationlorkey2 = "E", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "increase", speedchangenumberoftimes = 3, location = 3, locationlorkey1 = "D", locationlorkey2 = "E", locationlorkey3 = "E", locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "intermediate", componentTypeSub = "gearbox", componentTypeSub2 = "", speedchangedirection = "increase", speedchangenumberoftimes = 3, location = 4, locationlorkey1 = "D", locationlorkey2 = "E", locationlorkey3 = "E", locationlorkey4 = "E" });

            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "pump", componentTypeSub2 = "centrifugal", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "pump", componentTypeSub2 = "propeller", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "pump", componentTypeSub2 = "rotarythread", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "pump", componentTypeSub2 = "gear", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "pump", componentTypeSub2 = "screw", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "pump", componentTypeSub2 = "slidingvane", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "pump", componentTypeSub2 = "axialrecip", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "pump", componentTypeSub2 = "radialrecip", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });

            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "compressor", componentTypeSub2 = "centrifugal", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "G", locationlorkey2 = "G", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "compressor", componentTypeSub2 = "reciprocating", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "compressor", componentTypeSub2 = "screw", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "compressor", componentTypeSub2 = "screwtwin", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });

            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "fan_or_blower", componentTypeSub2 = "lobed", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "fan_or_blower", componentTypeSub2 = "overhungrotor", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "F", locationlorkey2 = "F", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "fan_or_blower", componentTypeSub2 = "supportedrotor", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "F", locationlorkey2 = "F", locationlorkey3 = null, locationlorkey4 = null });

            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "purifier_centrifuge", componentTypeSub2 = "", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });

            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "decanter", componentTypeSub2 = "", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });

            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "generator", componentTypeSub2 = "", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });

            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "vacuumpump", componentTypeSub2 = "centrifugal", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "G", locationlorkey2 = "G", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "vacuumpump", componentTypeSub2 = "axialrecip", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "vacuumpump", componentTypeSub2 = "radialrecip", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "vacuumpump", componentTypeSub2 = "reciprocating", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "vacuumpump", componentTypeSub2 = "lobed", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });

            output.Add(new ComponentLocations { componentLocationsLine = linenbr++, componentType = "driven", componentTypeSub = "spindle_or_shaft_or_bearing", componentTypeSub2 = "", speedchangedirection = "", speedchangenumberoftimes = 0, location = 0, locationlorkey1 = "C", locationlorkey2 = "C", locationlorkey3 = null, locationlorkey4 = null });
            return output;
        }
        public static ComponentPickupCodesInCompRec LoadPickupsdata(string pickupcodes)
        {
            ComponentPickupCodesInCompRec output = new ComponentPickupCodesInCompRec();
            string[] pickupcodesarray = (pickupcodes + ",,,,").Split(',');
            output.PickupOne = (short)(String.IsNullOrEmpty(pickupcodesarray[0]) ? 0 : int.Parse(pickupcodesarray[0]));
            output.PickupTwo = (short)(String.IsNullOrEmpty(pickupcodesarray[1]) ? 0 : int.Parse(pickupcodesarray[1]));
            output.PickupThree = (short)(String.IsNullOrEmpty(pickupcodesarray[2]) ? 0 : int.Parse(pickupcodesarray[2]));
            output.PickupFour = (short)(String.IsNullOrEmpty(pickupcodesarray[3]) ? 0 : int.Parse(pickupcodesarray[3]));
            return output;
        }
    }
}
