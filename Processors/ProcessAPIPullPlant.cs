﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using System.Web;
using EARPSInterfaceService.Helpers;
using Alba;
using System.Text.Json;
using System.Net.Http.Headers;
using SAAIDILibrary.Models;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessAPIPullPlant
    {
        public static async Task<PullPlant> PullPlant(string CustomerName, Guid plantUUID, Guid jobID, string levelname)
        {
            string url = "";

            url = $"{ServiceSettings.apiRPSuriBase}{ServiceSettings.apiRPSuriBase2}pull/customer/plant/{ plantUUID }?jobId={jobID}";

            using (HttpResponseMessage response = await APIRPSHelper.ApiRPSClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    PullPlant pullplant = await response.Content.ReadAsAsync<PullPlant>();
                    DebuggingHelper.WriteJsonToFile((await response.Content.ReadAsStringAsync()), levelname, ("pullplant" + pullplant.name));

                    EARPSInterfaceServiceLogger.AddLogEntry($"CustomerName: {CustomerName} jobID: {jobID} Plant pulled: {plantUUID}", null);
                    return pullplant;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }
 }
