﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using System.Web;
using EARPSInterfaceService.Helpers;
using Alba;
using System.Text.Json;
using System.Net.Http.Headers;
using SAAIDILibrary.Models;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessAPIPushPlant
    {
        public static async Task<PushPlantResponse> PostPushPlant(EARPSInterfaceService.Models.PushPlant pushplant, string CustomerName, Guid customerid, Guid plantUUID, Guid jobID, string levelname)
        {
            var url = $"{ServiceSettings.apiRPSuriBase}{ServiceSettings.apiRPSuriBase2}plant/pushPlant?jobId={jobID}&customerId={customerid}";
            var jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

            DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(pushplant, new JsonSerializerOptions { WriteIndented = true }), levelname, $"PushPlant_{pushplant.name}");

            using (HttpResponseMessage response = await APIRPSHelper.ApiRPSClient.PostAsJsonAsync(url, pushplant))
            {
                if (response.IsSuccessStatusCode)
                {
                    PushPlantResponse reponsepushplant = await response.Content.ReadAsAsync<EARPSInterfaceService.Models.PushPlantResponse>();
                    DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(reponsepushplant.message), levelname, ("postPushPlantResponse" + pushplant.name));

                    EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {customerid} Plant pushed: {pushplant.name}; status: {reponsepushplant.status}; {reponsepushplant.message};", null);
                    return reponsepushplant;

                }
                else
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {customerid} Plant pushed: {pushplant.name}; {response.StatusCode} {response.ReasonPhrase}", null);
                    throw new Exception ($"{response.StatusCode} {response.ReasonPhrase}");
                };
            }
        }
    }
 }
