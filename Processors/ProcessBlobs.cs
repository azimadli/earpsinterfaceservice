﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessBlobs
    {
        public static async Task<byte[]> GetBlob(string blobconnectionstring, string blobcontainer, string blobname4)
        {
            BlobClient blobclient3 = new BlobClient(blobconnectionstring, blobcontainer, blobname4);
            try
            {
                using (var memorystream = new MemoryStream())
                {
                    await blobclient3.DownloadToAsync(memorystream);
                    return memorystream.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to retrieve blob", ex);
            }
        }
        public static byte[]  GetPicFile(string filename)
        {
            try
            {
                MemoryStream memorystream = new MemoryStream();

                using (FileStream filestream = new FileStream(filename, FileMode.Open))
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"Reading to filestream {filestream.Length.ToString()}", null);
                    filestream.CopyTo(memorystream);
                    return memorystream.ToArray();
                };
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to retrieve blob", ex);
            }
        }

    }

}
