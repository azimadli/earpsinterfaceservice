﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using Newtonsoft.Json;
using EARPSInterfaceService.Logger;
using EARPSInterfaceService.Processors;
using System.IO;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace EARPSInterfaceService.Processors
{
    class ProcessComponentMap
    {
        public static ComponentInfo TranslateComponentMap(MachineComponentMap MachineCMBeingProcessed, string machinename, string levelname)
        {
            ComponentInfo componentinfocurrent = new ComponentInfo();

            //var componentstuffkey = MachineBeingProcessed.machineComponentMap.FirstOrDefault().key;
            //var componentstuff = MachineBeingProcessed.machineComponentMap.FirstOrDefault().value;
            var componentstuffkey = MachineCMBeingProcessed.key;
            var componentstuff = MachineCMBeingProcessed.value;
            componentinfocurrent.componentstuff = MachineCMBeingProcessed.value;

            if ((MachineCMBeingProcessed.value.ToLower().Contains("orientation") == true) && (MachineCMBeingProcessed.key != "Orientation Information"))
            {
                EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : contains orientation data {MachineCMBeingProcessed.value}", null);
            }
            if (MachineCMBeingProcessed.value.ToLower().Contains("rpm"))
            {
                EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : contains rpm {MachineCMBeingProcessed.value}", null);
            }
            if (MachineCMBeingProcessed.value.ToLower().Contains("ratio"))
            {
                EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : contains ratio {MachineCMBeingProcessed.value}", null);
            }

            componentstuff = componentstuff.Replace("Int:", "intermediate:");
            componentstuff = componentstuff.Replace("Spindle, Shaft or Bearing", "spindle_or_shaft_or_bearing");
            componentstuff = componentstuff.Replace(" : ", " / ");
            componentstuff = componentstuff.Replace("[", "{\"");
            componentstuff = componentstuff.Replace("]", "\"}");
            componentstuff = componentstuff.Replace(", ", "\", \"");
            componentstuff = componentstuff.Replace(": ", "\": \"");
            componentstuff = componentstuff.Replace("location 1", "location_1");
            componentstuff = componentstuff.Replace("location 2", "location_2");
            componentstuff = componentstuff.Replace("location 3", "location_3");
            componentstuff = componentstuff.Replace("location 4", "location_4");
            componentstuff = componentstuff.Replace("DEOrientation brg ", "DEOrientation_brg_");
            componentstuff = componentstuff.Replace("No Coupling", "nocoupling");
            componentstuff = componentstuff.Replace("Electric Motor or Turbine", "NOTdieselengine");
            componentstuff = componentstuff.Replace("fan_blades", "fanBlades");
            componentstuff = componentstuff.Replace("propeller_blades", "propellerBlades");
            componentstuff = componentstuff.Replace("motofan", "motorfan");
            componentstuff = componentstuff.Replace("motospeed", "motorspeed");

            var output = Newtonsoft.Json.JsonConvert.SerializeObject(componentstuff);
            componentinfocurrent = System.Text.Json.JsonSerializer.Deserialize<ComponentInfo>(componentstuff, new JsonSerializerOptions() { WriteIndented = true });
            componentinfocurrent.componentstuff = MachineCMBeingProcessed.value;

            // Driver Component Map begin
            if (MachineCMBeingProcessed.key == "Driver")
            {
                EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}", null);
                //Driver <=> Driver (string) "diesel", "motor", "turbine"
                componentinfocurrent.Driver = componentinfocurrent.Driver.Replace("Not Monitored", "not monitored");
                componentinfocurrent.Driver = componentinfocurrent.Driver.Replace("Diesel Engine", "diesel");
                componentinfocurrent.Driver = componentinfocurrent.Driver.Replace("Motor", "motor");
                componentinfocurrent.Driver = componentinfocurrent.Driver.Replace("Turbine", "turbine");
                componentinfocurrent.Driver = componentinfocurrent.Driver.ToLower();
                if (componentinfocurrent.Driver == null)
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; Driver value is NULL", null);
                }
                else
                { 
                    if ((componentinfocurrent.Driver.Contains("not monitored") == false) &&
                            (componentinfocurrent.Driver.Contains("diesel") == false) &&
                            (componentinfocurrent.Driver.Contains("motor") == false) &&
                            (componentinfocurrent.Driver.Contains("turbine") == false)
                            )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN Driver value {componentinfocurrent.Driver}", null);
                    }
                }

                componentinfocurrent.drive = String.IsNullOrEmpty(componentinfocurrent.drive) ? null : componentinfocurrent.drive.Replace("AC", "AC");
                componentinfocurrent.drive = String.IsNullOrEmpty(componentinfocurrent.drive) ? null : componentinfocurrent.drive.Replace("DC", "DC");
                componentinfocurrent.drive = String.IsNullOrEmpty(componentinfocurrent.drive) ? null : componentinfocurrent.drive.Replace("VFD", "VFD");
                if (componentinfocurrent.drive == null)
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; drive value is NULL", null);
                }
                else
                {
                    if ((componentinfocurrent.drive.Contains("AC") == false) &&
                            (componentinfocurrent.drive.Contains("DC") == false) &&
                            (componentinfocurrent.drive.Contains("VFD") == false)
                            )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN drive value {componentinfocurrent.drive}", null);
                    }
                }

                componentinfocurrent.Driver_locations_int = String.IsNullOrEmpty(componentinfocurrent.Driver_locations) ? 0 : int.Parse(componentinfocurrent.Driver_locations);
                componentinfocurrent.cyl_int = String.IsNullOrEmpty(componentinfocurrent.cyl) ? 0 : int.Parse(componentinfocurrent.cyl);
                componentinfocurrent.motorpoles_int = String.IsNullOrEmpty(componentinfocurrent.motorpoles) ? 0 : int.Parse(componentinfocurrent.motorpoles);
                componentinfocurrent.motorbars_int = String.IsNullOrEmpty(componentinfocurrent.motorbars) ? 0 : int.Parse(componentinfocurrent.motorbars);
                componentinfocurrent.fanBlades_int = String.IsNullOrEmpty(componentinfocurrent.fanBlades) ? 0 : int.Parse(componentinfocurrent.fanBlades);

                componentinfocurrent.isCloseCoupled_boolean = String.IsNullOrEmpty(componentinfocurrent.isCloseCoupled) ? false : componentinfocurrent.isCloseCoupled == "Yes" ? true : false;
                componentinfocurrent.redgear_boolean = String.IsNullOrEmpty(componentinfocurrent.redgear) ? false : componentinfocurrent.redgear == "Yes" ? true : false;
                componentinfocurrent.turbinesupported_boolean = String.IsNullOrEmpty(componentinfocurrent.turbinesupported) ? false : componentinfocurrent.turbinesupported == "Yes" ? true : false;
                componentinfocurrent.turbinebb_boolean = String.IsNullOrEmpty(componentinfocurrent.turbinebb) ? false : componentinfocurrent.turbinebb == "Yes" ? true : false;
                componentinfocurrent.turbineTbrg_boolean = String.IsNullOrEmpty(componentinfocurrent.turbineTbrg) ? false : componentinfocurrent.turbineTbrg == "Yes" ? true : false;
                componentinfocurrent.Tbrgbb_boolean = String.IsNullOrEmpty(componentinfocurrent.Tbrgbb) ? false : componentinfocurrent.Tbrgbb == "Yes" ? true : false;
                componentinfocurrent.motorfan_boolean = String.IsNullOrEmpty(componentinfocurrent.motorfan) ? false : componentinfocurrent.motorfan == "Yes" ? true : false;
                componentinfocurrent.motorbb_boolean = String.IsNullOrEmpty(componentinfocurrent.motorbb) ? false : componentinfocurrent.motorbb == "Yes" ? true : false;

                componentinfocurrent.CCcpl = String.IsNullOrEmpty(componentinfocurrent.CCcpl) ? null : componentinfocurrent.CCcpl.Replace("Close Coupled", "closecoupled");
                if (componentinfocurrent.CCcpl == null)
                {
                    //EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; CCcpl value is NULL", null);
                }
                else
                {
                    if (
                        (componentinfocurrent.CCcpl.Contains("closecoupled") == false)
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN CCcpl value {componentinfocurrent.CCcpl}", null);
                    }
                };

                if (componentinfocurrent.isCloseCoupled_boolean == true)
                {
                    componentinfocurrent.closeCoupledType = String.IsNullOrEmpty(componentinfocurrent.CCcpl) ? null : componentinfocurrent.CCcpl.Replace("Flexible", "flexible").Replace("Close Coupled", "closecoupled");

                    componentinfocurrent.closeCoupledLocations_int = String.IsNullOrEmpty(componentinfocurrent.couplingLocation) ? 0 : int.Parse(componentinfocurrent.couplingLocation);

                    if (componentinfocurrent.driven == null)
                    {
                        componentinfocurrent.closeCoupledDriven = null;
                    }
                    else
                    {
                        componentinfocurrent.closeCoupledDriven = componentinfocurrent.driven.Trim();
                        if (componentinfocurrent.closeCoupledDriven == "Single-Stage Fan")
                        {
                            EARPSInterfaceServiceLogger.AddLogEntry($"componentinfocurrent.closeCoupledDriven: >>{componentinfocurrent.closeCoupledDriven}<<", null);
                            EARPSInterfaceServiceLogger.AddLogEntry($"componentinfocurrent.closeCoupledDriven length: { componentinfocurrent.closeCoupledDriven.Length}", null);
                        }
                        componentinfocurrent.closeCoupledDriven = componentinfocurrent.closeCoupledDriven.Replace("Centrifugal Compressor", "compressor");
                        EARPSInterfaceServiceLogger.AddLogEntry($"componentinfocurrent.closeCoupledDriven: >>{componentinfocurrent.closeCoupledDriven}<<", null);
                        EARPSInterfaceServiceLogger.AddLogEntry($"componentinfocurrent.closeCoupledDriven length: { componentinfocurrent.closeCoupledDriven.Length}", null);
                        componentinfocurrent.closeCoupledDriven = componentinfocurrent.closeCoupledDriven.Replace("Centrifugal Pump - Supported Rotor", "centrifugalPumpSupported");
                        componentinfocurrent.closeCoupledDriven = componentinfocurrent.closeCoupledDriven.Replace("Centrifugal Pump - Overhung Rotor", "centrifugalPumpOverhung");
                        componentinfocurrent.closeCoupledDriven = componentinfocurrent.closeCoupledDriven.Replace("Pump (Supported)", "pumpsupported");
                        componentinfocurrent.closeCoupledDriven = componentinfocurrent.closeCoupledDriven.Replace("Pump-Motor-Pump", "pumpmotorpump");
                        componentinfocurrent.closeCoupledDriven = componentinfocurrent.closeCoupledDriven.Replace("Rotary Gear", "rotarygear");
                        componentinfocurrent.closeCoupledDriven = componentinfocurrent.closeCoupledDriven.Replace("Single-Stage Fan with Slinger", "singlestagefanwithslinger");
                        componentinfocurrent.closeCoupledDriven = componentinfocurrent.closeCoupledDriven.Replace("Single-Stage Fan", "singlestagefan");
                    }

                    componentinfocurrent.closeCoupleddrivenBallBearings_boolean = String.IsNullOrEmpty(componentinfocurrent.drivenbb) ? false : componentinfocurrent.drivenbb == "Yes" ? true : false;
                }

                componentinfocurrent.cpl1 = String.IsNullOrEmpty(componentinfocurrent.cpl1) ? null : componentinfocurrent.cpl1.Replace("Magnetic", "magnetic");
                componentinfocurrent.cpl1 = String.IsNullOrEmpty(componentinfocurrent.cpl1) ? null : componentinfocurrent.cpl1.Replace("Flexible", "flexible");
                componentinfocurrent.cpl1 = String.IsNullOrEmpty(componentinfocurrent.cpl1) ? null : componentinfocurrent.cpl1.Replace("Chain Drive", "chaindrive");
                componentinfocurrent.cpl1 = String.IsNullOrEmpty(componentinfocurrent.cpl1) ? null : componentinfocurrent.cpl1.Replace("No Coupling", "nocoupling");
                componentinfocurrent.cpl1 = String.IsNullOrEmpty(componentinfocurrent.cpl1) ? null : componentinfocurrent.cpl1.Replace("Belt Drive", "beltdrive");
                componentinfocurrent.cpl1rationumerator = 1;
                componentinfocurrent.cpl1ratiodenominator = 1;
                if (componentinfocurrent.cpl1 == null)
                {
                    //EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; cpl1 value is NULL", null);
                }
                else
                {
                    componentinfocurrent.speed = String.IsNullOrEmpty(componentinfocurrent.speed) ? null : componentinfocurrent.speed;
                    if (componentinfocurrent.speed != null)
                    {
                        List<LocationRatios> LocRatioList = ProcessSelections.ParseRatio("{R1 = " + componentinfocurrent.speed.Replace(":","/") + " }");
                        //componentinfocurrent.ISC_int = LocRatioList.Count;
                        if (LocRatioList.Count > 0)
                        {
                            componentinfocurrent.cpl1rationumerator = LocRatioList[0].numerator;
                            componentinfocurrent.cpl1ratiodenominator = LocRatioList[0].denominator;
                            componentinfocurrent.cpl1speedchange = LocRatioList[0].speedchange;
                        }
                    }
                    if (
                        (componentinfocurrent.cpl1.Contains("nocoupling") == false) &&
                        (componentinfocurrent.cpl1.Contains("magnetic") == false) &&
                        (componentinfocurrent.cpl1.Contains("flexible") == false) &&
                        (componentinfocurrent.cpl1.Contains("chaindrive") == false) &&
                        (componentinfocurrent.cpl1.Contains("beltdrive") == false)
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN cpl1 value {componentinfocurrent.cpl1}", null);
                    }
                }

                componentinfocurrent.Driver_locationType = String.IsNullOrEmpty(componentinfocurrent.Driver_locationType) ? null : componentinfocurrent.Driver_locationType.Replace("NDE", "NDE");
                componentinfocurrent.Driver_locationType = String.IsNullOrEmpty(componentinfocurrent.Driver_locationType) ? null : componentinfocurrent.Driver_locationType.Replace("DE", "DE");
                if (componentinfocurrent.Driver_locationType == null)
                {
                    //EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; Driver_locationType value is NULL", null);
                }
                else
                {
                    if ((componentinfocurrent.Driver_locationType.Contains("NDE") == false) &&
                        (componentinfocurrent.Driver_locationType.Contains("DE") == false)
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN Driver_locationType value {componentinfocurrent.Driver_locationType}", null);
                    }
                }

                //"motorDrive" (string) [ "AC", "DC", "VFD" ] motorDrive is a required string if driverType is motor and must be "AC", "DC", "VFD"
                //"motorFan" (boolean) [ true, false ] motorFan is a required boolean if driverType is motor
                //"motorBallBearings" (boolean) [ true, false ] motorBallBearings is a required boolean if driverType is motor
                //"drivenBallBearings" (boolean) [ true, false ] drivenBallBearings is a required boolean if driverType is motor
                //"drivenBalanceable" (boolean) [ true, false ] drivenBalanceable is a required boolean if driverType is motor
                //"motorPoles" int [ 2, 4, 6, 8, 10 ] motorPoles is a required integer if motorDrive is "VFD" and must be 2, 4, 6, 8, 10
                //"motorbars" int 0, motorbars is an optional integer if driverType is motor
                //"motorfanblades" int 0 motorfanblades is an optional integer if driverType is motor

            };
            // Driver Component Map end

            // Intermediate Component Map begin
            if (MachineCMBeingProcessed.key == "Intermediate")
            {
                EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}", null);
                componentinfocurrent.intermediate = componentinfocurrent.intermediate.Replace("Not Monitored", "not monitored");
                componentinfocurrent.intermediate = componentinfocurrent.intermediate.Replace("Gearbox", "gearbox");
                componentinfocurrent.intermediate = componentinfocurrent.intermediate.ToLower();
                if (componentinfocurrent.intermediate == null)
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; intermediate value is NULL", null);
                }
                else
                { 
                    if ((componentinfocurrent.intermediate.Contains("not monitored") == false) &&
                        (componentinfocurrent.intermediate.Contains("gearbox") == false)
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN intermediate value {componentinfocurrent.intermediate}", null);
                    }
                }

                componentinfocurrent.NOL_int = String.IsNullOrEmpty(componentinfocurrent.NOL) ? 0 : int.Parse(componentinfocurrent.NOL);
                bool isNumber = int.TryParse(componentinfocurrent.ISC, out int numericValue);
                if (isNumber == true) 
                {
                    componentinfocurrent.ISC_int = String.IsNullOrEmpty(componentinfocurrent.ISC) ? 0 : int.Parse(componentinfocurrent.ISC);
                }
                else
                {
                    if (componentinfocurrent.ISC != null && componentinfocurrent.ISC.ToUpper() == "MULTI")
                    {
                        componentinfocurrent.ISC_int = 3;
                    }
                }


                componentinfocurrent.location_1 = String.IsNullOrEmpty(componentinfocurrent.location_1) ? null : componentinfocurrent.location_1.Replace(" ", "_");
                if (componentinfocurrent.location_1 != null)
                {
                    if (
                        componentinfocurrent.location_1.Contains("brg_3") == false &&
                        componentinfocurrent.location_1.Contains("brg_4") == false &&
                        componentinfocurrent.location_1.Contains("brg_5") == false &&
                        componentinfocurrent.location_1.Contains("brg_6") == false &&
                        componentinfocurrent.location_1.Contains("brg_7") == false &&
                        componentinfocurrent.location_1.Contains("brg_8") == false &&
                        componentinfocurrent.location_1.Contains("brg_9") == false &&
                        componentinfocurrent.location_1.Contains("brg_10") == false
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN location_1 value {componentinfocurrent.location_1}", null);
                    }
                }
                componentinfocurrent.location_2 = String.IsNullOrEmpty(componentinfocurrent.location_2) ? null : componentinfocurrent.location_2.Replace(" ", "_");
                if (componentinfocurrent.location_2 != null)
                {
                    if (
                        componentinfocurrent.location_2.Contains("brg_3") == false &&
                        componentinfocurrent.location_2.Contains("brg_4") == false &&
                        componentinfocurrent.location_2.Contains("brg_5") == false &&
                        componentinfocurrent.location_2.Contains("brg_6") == false &&
                        componentinfocurrent.location_2.Contains("brg_7") == false &&
                        componentinfocurrent.location_2.Contains("brg_8") == false &&
                        componentinfocurrent.location_2.Contains("brg_9") == false &&
                        componentinfocurrent.location_2.Contains("brg_10") == false
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN location_2 value {componentinfocurrent.location_2}", null);
                    }
                }
                componentinfocurrent.location_3 = String.IsNullOrEmpty(componentinfocurrent.location_3) ? null : componentinfocurrent.location_3.Replace(" ", "_");
                if (componentinfocurrent.location_3 != null)
                {
                    if (
                        componentinfocurrent.location_3.Contains("brg_3") == false &&
                        componentinfocurrent.location_3.Contains("brg_4") == false &&
                        componentinfocurrent.location_3.Contains("brg_5") == false &&
                        componentinfocurrent.location_3.Contains("brg_6") == false &&
                        componentinfocurrent.location_3.Contains("brg_7") == false &&
                        componentinfocurrent.location_3.Contains("brg_8") == false &&
                        componentinfocurrent.location_3.Contains("brg_9") == false &&
                        componentinfocurrent.location_3.Contains("brg_10") == false
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN location_3 value {componentinfocurrent.location_3}", null);
                    }
                }
                componentinfocurrent.location_4 = String.IsNullOrEmpty(componentinfocurrent.location_4) ? null : componentinfocurrent.location_4.Replace(" ", "_");
                if (componentinfocurrent.location_4 != null)
                {
                    if (
                        componentinfocurrent.location_4.Contains("brg_3") == false &&
                        componentinfocurrent.location_4.Contains("brg_4") == false &&
                        componentinfocurrent.location_4.Contains("brg_5") == false &&
                        componentinfocurrent.location_4.Contains("brg_6") == false &&
                        componentinfocurrent.location_4.Contains("brg_7") == false &&
                        componentinfocurrent.location_4.Contains("brg_8") == false &&
                        componentinfocurrent.location_4.Contains("brg_9") == false &&
                        componentinfocurrent.location_4.Contains("brg_10") == false
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN location_4 value {componentinfocurrent.location_4}", null);
                    }
                }

                //Coupling2 <=> Coupling2 (string)
                componentinfocurrent.cpl2 = String.IsNullOrEmpty(componentinfocurrent.cpl2) ? null : componentinfocurrent.cpl2.Replace("Flexible", "flexible");
                componentinfocurrent.cpl2 = String.IsNullOrEmpty(componentinfocurrent.cpl2) ? null : componentinfocurrent.cpl2.Replace("Chain Drive", "chaindrive");
                componentinfocurrent.cpl2 = String.IsNullOrEmpty(componentinfocurrent.cpl2) ? null : componentinfocurrent.cpl2.Replace("No Coupling", "nocoupling");
                if (componentinfocurrent.cpl2 == null)
                {
                    //EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; cpl2 value is NULL", null);
                }
                else
                {
                    componentinfocurrent.cpl2rationumerator = 1;
                    componentinfocurrent.cpl2ratiodenominator = 1;
                    if (componentinfocurrent.speed != null)
                    {
                        List<LocationRatios> LocRatioList = ProcessSelections.ParseRatio("{R1 = " + componentinfocurrent.speed.Replace(":", "/") + " }");
                        //componentinfocurrent.ISC_int = LocRatioList.Count;
                        if (LocRatioList.Count > 0)
                        {
                            componentinfocurrent.cpl2rationumerator = LocRatioList[0].numerator;
                            componentinfocurrent.cpl2rationumerator = LocRatioList[0].denominator;
                            componentinfocurrent.cpl2speedchange = LocRatioList[0].speedchange;
                        }
                    }
                    if (
                        (componentinfocurrent.cpl2.Contains("nocoupling") == false) &&
                        (componentinfocurrent.cpl2.Contains("flexible") == false) &&
                        (componentinfocurrent.cpl2.Contains("chaindrive") == false)
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN cpl2 value {componentinfocurrent.cpl2}", null);
                    }
                }

                componentinfocurrent.LOC1rationumerator = 1;
                componentinfocurrent.LOC1ratiodenominator = 1;
                componentinfocurrent.LOC1speedchange = "nochange";
                componentinfocurrent.LOC2rationumerator = 1;
                componentinfocurrent.LOC2ratiodenominator = 1;
                componentinfocurrent.LOC2speedchange = "nochange";
                componentinfocurrent.LOC3rationumerator = 1;
                componentinfocurrent.LOC3ratiodenominator = 1;
                componentinfocurrent.LOC3speedchange = "nochange";
                componentinfocurrent.LOC4rationumerator = 1;
                componentinfocurrent.LOC4ratiodenominator = 1;
                componentinfocurrent.LOC4speedchange = "nochange";
                if (componentinfocurrent.ratios != null) 
                {
                    List<LocationRatios> LocRatioList = ProcessSelections.ParseRatio(componentinfocurrent.ratios);
                    //componentinfocurrent.ISC_int = LocRatioList.Count;
                    if (LocRatioList.Count > 0)
                    {
                        componentinfocurrent.LOC1rationumerator = LocRatioList[0].numerator;
                        componentinfocurrent.LOC1ratiodenominator = LocRatioList[0].denominator;
                        componentinfocurrent.LOC1speedchange = LocRatioList[0].speedchange;
                    }
                    if (LocRatioList.Count > 1)
                    {
                        componentinfocurrent.LOC2rationumerator = LocRatioList[1].numerator;
                        componentinfocurrent.LOC2ratiodenominator = LocRatioList[1].denominator;
                        componentinfocurrent.LOC2speedchange = LocRatioList[1].speedchange;
                    }
                    if (LocRatioList.Count > 2)
                    {
                        componentinfocurrent.LOC3rationumerator = LocRatioList[2].numerator;
                        componentinfocurrent.LOC3ratiodenominator = LocRatioList[2].denominator;
                        componentinfocurrent.LOC3speedchange = LocRatioList[2].speedchange;
                    }
                    if (LocRatioList.Count > 3)
                    {
                        componentinfocurrent.LOC4rationumerator = LocRatioList[3].numerator;
                        componentinfocurrent.LOC4ratiodenominator = LocRatioList[3].denominator;
                        componentinfocurrent.LOC4speedchange = LocRatioList[3].speedchange;
                    }
                };
            };
            // Intermediate Component Map begin

            // Driven Component Map begin
            if (MachineCMBeingProcessed.key == "Driven")
            {
                EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}", null);
                componentinfocurrent.Driven = componentinfocurrent.Driven.Replace("Not Monitored", "not monitored");
                componentinfocurrent.Driven = componentinfocurrent.Driven.Replace("Fan or Blower", "fan_or_blower");
                componentinfocurrent.Driven = componentinfocurrent.Driven.Replace("Purifier (Centrifuge)", "purifier_centrifuge");
                componentinfocurrent.Driven = componentinfocurrent.Driven.Replace("Generator", "generator");
                componentinfocurrent.Driven = componentinfocurrent.Driven.Replace("Pump", "pump");
                componentinfocurrent.Driven = componentinfocurrent.Driven.ToLower();
                if (componentinfocurrent.Driven == null)
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; Driven value is NULL", null);
                }
                else
                { 
                    if ((componentinfocurrent.Driven.Contains("not monitored") == false) &&
                        (componentinfocurrent.Driven.Contains("fan_or_blower") == false) &&
                        (componentinfocurrent.Driven.Contains("purifier_centrifuge") == false) &&
                        (componentinfocurrent.Driven.Contains("generator") == false) &&
                        (componentinfocurrent.Driven.Contains("pump") == false) 
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN Driven value {componentinfocurrent.Driven}", null);
                    }
                }

                componentinfocurrent.drivenbrgs = String.IsNullOrEmpty(componentinfocurrent.drivenbrgs) ? null : componentinfocurrent.drivenbrgs.Replace("Ball Bearings Both Ends", "ballbearingsbothends");
                componentinfocurrent.drivenbrgs = String.IsNullOrEmpty(componentinfocurrent.drivenbrgs) ? null : componentinfocurrent.drivenbrgs.Replace("NDE Journal", "NDE_Journal");
                componentinfocurrent.drivenbrgs = String.IsNullOrEmpty(componentinfocurrent.drivenbrgs) ? null : componentinfocurrent.drivenbrgs.Replace("NDE Ball", "NDE_Ball");
                componentinfocurrent.drivenbrgs = String.IsNullOrEmpty(componentinfocurrent.drivenbrgs) ? null : componentinfocurrent.drivenbrgs.Replace("Journal Bearings Both Ends", "journalbearingsbothends");
                if (componentinfocurrent.drivenbrgs == null)
                {
                    //EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; drivenbrgs value is NULL", null);
                }
                else
                {
                    if ((componentinfocurrent.drivenbrgs.Contains("ballbearingsbothends") == false) &&
                        (componentinfocurrent.drivenbrgs.Contains("NDE_Journal") == false) &&
                        (componentinfocurrent.drivenbrgs.Contains("NDE_Ball") == false) &&
                        (componentinfocurrent.drivenbrgs.Contains("journalbearingsbothends") == false)
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN drivenbrgs value {componentinfocurrent.drivenbrgs}", null);
                    }
                }

                componentinfocurrent.Driven_locations_int = String.IsNullOrEmpty(componentinfocurrent.Driven_locations) ? 0 : int.Parse(componentinfocurrent.Driven_locations);
                componentinfocurrent.Driven_Threads_int = String.IsNullOrEmpty(componentinfocurrent.Driven_Threads) ? 0 : int.Parse(componentinfocurrent.Driven_Threads);
                componentinfocurrent.fanBlades_int = String.IsNullOrEmpty(componentinfocurrent.fanBlades) ? 0 : int.Parse(componentinfocurrent.fanBlades);
                componentinfocurrent.propellerBlades_int = String.IsNullOrEmpty(componentinfocurrent.propellerBlades) ? 0 : int.Parse(componentinfocurrent.propellerBlades);
                componentinfocurrent.Driven_Threads_int = String.IsNullOrEmpty(componentinfocurrent.Driven_Threads) ? 0 : int.Parse(componentinfocurrent.Driven_Threads);
                componentinfocurrent.Idler_threads_int = String.IsNullOrEmpty(componentinfocurrent.Idler_threads) ? 0 : int.Parse(componentinfocurrent.Idler_threads);
                componentinfocurrent.pistons_int = String.IsNullOrEmpty(componentinfocurrent.pistons) ? 0 : int.Parse(componentinfocurrent.pistons);
                componentinfocurrent.vanes_int = String.IsNullOrEmpty(componentinfocurrent.vanes) ? 0 : int.Parse(componentinfocurrent.vanes);

                componentinfocurrent.motorspeed_float = String.IsNullOrEmpty(componentinfocurrent.motorspeed) ? 0 : int.Parse(componentinfocurrent.motorspeed);
                componentinfocurrent.driverspeed_float = String.IsNullOrEmpty(componentinfocurrent.speed) ? 0 : int.Parse(componentinfocurrent.speed);

                componentinfocurrent.Aop_boolean = String.IsNullOrEmpty(componentinfocurrent.Aop) ? false : componentinfocurrent.Aop == "Yes" ? true : false;
                componentinfocurrent.exc_boolean = String.IsNullOrEmpty(componentinfocurrent.exc) ? false : componentinfocurrent.exc == "Yes" ? true : false;
                componentinfocurrent.rotorOH_boolean = String.IsNullOrEmpty(componentinfocurrent.rotorOH) ? false : componentinfocurrent.rotorOH == "Yes" ? true : false;
                componentinfocurrent.drivenbb_boolean = String.IsNullOrEmpty(componentinfocurrent.drivenbb) ? false : componentinfocurrent.drivenbb == "Yes" ? true : false;
                componentinfocurrent.drivenbal_boolean = String.IsNullOrEmpty(componentinfocurrent.drivenbal) ? false : componentinfocurrent.drivenbal == "Yes" ? true : false;

                //excOH <=> exciterOverhungOrSupported (string) optional
                componentinfocurrent.excOH = String.IsNullOrEmpty(componentinfocurrent.excOH) ? null : componentinfocurrent.excOH.Replace("overhung", "overhung");
                componentinfocurrent.excOH = String.IsNullOrEmpty(componentinfocurrent.excOH) ? null : componentinfocurrent.excOH.Replace("Supported", "supported");
                if (componentinfocurrent.excOH == null)
                {
                    //EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; excOH value is NULL", null);
                }
                else
                {
                    if ((componentinfocurrent.excOH.Contains("overhung") == false) &&
                        (componentinfocurrent.excOH.Contains("supported") == false)
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN excOH value {componentinfocurrent.excOH}", null);
                    }
                }

                //Driven_locationType <=> Driven_locationType (string) optional
                componentinfocurrent.Driven_locationType = String.IsNullOrEmpty(componentinfocurrent.Driven_locationType) ? null : componentinfocurrent.Driven_locationType.Replace("NDE", "NDE");
                componentinfocurrent.Driven_locationType = String.IsNullOrEmpty(componentinfocurrent.Driven_locationType) ? null : componentinfocurrent.Driven_locationType.Replace("DE", "DE");
                if (componentinfocurrent.Driven_locationType == null)
                {
                    //EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; Driven_locationType value is NULL", null);
                }
                else
                {
                    if ((componentinfocurrent.Driven_locationType.Contains("NDE") == false) &&
                        (componentinfocurrent.Driven_locationType.Contains("DE") == false)
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN Driven_locationType value {componentinfocurrent.Driven_locationType}", null);
                    }
                }

                //gendriver <=> drivenBy (string)
                componentinfocurrent.gendriver = String.IsNullOrEmpty(componentinfocurrent.gendriver) ? null : componentinfocurrent.gendriver.Replace("Diesel Engine", "dieselengine");
                componentinfocurrent.gendriver = String.IsNullOrEmpty(componentinfocurrent.gendriver) ? null : componentinfocurrent.gendriver.Replace("NOT Diesel Engine", "NOTdieselengine");
                componentinfocurrent.gendriver = String.IsNullOrEmpty(componentinfocurrent.gendriver) ? null : componentinfocurrent.gendriver.Replace("Electric Motor or Turbine", "electricmotororturbine");
                if (componentinfocurrent.gendriver == null)
                {
                    //EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; gendriver value is NULL", null);
                }
                else
                {
                    if ((componentinfocurrent.gendriver.Contains("dieselengine") == false) &&
                        (componentinfocurrent.gendriver.Contains("NOTdieselengine") == false) &&
                        (componentinfocurrent.gendriver.Contains("electricmotororturbine") == false)
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN gendriver value {componentinfocurrent.gendriver}", null);
                    }
                }
                //generatorbars - not available from RPS per Dan White 20210816

                //RotarySlidingVane Pump
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("RotarySlidingVane Pump", "rotaryslidingvanepump");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("RotaryScrew Pump", "rotaryscrewpump");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Gear Pump", "gearpump");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Propeller Pump", "propellerpump");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Centrifugal Pump", "centrifugalpump");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Supported Rotor Fan or Blower", "supportedrotor");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Overhung Rotor Fan or Blower", "overhungrotor");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Lobed Rotor Fan or Blower", "lobedxxx");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Lobed Fan or Blower", "lobed");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Belt Drive Purifier(Centrifuge)", "beltdrive");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Belt Drive Purifier (Centrifuge)", "beltdrive");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Axial Recip Pump", "axialrecip");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Radial Recip Pump", "radialrecip");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Rotary Thread Pump", "rotarythread");
                componentinfocurrent.drivenc = String.IsNullOrEmpty(componentinfocurrent.drivenc) ? null : componentinfocurrent.drivenc.Replace("Screw Pump", "screwpump");
                if (componentinfocurrent.drivenc == null)
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; drivenc value is NULL", null);
                }
                else
                {
                    if (
                        (componentinfocurrent.drivenc.Contains("rotaryslidingvanepump") == false) &&
                        (componentinfocurrent.drivenc.Contains("rotaryscrewpump") == false) &&
                        (componentinfocurrent.drivenc.Contains("gearpump") == false) &&
                        (componentinfocurrent.drivenc.Contains("propellerpump") == false) &&
                        (componentinfocurrent.drivenc.Contains("centrifugalpump") == false) &&
                        (componentinfocurrent.drivenc.Contains("supportedrotor") == false) &&
                        (componentinfocurrent.drivenc.Contains("overhungrotor") == false) &&
                        (componentinfocurrent.drivenc.Contains("lobed") == false) &&
                        (componentinfocurrent.drivenc.Contains("beltdrive") == false) &&
                        (componentinfocurrent.drivenc.Contains("axialrecip") == false) &&
                        (componentinfocurrent.drivenc.Contains("radialrecip") == false) &&
                        (componentinfocurrent.drivenc.Contains("rotarythread") == false)
                        )
                    {
                        EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN drivenc value {componentinfocurrent.drivenc}", null);
                    }
                }

                // actually a driven answer - should be DrivenTbrg
                if (String.IsNullOrEmpty(componentinfocurrent.DriverTbrg) )
                {
                    componentinfocurrent.DriverTbrg = null;
                    //EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; DriverTbrg value is NULL", null);
                }
                else
                {
                    componentinfocurrent.DriverTbrg = componentinfocurrent.DriverTbrg.ToLower();
                    switch (componentinfocurrent.DriverTbrg)
                    {
                        case "yes - ball":
                            componentinfocurrent.DriverTbrg = "ball";
                            break;
                        case "ball":
                            componentinfocurrent.DriverTbrg = "ball";
                            break;
                        case "yes - journal":
                            componentinfocurrent.DriverTbrg = "journal";
                            break;
                        case "journal":
                            componentinfocurrent.DriverTbrg = "journal";
                            break;
                        case "yes":
                            componentinfocurrent.DriverTbrg = "yes";
                            break;
                        case "no":
                            componentinfocurrent.DriverTbrg = "no";
                            break;
                        default:
                            componentinfocurrent.DriverTbrg = "no";
                            EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}; UNKNOWN DriverTbrg {componentinfocurrent.DriverTbrg}", null);
                            break;
                    };
                }
            };
            // Driven Component Map end

            // Orientation Component Map begin
            if (MachineCMBeingProcessed.key == "Orientation Information")
            {
                EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : {MachineCMBeingProcessed.value}", null);
                List<OrientationBRG> brgs = new List<OrientationBRG>();
                brgs.Add(new OrientationBRG { orientationbrgKey = 1, DEOrientation_brg = componentinfocurrent.DEOrientation_brg_1, NDEOrientation_brg = componentinfocurrent.NDEOrientation_brg_1 });
                brgs.Add(new OrientationBRG { orientationbrgKey = 2, DEOrientation_brg = componentinfocurrent.DEOrientation_brg_2, NDEOrientation_brg = componentinfocurrent.NDEOrientation_brg_2 });
                brgs.Add(new OrientationBRG { orientationbrgKey = 3, DEOrientation_brg = componentinfocurrent.DEOrientation_brg_3, NDEOrientation_brg = componentinfocurrent.NDEOrientation_brg_3 });
                brgs.Add(new OrientationBRG { orientationbrgKey = 4, DEOrientation_brg = componentinfocurrent.DEOrientation_brg_4, NDEOrientation_brg = componentinfocurrent.NDEOrientation_brg_4 });
                brgs.Add(new OrientationBRG { orientationbrgKey = 5, DEOrientation_brg = componentinfocurrent.DEOrientation_brg_5, NDEOrientation_brg = componentinfocurrent.NDEOrientation_brg_5 });
                brgs.Add(new OrientationBRG { orientationbrgKey = 6, DEOrientation_brg = componentinfocurrent.DEOrientation_brg_6, NDEOrientation_brg = componentinfocurrent.NDEOrientation_brg_6 });
                brgs.Add(new OrientationBRG { orientationbrgKey = 7, DEOrientation_brg = componentinfocurrent.DEOrientation_brg_7, NDEOrientation_brg = componentinfocurrent.NDEOrientation_brg_7 });
                brgs.Add(new OrientationBRG { orientationbrgKey = 8, DEOrientation_brg = componentinfocurrent.DEOrientation_brg_8, NDEOrientation_brg = componentinfocurrent.NDEOrientation_brg_8 });
                brgs.Add(new OrientationBRG { orientationbrgKey = 9, DEOrientation_brg = componentinfocurrent.DEOrientation_brg_9, NDEOrientation_brg = componentinfocurrent.NDEOrientation_brg_9 });
                brgs.Add(new OrientationBRG { orientationbrgKey = 10, DEOrientation_brg = componentinfocurrent.DEOrientation_brg_10, NDEOrientation_brg = componentinfocurrent.NDEOrientation_brg_10 });

                for (var i = 0; i < 10; i++)
                {
                    if (i < 2)
                    {
                        if (brgs[i].DEOrientation_brg != null) { componentinfocurrent.Driver_DEOrientation = brgs[i].DEOrientation_brg; }
                        if (brgs[i].NDEOrientation_brg != null) { componentinfocurrent.Driver_NDEOrientation = brgs[i].NDEOrientation_brg; }
                    }
                    else
                    {
                        if (brgs[i].DEOrientation_brg != null) { componentinfocurrent.Driven_DEOrientation = brgs[i].DEOrientation_brg; }
                        if (brgs[i].NDEOrientation_brg != null) { componentinfocurrent.Driven_NDEOrientation = brgs[i].NDEOrientation_brg; }
                    }
                }
            };
            // Orientation Component Map end

            if (MachineCMBeingProcessed.key != "Driver" 
                && MachineCMBeingProcessed.key != "Intermediate" 
                && MachineCMBeingProcessed.key != "Driven" 
                && MachineCMBeingProcessed.key != "Orientation Information")
            {
                EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : UNKNOWN machineComponentMap key {MachineCMBeingProcessed.value}", null);
            };

            if ((componentinfocurrent.componentstuff != null) && (componentstuff.ToLower().Contains("not monitored") == false))
            {
                DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(componentinfocurrent, new JsonSerializerOptions { WriteIndented = true }), levelname, $"compinfo_{MachineCMBeingProcessed.key}");
            }
            else 
            {
                EARPSInterfaceServiceLogger.AddLogEntry($"machineComponentMap for {MachineCMBeingProcessed.key} : is null or not monitored {MachineCMBeingProcessed.value}", null);
            }
            ;

            return componentinfocurrent;
        }

    }
}
