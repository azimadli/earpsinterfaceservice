﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessExceptions
    {
        public static void WriteExceptionLine(string filedir, string linetowrite)
        {
            try
            {
                Directory.CreateDirectory(@"jsonfiles\" + filedir);
                if (!File.Exists(@"jsonfiles\" + filedir + @"\ExceptionReport.txt"))
                {
                    using FileStream fs = File.Create(@"jsonfiles\" + filedir + @"\ExceptionReport.txt");
                }

                using StreamWriter outputFile = File.AppendText((Path.Combine(@"jsonfiles\" + filedir, "ExceptionReport.txt")));
                outputFile.WriteLine(linetowrite);
                EARPSInterfaceServiceLogger.AddLogEntry(linetowrite, null);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in WriteExceptionLine", ex);
            }
        }
    }
}

