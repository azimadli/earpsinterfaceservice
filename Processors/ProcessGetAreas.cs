﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDI.Models;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessGetAreas
    {
        public static async Task<List<SAAIDILibrary.Models.Area>> GetAreas(int outgoingcustomer, int outgoingplantid, string outgoingplantname, string levelname)
        {
            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/{outgoingcustomer}/{outgoingplantid}/areas";
            //var jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

            //DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(plant, new JsonSerializerOptions { WriteIndented = true }), levelname, plant.PlantName);

            List <SAAIDILibrary.Models.Area> areas = null;
            using (HttpResponseMessage response = await APIEAHelper.ApiEAClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    areas = await response.Content.ReadAsAsync<List<SAAIDILibrary.Models.Area>>();
                    DebuggingHelper.WriteJsonToFile((await response.Content.ReadAsStringAsync()), levelname, ("areas_in_plant_" + outgoingplantname));
                    EARPSInterfaceServiceLogger.AddLogEntry($"customer: {outgoingcustomer} areas_in_plant: {outgoingplantid} - {outgoingplantname}", null);
                    return areas;
                }
                else
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"customer: {outgoingcustomer} areas_in_plant: {outgoingplantid} - {outgoingplantname} response.ReasonPhrase {response.ReasonPhrase}", null);
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }

}
