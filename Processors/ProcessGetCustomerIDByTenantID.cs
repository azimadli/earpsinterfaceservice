﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using SAAIDILibrary.Models;
using System.Text.Json;
using SAAIDI.Models;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessGetCustomerIDByTenantID
    {
        public static async Task<SAAIDILibrary.Models.Customer> GetCustomerID(Guid tenantID, string levelname)
        {
            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/customers/getByTenantID/{tenantID}";
            var jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

            using (HttpResponseMessage response = await APIEAHelper.ApiEAClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    SAAIDILibrary.Models.Customer customer = null;
                    customer = await response.Content.ReadAsAsync<SAAIDILibrary.Models.Customer>();
                    DebuggingHelper.WriteJsonToFile((await response.Content.ReadAsStringAsync()), levelname, ("CustomerID" + tenantID));
                    EARPSInterfaceServiceLogger.AddLogEntry($"customer: UUID: {tenantID};  ID: {customer.CustomerID} ", null);
                    return customer;
                }
                else
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {tenantID}; response.ReasonPhrase {response.ReasonPhrase}", null);
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }

}
