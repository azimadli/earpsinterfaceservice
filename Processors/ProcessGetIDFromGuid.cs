﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDI.Models;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessGetIDFromGuid
    {
        public static async Task<GetIDFromGuidResponse> GetAssetIDFromGuid(Guid tenantID, Guid assetUUID, int assetTypeID, string levelname)
        {
            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/{tenantID}/asset/getIdFromGuid";

            var idrequest = new GetIDFromGuidRequest();
            idrequest.assetUUID = assetUUID;
            idrequest.assetTypeID = assetTypeID;
            var idrequestjson = System.Text.Json.JsonSerializer.Serialize(idrequest);

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(url),
                Content = new StringContent(idrequestjson, Encoding.UTF8, "application/json") // MediaTypeNames.Application.Json /* or "application/json" in older versions */),
            };

            var response = await APIEAHelper.ApiEAClient.SendAsync(request).ConfigureAwait(false);
            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode)
            {
                GetIDFromGuidResponse reponseAssetID = null;
                reponseAssetID = await response.Content.ReadAsAsync<EARPSInterfaceService.Models.GetIDFromGuidResponse>().ConfigureAwait(false);

                EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {tenantID} AssetUUID: {assetUUID}; type: {assetTypeID}", null);
                return reponseAssetID;

            }
            else
            {
                EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {tenantID} AssetUUID: {assetUUID}; type: {assetTypeID}; Error - { response.ReasonPhrase}", null);
                throw new Exception(response.ReasonPhrase);
            }
        }
    }

}
