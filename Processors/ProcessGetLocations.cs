﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDI.Models;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessGetLocations
    {
        public static async Task<List<SAAIDILibrary.Models.Location>> GetLocations(int outgoingcustomer, int outgoingmachineid, string outgoingmachinename, string levelname)
        {
            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/{outgoingcustomer}/{outgoingmachineid}/locations";
            List <SAAIDILibrary.Models.Location> locations = null;
            using (HttpResponseMessage response = await APIEAHelper.ApiEAClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    locations = await response.Content.ReadAsAsync<List<SAAIDILibrary.Models.Location>>();
                    DebuggingHelper.WriteJsonToFile((await response.Content.ReadAsStringAsync()), levelname, ("locations_in_machine_" + outgoingmachinename));
                    EARPSInterfaceServiceLogger.AddLogEntry($"customer: {outgoingcustomer} locations_in_machine: {outgoingmachineid} - {outgoingmachinename}", null);
                    return locations;
                }
                else
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"customer: {outgoingcustomer} locations_in_machine: {outgoingmachineid} - {outgoingmachinename} response.ReasonPhrase {response.ReasonPhrase}", null);
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }

}
