﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
//using Microsoft.Extensions.Logging;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDI.Models;
using NLog;
using NLog.Fluent;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessGetMIDBymidId
    {
        public static async Task<SAAIDILibrary.Models.MID> GetMID(int customerID, int midId, int machineid, string machinename, string levelname)
        {
            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/{customerID}/mid/{midId}";
            var jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

            //DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(mid, new JsonSerializerOptions { WriteIndented = true }), levelname, ("postMIDRequest" + mid.MIDName));

            using (HttpResponseMessage response = await APIEAHelper.ApiEAClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    SAAIDILibrary.Models.MID mid = null;
                    mid = await response.Content.ReadAsAsync<SAAIDILibrary.Models.MID>();
                    DebuggingHelper.WriteJsonToFile((await response.Content.ReadAsStringAsync()), levelname, ("MID_" + mid.MIDName + "_" + midId));
                    EARPSInterfaceServiceLogger.AddLogEntry($"customer: {customerID} MID: {midId} for machine: {machineid} - {machinename}", null);
                    return mid;

                }
                else
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"TenantID: {customerID} MID NOT retrieved: {midId} for machine: {machineid} - {machinename} {response.ReasonPhrase}", null);
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }

}
