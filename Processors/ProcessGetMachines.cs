﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDI.Models;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessGetMachines
    {
        public static async Task<List<SAAIDILibrary.Models.Machine>> GetMachines(int outgoingcustomer, int outgoingareaid, string outgoingareaname, string levelname)
        {
            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/{outgoingcustomer}/{outgoingareaid}/machines";
            List <SAAIDILibrary.Models.Machine> machines = null;
            using (HttpResponseMessage response = await APIEAHelper.ApiEAClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    machines = await response.Content.ReadAsAsync<List<SAAIDILibrary.Models.Machine>>();
                    DebuggingHelper.WriteJsonToFile((await response.Content.ReadAsStringAsync()), levelname, ("machines_in_area_" + outgoingareaname));
                    EARPSInterfaceServiceLogger.AddLogEntry($"customer: {outgoingcustomer} machines_in_area: {outgoingareaid} - {outgoingareaname}", null);
                    return machines;
                }
                else
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"customer: {outgoingcustomer} machines_in_area: {outgoingareaid}  - {outgoingareaname} response.ReasonPhrase {response.ReasonPhrase}", null);
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }

}
