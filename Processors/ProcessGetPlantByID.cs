﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDI.Models;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessGetPlantByID
    {
        public static async Task<SAAIDILibrary.Models.Plant> GetPlantByID(int outgoingcustomer, int outgoingplant, string levelname)
        {
            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/{outgoingcustomer}/plants/{outgoingplant}";
            var jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

            //DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(plant, new JsonSerializerOptions { WriteIndented = true }), levelname, plant.PlantName);

            using (HttpResponseMessage response = await APIEAHelper.ApiEAClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    SAAIDILibrary.Models.Plant plant = null;
                    plant = await response.Content.ReadAsAsync<SAAIDILibrary.Models.Plant>();
                    DebuggingHelper.WriteJsonToFile((await response.Content.ReadAsStringAsync()), levelname, ("plant_" + outgoingcustomer));
                    EARPSInterfaceServiceLogger.AddLogEntry($"customer: {outgoingcustomer} plant: {outgoingplant}", null);
                    return plant;

                }
                else
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"customer: {outgoingcustomer} plant: {outgoingplant} response.ReasonPhrase {response.ReasonPhrase}", null);
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }

}
