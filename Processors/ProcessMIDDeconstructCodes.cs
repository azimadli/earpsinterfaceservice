﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MIDCodeGenerationLibrary.Models;

namespace EARPSInterfaceService.Processors
{
    public class ProcessMIDDeconstructCodes
    {
        public static Task<MachineComponentsForMIDgeneration> DeconstructMID(SAAIDILibrary.Models.MID MID)
        {
            MachineComponentsForMIDgeneration deconcoderesults = new MachineComponentsForMIDgeneration();
            return Task.FromResult(deconcoderesults);
        }
    }
}
