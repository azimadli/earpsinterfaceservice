﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessOtherFileWrites
    {
        public static void WriteOtherFileLine(string otherfiledir, string otherfilename, string otherfilelinetowrite)
        {
            try
            {
                Directory.CreateDirectory(@"jsonfiles\" + otherfiledir);
                if (!File.Exists(@"jsonfiles\" + otherfiledir + @"\" + otherfilename))
                {
                    using FileStream fs = File.Create(@"jsonfiles\" + otherfiledir + @"\" + otherfilename);
                }

                using StreamWriter outputFile = File.AppendText((Path.Combine(@"jsonfiles\" + otherfiledir, otherfilename)));
                outputFile.WriteLine(otherfilelinetowrite);
                EARPSInterfaceServiceLogger.AddLogEntry(otherfilelinetowrite, null);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in WriteOtherFileLine", ex);
            }
        }
    }
}

