﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Linq;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using Newtonsoft.Json;
using EARPSInterfaceService.Logger;
using System.IO;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;
using MIDCodeGenerationLibrary.Models;

namespace EARPSInterfaceService.Processors
{
    class ProcessPickupCodes
    {
        public static machinelocs SetupPickupCodes(MIDCodeCreatorRequest midstuff, ComponentInfo componentinfointermediate)
        {
            machinelocs ml = new machinelocs();
            //Process Pickup Codes
            //Process Driver Pickup Codes
            if (midstuff.machineComponentsForMIDgeneration.driver != null && midstuff.machineComponentsForMIDgeneration.driver.driverLocationNDE == true) { ml.drvr_loc_01 = 1; ml.lastlocvalueUsed = 1; ml.lastlocvalueUseddrvr = ml.drvr_loc_01; } else { ml.drvr_loc_01 = 0; };
            if (midstuff.machineComponentsForMIDgeneration.driver != null && midstuff.machineComponentsForMIDgeneration.driver.driverLocationDE == true) { ml.drvr_loc_02 = 2; ml.lastlocvalueUsed = 2; ml.lastlocvalueUseddrvr = ml.drvr_loc_02; } else { ml.drvr_loc_02 = 0; };
            if (ml.drvr_loc_01 > 0)
            {
                ml.firstlocvalueUseddrvr = ml.drvr_loc_01;
            }
            else
            {
                if (ml.drvr_loc_02 > 0)
                {
                    ml.firstlocvalueUseddrvr = ml.drvr_loc_02;
                }
            }

            //Process Intermediate Pickup Codes
            short interloc1_short = (short)(String.IsNullOrEmpty(componentinfointermediate.location_1) ? 0 : int.Parse(componentinfointermediate.location_1.Substring(4)));
            short interloc2_short = (short)(String.IsNullOrEmpty(componentinfointermediate.location_2) ? 0 : int.Parse(componentinfointermediate.location_2.Substring(4)));
            short interloc3_short = (short)(String.IsNullOrEmpty(componentinfointermediate.location_3) ? 0 : int.Parse(componentinfointermediate.location_3.Substring(4)));
            short interloc4_short = (short)(String.IsNullOrEmpty(componentinfointermediate.location_4) ? 0 : int.Parse(componentinfointermediate.location_4.Substring(4)));
            if (interloc1_short != 0) { ml.intm_loc_01 = interloc1_short; ml.lastlocvalueUsed = interloc1_short; ml.firstlocvalueUsedintm = interloc1_short; ml.lastlocvalueUsedintm = interloc1_short; } else { ml.intm_loc_01 = 0; };
            if (interloc2_short != 0) { ml.intm_loc_02 = interloc2_short; ml.lastlocvalueUsed = interloc2_short; ml.lastlocvalueUsedintm = interloc2_short; } else { ml.intm_loc_02 = 0; };
            if (interloc3_short != 0) { ml.intm_loc_03 = interloc3_short; ml.lastlocvalueUsed = interloc3_short; ml.lastlocvalueUsedintm = interloc3_short; } else { ml.intm_loc_03 = 0; };
            if (interloc4_short != 0) { ml.intm_loc_04 = interloc4_short; ml.lastlocvalueUsed = interloc4_short; ml.lastlocvalueUsedintm = interloc4_short; } else { ml.intm_loc_04 = 0; };

            //Process Driven Pickup Codes
            // fix  Here is where you could set the first driven pickup code to 7
            if (midstuff.machineComponentsForMIDgeneration.driven != null && midstuff.machineComponentsForMIDgeneration.driven.drivenLocationNDE == true) { ml.drvn_loc_01 = ++ml.lastlocvalueUsed; ml.firstlocvalueUseddrvn = ml.drvn_loc_01; ml.lastlocvalueUseddrvn = ml.drvn_loc_01; } else { ml.drvn_loc_01 = 0; };
            if (midstuff.machineComponentsForMIDgeneration.driven != null && midstuff.machineComponentsForMIDgeneration.driven.drivenLocationDE == true) { ml.drvn_loc_02 = ++ml.lastlocvalueUsed; if (ml.firstlocvalueUseddrvn == 0) { ml.firstlocvalueUseddrvn = ml.drvn_loc_02; }; ml.lastlocvalueUseddrvn = ml.drvn_loc_02; } else { ml.drvn_loc_02 = 0; };

            if (midstuff.machineComponentsForMIDgeneration.coupling1 != null) 
            {
                if (ml.lastlocvalueUseddrvr > 0)
                {
                    ml.cpl1_loc_01 = ml.lastlocvalueUseddrvr;
                }
                if (ml.lastlocvalueUsedintm > 0)
                {
                    ml.cpl1_loc_02 = ml.firstlocvalueUsedintm;
                }
                else
                {
                    ml.cpl1_loc_02 = ml.firstlocvalueUseddrvn;
                }
            }

            if (midstuff.machineComponentsForMIDgeneration.coupling2 != null && ml.lastlocvalueUseddrvn > 0 && ml.lastlocvalueUsedintm > 0)
            {
                ml.cpl2_loc_02 = ml.lastlocvalueUseddrvn;
                ml.cpl2_loc_01 = ml.lastlocvalueUsedintm;
            }

            if (ml.lastlocvalueUseddrvr > 0)
            {
                if (ml.firstlocvalueUsedintm > 0)
                {
                    ml.drvr_loc_extra = ml.firstlocvalueUsedintm;
                }
                else
                {
                    if (ml.firstlocvalueUseddrvn > 0)
                    {
                        ml.drvr_loc_extra = ml.firstlocvalueUseddrvn;
                    }
                    else
                    {
                        ml.drvr_loc_extra = 0;
                    }
                }
            }
            if (ml.lastlocvalueUseddrvn > 0)
            {
                if (ml.lastlocvalueUsedintm > 0)
                {
                    ml.drvn_loc_extra = ml.lastlocvalueUsedintm;
                }
                else
                {
                    if (ml.lastlocvalueUseddrvr > 0)
                    {
                        ml.drvn_loc_extra = ml.lastlocvalueUseddrvr;
                    }
                    else
                    {
                        ml.drvn_loc_extra = 0;
                    }
                }
            }

            //Output - Setup Pickup Codes
            //Driver Setup Pickup Codes
            int[] drvrpickupcodearray = { ml.drvr_loc_01, ml.drvr_loc_02, ml.drvr_loc_extra };
            ml.drvrpickupcode = string.Join(",", from loc in drvrpickupcodearray where loc > 0 select loc);

            ml.drvrpickup1 = 0;
            ml.drvrpickup2 = 0;
            if (ml.drvr_loc_01 > 0)
            {
                ml.drvrpickup1 = ml.drvr_loc_01;
                if (ml.drvr_loc_02 > 0)
                {
                    ml.drvrpickup2 = ml.drvr_loc_02;
                }
            }
            else
            {
                if (ml.drvr_loc_02 > 0)
                {
                    ml.drvrpickup1 = ml.drvr_loc_02;
                }
            }
            ml.drvrpickup3 = 0;
            ml.drvrpickup4 = 0;

            //Coupling1 Setup Pickup Codes

            int[] cpl1pickupcodearray = { ml.cpl1_loc_01, ml.cpl1_loc_02 };
            ml.cpl1pickupcode = string.Join(",", from loc in cpl1pickupcodearray where loc > 0 select loc);

            ml.cpl1pickup1 = 0;
            ml.cpl1pickup2 = 0;
            ml.cpl1pickup3 = 0;
            ml.cpl1pickup4 = 0;

            //Intermediate Setup Pickup Codes

            int[] intmpickupcodearray = { ml.intm_loc_01, ml.intm_loc_02, ml.intm_loc_03, ml.intm_loc_04 };
            ml.intmpickupcode = string.Join(",", from loc in intmpickupcodearray where loc > 0 select loc);

            ml.intmpickup1 = ml.intm_loc_01;
            ml.intmpickup2 = ml.intm_loc_02;
            ml.intmpickup3 = ml.intm_loc_03;
            ml.intmpickup4 = ml.intm_loc_04;

            //Coupling2 Setup Pickup Codes

            int[] cpl2pickupcodearray = { ml.cpl2_loc_01, ml.cpl2_loc_02 };
            ml.cpl2pickupcode = string.Join(",", from loc in cpl2pickupcodearray where loc > 0 select loc);

            ml.cpl2pickup1 = 0;
            ml.cpl2pickup2 = 0;
            ml.cpl2pickup3 = 0;
            ml.cpl2pickup4 = 0;

            //Driven Setup Pickup Codes

            int[] drvnpickupcodearray = { ml.drvn_loc_02, ml.drvn_loc_01, ml.drvn_loc_extra };
            ml.drvnpickupcode = string.Join(",", from loc in drvnpickupcodearray where loc > 0 select loc);

            ml.drvnpickup1 = 0;
            ml.drvnpickup2 = 0;
            if (ml.drvn_loc_01 > 0)
            {
                ml.drvnpickup1 = ml.drvn_loc_01;
                if (ml.drvn_loc_02 > 0)
                {
                    ml.drvnpickup2 = ml.drvn_loc_02;
                }
            }
            else
            {
                if (ml.drvn_loc_02 > 0)
                {
                    ml.drvnpickup1 = ml.drvn_loc_02;
                }
            }
            ml.drvnpickup3 = 0;
            ml.drvnpickup4 = 0;

            bool driverNDE = false;  
            bool driverDE = false;  
            bool drivenDE = false; 
            bool drivenNDE = false;

            if (midstuff.machineComponentsForMIDgeneration.driver != null)
            {
                driverNDE = (bool)midstuff.machineComponentsForMIDgeneration.driver.driverLocationNDE;
                driverDE = (bool)midstuff.machineComponentsForMIDgeneration.driver.driverLocationDE;
            }
            if (midstuff.machineComponentsForMIDgeneration.driven != null)
            {
                drivenDE = (bool)midstuff.machineComponentsForMIDgeneration.driven.drivenLocationDE;
                drivenNDE = (bool)midstuff.machineComponentsForMIDgeneration.driven.drivenLocationNDE;
            }
            EARPSInterfaceServiceLogger.AddLogEntry($"Driver: {ml.drvrpickupcode}; {ml.drvrpickup1}; {ml.drvrpickup2}; {ml.drvrpickup3}; {ml.drvrpickup4}; first: {ml.firstlocvalueUseddrvr}; last: {ml.lastlocvalueUseddrvr}; extra: {ml.drvr_loc_extra}", null);
            EARPSInterfaceServiceLogger.AddLogEntry($"Coupl1: {ml.cpl1pickupcode}; {ml.cpl1pickup1}; {ml.cpl1pickup2}; {ml.cpl1pickup3}; {ml.cpl1pickup4}; ", null);
            EARPSInterfaceServiceLogger.AddLogEntry($"Interm: {ml.intmpickupcode}; {ml.intmpickup1}; {ml.intmpickup2}; {ml.intmpickup3}; {ml.intmpickup4}; first: {ml.firstlocvalueUsedintm}; last: {ml.lastlocvalueUsedintm} ", null);
            EARPSInterfaceServiceLogger.AddLogEntry($"Coupl2: {ml.cpl2pickupcode}; {ml.cpl2pickup1}; {ml.cpl2pickup2}; {ml.cpl2pickup3}; {ml.cpl2pickup4}; ", null);
            EARPSInterfaceServiceLogger.AddLogEntry($"Driven: {ml.drvnpickupcode}; {ml.drvnpickup1}; {ml.drvnpickup2}; {ml.drvnpickup3}; {ml.drvnpickup4}; first: {ml.firstlocvalueUseddrvn}; last: {ml.lastlocvalueUseddrvn}; extra: {ml.drvn_loc_extra}", null);
            return ml;
        }
    }
}
