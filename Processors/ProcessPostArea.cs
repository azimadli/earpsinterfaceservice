﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDILibrary.Models;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessPostArea
    {
        public static async Task<AreaResponse> PostArea(SAAIDILibrary.Models.Area area, Guid tenantID, string levelname)
        {

            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/{tenantID}/areas";
            var jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

            DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(area, new JsonSerializerOptions { WriteIndented = true }), levelname, area.AreaName);

            using (HttpResponseMessage response = await APIEAHelper.ApiEAClient.PostAsJsonAsync(url, area))
            {
                if (response.IsSuccessStatusCode)
                {
                    AreaResponse reponsearea = await response.Content.ReadAsAsync<EARPSInterfaceService.Models.AreaResponse>();
                    DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(reponsearea.data), levelname, ("postMachineResponse" + area.AreaName));

                    EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {tenantID} Area added: {reponsearea.data.AreaName}; {reponsearea.data.AreaID}; {reponsearea.data.AreaUUID};", null);
                    return reponsearea;

                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }

}
