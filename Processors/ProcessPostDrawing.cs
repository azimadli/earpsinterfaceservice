﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDILibrary.Models;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessPostDrawing
    {
        public static async Task<DrawingResponse> PostDrawing(DrawingRequest drawing, Guid tenantID, string levelname)
        {

            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/{tenantID}/drawings";
            var jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

            DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(drawing, new JsonSerializerOptions { WriteIndented = true }), levelname, "postDrawingRequest");

            using (HttpResponseMessage response = await APIEAHelper.ApiEAClient.PostAsJsonAsync(url, drawing))
            {
                if (response.IsSuccessStatusCode)
                {
                    DrawingResponse reponsedrawing = await response.Content.ReadAsAsync<EARPSInterfaceService.Models.DrawingResponse>();
                    DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(reponsedrawing.data), levelname, "postDrawingResponse");

                    EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {tenantID} Drawing added: {reponsedrawing.data.DrawingID}; {reponsedrawing.data.DrawingID}; {reponsedrawing.data.DrawingName};", null);
                    return reponsedrawing;

                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }

}
