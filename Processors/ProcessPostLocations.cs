﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDI.Models;
using EARPSInterfaceService.Logger;
using System.IO;

namespace EARPSInterfaceService.Processors
{
    class ProcessPostLocations
    {
        public static async Task<HttpResponseMessage> PostLocation(Models.LocationReqest location, Guid tenantID, string levelname)
        {
            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/{tenantID}/locations";
            var jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

            DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(location, new JsonSerializerOptions { WriteIndented = true }), levelname, location.LocationName);

            using (HttpResponseMessage response = await APIEAHelper.ApiEAClient.PostAsJsonAsync(url, location))
            {
                if (response.IsSuccessStatusCode)
                {
                    //LocationResponse reponselocation = await response.Content.ReadAsAsync<EARPSInterfaceService.Models.LocationResponse>();
                    //DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(reponselocation.data), levelname, ("postLocationResponse" + location.LocationName));

                    EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {tenantID} Location added: {location.LocationName};", null);
                    return response;
                }
                else
                {
                    //CaptureResponse reponsewithmessage = await response.Content.ReadAsAsync<EARPSInterfaceService.Models.CaptureResponse>();
                    //EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {tenantID} Location add error: {reponsewithmessage.message}", null);
                    //return response;
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }

}
