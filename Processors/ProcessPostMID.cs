﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
//using Microsoft.Extensions.Logging;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDI.Models;
using NLog;
using NLog.Fluent;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessPostMID
    {
        public static async Task<MIDResponse> PostMID(SAAIDILibrary.Models.MID mid, Guid tenantID, string levelname)
        {
            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/{tenantID}/mid";
            var jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

            DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(mid, new JsonSerializerOptions { WriteIndented = true }), levelname, ("postMIDRequest" + mid.MIDName));

            using (HttpResponseMessage response = await APIEAHelper.ApiEAClient.PostAsJsonAsync(url, mid))
            {
                if (response.IsSuccessStatusCode)
                {
                    MIDResponse reponsemid = await response.Content.ReadAsAsync<EARPSInterfaceService.Models.MIDResponse>();
                    DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(reponsemid.data), levelname, ("postMIDResponse" + mid.MIDName));

                    EARPSInterfaceServiceLogger.AddLogEntry($"TenantID: {tenantID} MID added: {reponsemid.data.MIDName}; {reponsemid.data.MIDID}; {reponsemid.data.MIDUUID};", null);
                    return reponsemid;
                }
                else
                {
                    //EARPSInterfaceServiceLogger.AddLogEntry($"TenantID: {tenantID} MID NOT added: {response.ReasonPhrase}", null);
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }

}
