﻿using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
//using Microsoft.Extensions.Logging;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDI.Models;
using NLog;
using NLog.Fluent;
using EARPSInterfaceService.Logger;
using System;
using System.Collections.Generic;
using MIDCodeGeneration.Models.APIResponse;
using MIDCodeGenerationLibrary.Processes;
using MIDCodeGenerationLibrary.Models;
using Microsoft.AspNetCore.Mvc;
using MIDCodeGeneration.Models.DrivenModels;
using Newtonsoft.Json;
using System.Linq;
using MIDCodeGeneration.Models.CodeDeconstructionModels;



namespace EARPSInterfaceService.Processors
{
    class ProcessPostMIDCodeGenerationLibrary
    {
        public static Task<MIDCodeDetails> PostMIDCodeGeneration(MIDCodeCreatorRequest midcomp, Guid tenantID, string machinename, string levelname, string levelnametop)
        {
            MIDCodeDetails midcoderesults = new MIDCodeDetails();
            try
            {
                DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(midcomp, new JsonSerializerOptions { WriteIndented = true }), levelname, "midcomp");

                Task<ActionResult> compCodeResults = Task.Run<ActionResult>(async () => await ProcessMIDCodes.GenerateCodes(midcomp));

                object result = compCodeResults.Result;
                var okresult = result as OkObjectResult;
                if (okresult == null)
                {
                    //cast as badrequest

                    var badRequest = result as BadRequestObjectResult;
                    ApiBadRequest400Response badReqResponse = badRequest.Value as ApiBadRequest400Response;
                    var badRequestresult = JsonConvert.SerializeObject(badReqResponse.ValidationErrors);
                    var stringoferrors = "badRequest.StatusCode: " + badRequest.StatusCode.ToString() + "; bad request errors: ";
                    foreach (MIDCodeGeneration.Models.APIResponse.ApiBadRequest400Response.ValidationError v in badReqResponse.ValidationErrors.ToList())
                    { stringoferrors = stringoferrors + "errfield: " + v.Field + ", errmessage: " + v.Message + "; "; }
                    ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.  Reason: {stringoferrors}");
                    EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.  ValidationErrors: {stringoferrors}", null);
                    return Task.FromResult(midcoderesults = null);
                }
                else
                {
                    ApiOkResponse apiokresponse = okresult.Value as ApiOkResponse;
                    midcoderesults = JsonConvert.DeserializeObject<MIDCodeDetails>(apiokresponse.Result.ToString());
                    DebuggingHelper.WriteJsonToFile(apiokresponse.Result.ToString(), levelname, "midresponse");
                    return Task.FromResult(midcoderesults);
                }

            }
            catch (Exception ex)
            {
                ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.");
                EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.", ex);
                return Task.FromResult(midcoderesults = null);
            }
        }
        public static Task<MachineComponentsForMIDgeneration> PostMIDCodeDeconstruction(MIDCodeDeconstructionRequest middecon, Guid tenantID, string machinename, string levelname, string levelnametop)
        {
            MachineComponentsForMIDgeneration deconcoderesults = new MachineComponentsForMIDgeneration();
            try
            {
                DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(middecon, new JsonSerializerOptions { WriteIndented = true }), levelname, $"middecon_{machinename}");

                Task<ActionResult> compCodeResults = Task.Run<ActionResult>(async () => await ProcessMIDDeconstruction.DeconstructCodes(middecon));

                object result = compCodeResults.Result;
                var okresult = result as OkObjectResult;
                if (okresult == null)
                {
                    //cast as badrequest

                    var badRequest = result as BadRequestObjectResult;
                    ApiBadRequest400Response badReqResponse = badRequest.Value as ApiBadRequest400Response;
                    var badRequestresult = JsonConvert.SerializeObject(badReqResponse.ValidationErrors);
                    var stringoferrors = "badRequest.StatusCode: " + badRequest.StatusCode.ToString() + "; bad request errors: ";
                    foreach (MIDCodeGeneration.Models.APIResponse.ApiBadRequest400Response.ValidationError v in badReqResponse.ValidationErrors.ToList())
                    { stringoferrors = stringoferrors + "errfield: " + v.Field + ", errmessage: " + v.Message + "; "; }
                    ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.  Reason: {stringoferrors}");
                    EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.  ValidationErrors: {stringoferrors}", null);
                    return Task.FromResult(deconcoderesults = null);
                }
                else
                {
                    ApiOkResponse apiokresponse = okresult.Value as ApiOkResponse;
                    deconcoderesults = JsonConvert.DeserializeObject<MachineComponentsForMIDgeneration>(apiokresponse.Result.ToString());
                    DebuggingHelper.WriteJsonToFile(apiokresponse.Result.ToString(), levelname, "deconcoderesults" + "_" + machinename);
                    return Task.FromResult(deconcoderesults);
                }

            }
            catch (Exception ex)
            {
                ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.");
                EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.", ex);
                return Task.FromResult(deconcoderesults = null);
            }
        }
        public static Task<List<MIDCodeGeneration.Models.DetailModels.Driver>> GetComponentDetailsDriver(string componentsubType, int detailid, Guid tenantID, string machinename, string levelname, string levelnametop)
        {
            List<MIDCodeGeneration.Models.DetailModels.Driver> componentdetailsresultsdrivers = new List<MIDCodeGeneration.Models.DetailModels.Driver>();
            try
            {
                //DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(middecon, new JsonSerializerOptions { WriteIndented = true }), levelname, "middecon" + "_" + machinename);

                Task<ActionResult> compCodeResults = Task.Run<ActionResult>(async () => await ProcessMIDDetails.GenerateDetails("Driver", componentsubType, detailid));

                object result = compCodeResults.Result;
                var okresult = result as OkObjectResult;
                if (okresult == null)
                {
                    //cast as badrequest

                    var badRequest = result as BadRequestObjectResult;
                    ApiBadRequest400Response badReqResponse = badRequest.Value as ApiBadRequest400Response;
                    var badRequestresult = JsonConvert.SerializeObject(badReqResponse.ValidationErrors);
                    var stringoferrors = "badRequest.StatusCode: " + badRequest.StatusCode.ToString() + "; bad request errors: ";
                    foreach (MIDCodeGeneration.Models.APIResponse.ApiBadRequest400Response.ValidationError v in badReqResponse.ValidationErrors.ToList())
                    { stringoferrors = stringoferrors + "errfield: " + v.Field + ", errmessage: " + v.Message + "; "; }
                    ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.  Reason: {stringoferrors}");
                    EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.  ValidationErrors: {stringoferrors}", null);
                    return Task.FromResult(componentdetailsresultsdrivers = null);
                }
                else
                {
                    ApiOkResponse apiokresponse = okresult.Value as ApiOkResponse;

                    //JsonConvert.DeserializeObject<List<CustomerJson>>(json);
                    //List<Order> ObjOrderList = JsonConvert.DeserializeObject<List<Order>>(orderJson);

                    componentdetailsresultsdrivers = JsonConvert.DeserializeObject<List<MIDCodeGeneration.Models.DetailModels.Driver>>(apiokresponse.Result.ToString());
                    DebuggingHelper.WriteJsonToFile(apiokresponse.Result.ToString(), levelname, "componentdetailsresultsdriver" + "_" + machinename);
                    return Task.FromResult(componentdetailsresultsdrivers);
                }

            }
            catch (Exception ex)
            {
                ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.");
                EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.", ex);
                return Task.FromResult(componentdetailsresultsdrivers = null);
            }
        }
        public static Task<List<MIDCodeGeneration.Models.DetailModels.Driven>> GetComponentDetailsDriven(string componentsubType, int detailid, Guid tenantID, string machinename, string levelname, string levelnametop)
        {
            List<MIDCodeGeneration.Models.DetailModels.Driven> componentdetailsresultsdrivens = new List<MIDCodeGeneration.Models.DetailModels.Driven>();
            try
            {
                //DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(middecon, new JsonSerializerOptions { WriteIndented = true }), levelname, "middecon" + "_" + machinename);

                Task<ActionResult> compCodeResults = Task.Run<ActionResult>(async () => await ProcessMIDDetails.GenerateDetails("Driven", componentsubType, detailid));

                object result = compCodeResults.Result;
                var okresult = result as OkObjectResult;
                if (okresult == null)
                {
                    //cast as badrequest

                    var badRequest = result as BadRequestObjectResult;
                    ApiBadRequest400Response badReqResponse = badRequest.Value as ApiBadRequest400Response;
                    var badRequestresult = JsonConvert.SerializeObject(badReqResponse.ValidationErrors);
                    var stringoferrors = "badRequest.StatusCode: " + badRequest.StatusCode.ToString() + "; bad request errors: ";
                    foreach (MIDCodeGeneration.Models.APIResponse.ApiBadRequest400Response.ValidationError v in badReqResponse.ValidationErrors.ToList())
                    { stringoferrors = stringoferrors + "errfield: " + v.Field + ", errmessage: " + v.Message + "; "; }
                    ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.  Reason: {stringoferrors}");
                    EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.  ValidationErrors: {stringoferrors}", null);
                    return Task.FromResult(componentdetailsresultsdrivens = null);
                }
                else
                {
                    ApiOkResponse apiokresponse = okresult.Value as ApiOkResponse;

                    //JsonConvert.DeserializeObject<List<CustomerJson>>(json);
                    //List<Order> ObjOrderList = JsonConvert.DeserializeObject<List<Order>>(orderJson);

                    componentdetailsresultsdrivens = JsonConvert.DeserializeObject<List<MIDCodeGeneration.Models.DetailModels.Driven>>(apiokresponse.Result.ToString());
                    DebuggingHelper.WriteJsonToFile(apiokresponse.Result.ToString(), levelname, "componentdetailsresultsdriven" + "_" + machinename);
                    return Task.FromResult(componentdetailsresultsdrivens);
                }

            }
            catch (Exception ex)
            {
                ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.");
                EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.", ex);
                return Task.FromResult(componentdetailsresultsdrivens = null);
            }
        }
        public static Task<List<MIDCodeGeneration.Models.DetailModels.Coupling1>> GetComponentDetailsCoupling1(string componentsubType, int detailid, Guid tenantID, string machinename, string levelname, string levelnametop)
        {
            List<MIDCodeGeneration.Models.DetailModels.Coupling1> componentdetailsresultsCoupling1s = new List<MIDCodeGeneration.Models.DetailModels.Coupling1>();
            try
            {
                //DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(middecon, new JsonSerializerOptions { WriteIndented = true }), levelname, "middecon" + "_" + machinename);

                Task<ActionResult> compCodeResults = Task.Run<ActionResult>(async () => await ProcessMIDDetails.GenerateDetails("Coupling1", componentsubType, detailid));

                object result = compCodeResults.Result;
                var okresult = result as OkObjectResult;
                if (okresult == null)
                {
                    //cast as badrequest

                    var badRequest = result as BadRequestObjectResult;
                    ApiBadRequest400Response badReqResponse = badRequest.Value as ApiBadRequest400Response;
                    var badRequestresult = JsonConvert.SerializeObject(badReqResponse.ValidationErrors);
                    var stringoferrors = "badRequest.StatusCode: " + badRequest.StatusCode.ToString() + "; bad request errors: ";
                    foreach (MIDCodeGeneration.Models.APIResponse.ApiBadRequest400Response.ValidationError v in badReqResponse.ValidationErrors.ToList())
                    { stringoferrors = stringoferrors + "errfield: " + v.Field + ", errmessage: " + v.Message + "; "; }
                    ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.  Reason: {stringoferrors}");
                    EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.  ValidationErrors: {stringoferrors}", null);
                    return Task.FromResult(componentdetailsresultsCoupling1s = null);
                }
                else
                {
                    ApiOkResponse apiokresponse = okresult.Value as ApiOkResponse;

                    //JsonConvert.DeserializeObject<List<CustomerJson>>(json);
                    //List<Order> ObjOrderList = JsonConvert.DeserializeObject<List<Order>>(orderJson);

                    componentdetailsresultsCoupling1s = JsonConvert.DeserializeObject<List<MIDCodeGeneration.Models.DetailModels.Coupling1>>(apiokresponse.Result.ToString());
                    DebuggingHelper.WriteJsonToFile(apiokresponse.Result.ToString(), levelname, "componentdetailsresultscoupling1" + "_" + machinename);
                    return Task.FromResult(componentdetailsresultsCoupling1s);
                }

            }
            catch (Exception ex)
            {
                ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.");
                EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.", ex);
                return Task.FromResult(componentdetailsresultsCoupling1s = null);
            }
        }
        public static Task<List<MIDCodeGeneration.Models.DetailModels.Coupling2>> GetComponentDetailsCoupling2(string componentsubType, int detailid, Guid tenantID, string machinename, string levelname, string levelnametop)
        {
            List<MIDCodeGeneration.Models.DetailModels.Coupling2> componentdetailsresultsCoupling2s = new List<MIDCodeGeneration.Models.DetailModels.Coupling2>();
            try
            {
                //DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(middecon, new JsonSerializerOptions { WriteIndented = true }), levelname, "middecon" + "_" + machinename);

                Task<ActionResult> compCodeResults = Task.Run<ActionResult>(async () => await ProcessMIDDetails.GenerateDetails("Coupling2", componentsubType, detailid));

                object result = compCodeResults.Result;
                var okresult = result as OkObjectResult;
                if (okresult == null)
                {
                    //cast as badrequest

                    var badRequest = result as BadRequestObjectResult;
                    ApiBadRequest400Response badReqResponse = badRequest.Value as ApiBadRequest400Response;
                    var badRequestresult = JsonConvert.SerializeObject(badReqResponse.ValidationErrors);
                    var stringoferrors = "badRequest.StatusCode: " + badRequest.StatusCode.ToString() + "; bad request errors: ";
                    foreach (MIDCodeGeneration.Models.APIResponse.ApiBadRequest400Response.ValidationError v in badReqResponse.ValidationErrors.ToList())
                    { stringoferrors = stringoferrors + "errfield: " + v.Field + ", errmessage: " + v.Message + "; "; }
                    ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.  Reason: {stringoferrors}");
                    EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.  ValidationErrors: {stringoferrors}", null);
                    return Task.FromResult(componentdetailsresultsCoupling2s = null);
                }
                else
                {
                    ApiOkResponse apiokresponse = okresult.Value as ApiOkResponse;

                    //JsonConvert.DeserializeObject<List<CustomerJson>>(json);
                    //List<Order> ObjOrderList = JsonConvert.DeserializeObject<List<Order>>(orderJson);

                    componentdetailsresultsCoupling2s = JsonConvert.DeserializeObject<List<MIDCodeGeneration.Models.DetailModels.Coupling2>>(apiokresponse.Result.ToString());
                    DebuggingHelper.WriteJsonToFile(apiokresponse.Result.ToString(), levelname, "componentdetailsresultscoupling2" + "_" + machinename);
                    return Task.FromResult(componentdetailsresultsCoupling2s);
                }

            }
            catch (Exception ex)
            {
                ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.");
                EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.", ex);
                return Task.FromResult(componentdetailsresultsCoupling2s = null);
            }
        }
        public static Task<List<MIDCodeGeneration.Models.DetailModels.Intermediate>> GetComponentDetailsIntermediate(string componentsubType, int detailid, Guid tenantID, string machinename, string levelname, string levelnametop)
        {
            List<MIDCodeGeneration.Models.DetailModels.Intermediate> componentdetailsresultsIntermediates = new List<MIDCodeGeneration.Models.DetailModels.Intermediate>();
            try
            {
                //DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(middecon, new JsonSerializerOptions { WriteIndented = true }), levelname, "middecon" + "_" + machinename);

                Task<ActionResult> compCodeResults = Task.Run<ActionResult>(async () => await ProcessMIDDetails.GenerateDetails("Intermediate", componentsubType, detailid));

                object result = compCodeResults.Result;
                var okresult = result as OkObjectResult;
                if (okresult == null)
                {
                    //cast as badrequest

                    var badRequest = result as BadRequestObjectResult;
                    ApiBadRequest400Response badReqResponse = badRequest.Value as ApiBadRequest400Response;
                    var badRequestresult = JsonConvert.SerializeObject(badReqResponse.ValidationErrors);
                    var stringoferrors = "badRequest.StatusCode: " + badRequest.StatusCode.ToString() + "; bad request errors: ";
                    foreach (MIDCodeGeneration.Models.APIResponse.ApiBadRequest400Response.ValidationError v in badReqResponse.ValidationErrors.ToList())
                    { stringoferrors = stringoferrors + "errfield: " + v.Field + ", errmessage: " + v.Message + "; "; }
                    ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.  Reason: {stringoferrors}");
                    EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.  ValidationErrors: {stringoferrors}", null);
                    return Task.FromResult(componentdetailsresultsIntermediates = null);
                }
                else
                {
                    ApiOkResponse apiokresponse = okresult.Value as ApiOkResponse;

                    //JsonConvert.DeserializeObject<List<CustomerJson>>(json);
                    //List<Order> ObjOrderList = JsonConvert.DeserializeObject<List<Order>>(orderJson);

                    componentdetailsresultsIntermediates = JsonConvert.DeserializeObject<List<MIDCodeGeneration.Models.DetailModels.Intermediate>>(apiokresponse.Result.ToString());
                    DebuggingHelper.WriteJsonToFile(apiokresponse.Result.ToString(), levelname, "componentdetailsresultsintermediate" + "_" + machinename);
                    return Task.FromResult(componentdetailsresultsIntermediates);
                }

            }
            catch (Exception ex)
            {
                ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {machinename}; call to MIDDerivation failed.");
                EARPSInterfaceServiceLogger.AddLogEntry($"MachineName: {machinename}; call to MIDDerivation failed.", ex);
                return Task.FromResult(componentdetailsresultsIntermediates = null);
            }
        }
    }
}
