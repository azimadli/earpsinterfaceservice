﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
//using Microsoft.Extensions.Logging;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDI.Models;
using NLog;
using NLog.Fluent;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessPostMachine
    {
        public static async Task<MachineResponse> PostMachine(SAAIDILibrary.Models.Machine machine, Guid tenantID, string levelname)
        {
            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/{tenantID}/machines";
            var jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

            DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(machine, new JsonSerializerOptions { WriteIndented = true }), levelname, ("postMachineRequest" + machine.MachineName));

            using (HttpResponseMessage response = await APIEAHelper.ApiEAClient.PostAsJsonAsync(url, machine))
            {
                if (response.IsSuccessStatusCode)
                {
                    MachineResponse reponsemachine = await response.Content.ReadAsAsync<EARPSInterfaceService.Models.MachineResponse>();
                    DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(reponsemachine.data), levelname, ("postMachineResponse" + machine.MachineName));
                    
                    EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {tenantID} Machine added: {reponsemachine.data.MachineName}; {reponsemachine.data.MachineID}; {reponsemachine.data.MachineUUID};", null);
                    return reponsemachine;
                }
                else
                {
                    //EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {tenantID} Machine NOT added: {machine}", new Exception(response.ReasonPhrase));
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }

}
