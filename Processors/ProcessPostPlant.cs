﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Helpers;
using System.Text.Json;
using SAAIDI.Models;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessPostPlant
    {
        public static async Task<HttpResponseMessage> PostPlant(SAAIDILibrary.Models.Plant plant, Guid tenantID, string levelname)
        {
            var url = $"{ServiceSettings.apiEAuriBase}/saaidi/azima/{tenantID}/plants";
            var jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

            DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(plant, new JsonSerializerOptions { WriteIndented = true }), levelname, plant.PlantName);

            using (HttpResponseMessage response = await APIEAHelper.ApiEAClient.PostAsJsonAsync(url, plant))
            {
                if (response.IsSuccessStatusCode)
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {tenantID} Plant added: {plant.PlantName}", null);
                    return response;

                }
                else
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {tenantID} Plant added: {plant} response.ReasonPhrase {response.ReasonPhrase}", null);
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }

}
