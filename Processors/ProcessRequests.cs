﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using SAAIDILibrary.Models;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Processors;
using System.Linq;
using System.Net.Http;
using EARPSInterfaceService.Logger;
using MIDCodeGeneration.Models.APIResponse;
using MIDCodeGenerationLibrary.Processes;
using MIDCodeGenerationLibrary.Models;
using MIDCodeGeneration.Models.DrivenModels;
using Newtonsoft.Json;
using EARPSInterfaceService.Helpers;
using System.IO;
using System.Net;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using Encoder = System.Drawing.Imaging.Encoder;
using Microsoft.AspNetCore.Mvc;

namespace EARPSInterfaceService.Processors
{
    class ProcessRequests
    {

        public static async Task ProcessRequest(Guid jobID, string customerName, Guid tenantID, string plantName, Guid plantUUID, string jobtype, string jobStatus)
        {
            string levelname = customerName + @"\" + plantName + @"\" + jobtype;
            string levelnametop = levelname;

            //Retrieve CustomerID
            int incomingCustomerID = 0;
            var incomingcustomer = await ProcessGetCustomerIDByTenantID.GetCustomerID(tenantID, levelname);
            if (incomingcustomer == null)
            {
                ProcessExceptions.WriteExceptionLine(levelnametop, $"CustomerName: {customerName}; tenantID: {tenantID} - Error: customer not found.");
            }
            else
            {
                incomingCustomerID = incomingcustomer.CustomerID;

                if (jobtype == "SendDataToEA")
                {
                    jobStatus = await ProcessRequestPullToEA.PullToEA(jobID, customerName, tenantID, plantName, plantUUID, jobtype, incomingCustomerID, jobStatus, levelname);
                }
                else
                {
                    if (jobtype == "SendDataToRPS")
                    {
                        jobStatus = await ProcessRequestPushToRPS.PushToRPS(jobID, customerName, incomingCustomerID, tenantID, plantName, plantUUID, jobtype, incomingCustomerID, jobStatus, levelname);
                    }
                    else
                    {
                        ProcessExceptions.WriteExceptionLine(levelnametop, $"CustomerName: {customerName}; tenantID: {tenantID}; jobtype: {jobtype} - Error: jobtype not defined.");
                    }
                }
            }
        }
    }
}


