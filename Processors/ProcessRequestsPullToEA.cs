﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using SAAIDILibrary.Models;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Processors;
using System.Linq;
using System.Net.Http;
using EARPSInterfaceService.Logger;
using MIDCodeGeneration.Models.APIResponse;
using MIDCodeGenerationLibrary.Processes;
using MIDCodeGenerationLibrary.Models;
using MIDCodeGeneration.Models.CodeDeconstructionModels;
using MIDCodeGeneration.Models.DrivenModels;
using Newtonsoft.Json;
using EARPSInterfaceService.Helpers;
using System.IO;
using System.Net;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using Encoder = System.Drawing.Imaging.Encoder;
using Microsoft.AspNetCore.Mvc;

namespace EARPSInterfaceService.Processors
{
    class ProcessRequestPullToEA
    {
        private static HttpResponseMessage postplantresults;

        public static MIDCodeCreatorRequest ComponentsRoot { get; private set; }
        public static int machinesinareacounter = 0;
        public static int machinesinareaskippedcounter = 0;
        public static Boolean skipmachine = false;
        public static short nextMIDNumber = 0;

        public static async Task<string> PullToEA(Guid jobid, string customerName, Guid customerId, string plantName, Guid plantUUID, string jobtype, int incomingCustomerID, string jobStatus, string levelname)
        {
            string levelnametop = levelname;
            var jobstatus = jobStatus;
            List<LocationLOR> LocLOR = LoadData.LoadLORdata();
            foreach (var LocationLOR in LocLOR)
            {
                ProcessOtherFileWrites.WriteOtherFileLine(levelnametop, "LocationLOR.txt",
                    LocationLOR.lorKey +
                    $">>> " +
                    LocationLOR.lowrangefactorFMAX + " " +
                    LocationLOR.highrangefactorFMAX + " " +
                    LocationLOR.demodlowrangefactorFMAX + " " +
                    LocationLOR.lowrangefactorLOR  + " " +
                    LocationLOR.highrangefactorLOR + " " +
                    LocationLOR.demodlowrangefactorLOR + " " +
                    LocationLOR.lowrangeLOR + " " +
                    LocationLOR.highrangeLOR + " " +
                    LocationLOR.demodlowrangeLOR + " " +
                    $"<<<"
                    );
            }

            List<ComponentLocations> CompLoc = LoadData.Loadcomplocdata();
            foreach (var ComponentLocations in CompLoc)
            {
                ProcessOtherFileWrites.WriteOtherFileLine(levelnametop, "ComponentLocations.txt", 
                    ComponentLocations.componentLocationsLine + " " +
                    $">>> " +
                    ComponentLocations.componentType + " " +
                    ComponentLocations.componentTypeSub + " " +
                    ComponentLocations.componentTypeSub2 + " " +
                    ComponentLocations.speedchangedirection + " " +// fix rpm ratio speed - no change
                    ComponentLocations.speedchangenumberoftimes + " " +// fix rpm ratio speed - no change
                    ComponentLocations.location + " " +
                    ComponentLocations.locationlorkey1 + "  " +
                    ComponentLocations.locationlorkey2 + " " +
                    ComponentLocations.locationlorkey3 + " " +
                    ComponentLocations.locationlorkey4 + "  " +
                    $"<<<"
                    );
            }

            Guid tenantID = customerId;
            //Retrieve Plant
            try
            {
                //Test
                var pullplant = await ProcessAPIPullPlant.PullPlant(customerName, plantUUID,  jobid, levelname);
                //Process Plant
                SAAIDILibrary.Models.Plant newplant = new SAAIDILibrary.Models.Plant();
                newplant.PlantID = 0;
                newplant.PlantUUID = pullplant.id; //jcm 20220317
                newplant.PlantName = pullplant.name;
                newplant.CompanyName = customerName;
                newplant.PortalActive = 0;
                nextMIDNumber = (short)pullplant.midStart;

                string PlantUnits = "in/s"; //Fix - waiting for this data to be added to the incoming json 9/1/21
                int PlantLineFrequency = 60;  //Fix - waiting for this data to be added to the incomin json 9/2/21
                ProcessExceptions.WriteExceptionLine(levelnametop, @"ExceptionReport");
                postplantresults = await ProcessPostPlant.PostPlant(newplant, customerId, levelname);
                EARPSInterfaceServiceLogger.AddLogEntry(postplantresults.ToString(),null);

                //Process Areas in a Plant
                var area = from a in pullplant.areas select a;
                foreach (var AreaBeingProcessed in area)
                {
                    EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}",null);

                    levelname = DebuggingHelper.Changelevelname(levelname, @"\" + AreaBeingProcessed.name, "add");
                    SAAIDILibrary.Models.Area newarea = new SAAIDILibrary.Models.Area();
                    newarea.AreaID = 0;
                    newarea.AreaUUID = AreaBeingProcessed.id;//jcm 20220317
                    newarea.AreaName = AreaBeingProcessed.name;
                    var tempareaname = AreaBeingProcessed.name;
                    newarea.PlantUUID = pullplant.id;//jcm 20220317
                    var postarearesults = await ProcessPostArea.PostArea(newarea, customerId, levelname);
                    //Process Machines in an Area
                    machinesinareacounter = 0;
                    machinesinareaskippedcounter = 0;
                    var machine = from m in AreaBeingProcessed.machines select m;
                    foreach (var MachineBeingProcessed in machine)
                    {
                        levelname = DebuggingHelper.Changelevelname(levelname, @"\" + MachineBeingProcessed.name, "add");
                        //MachineBeingProcessed.intermediate = MachineBeingProcessed.gearbox;
                        //ma = new MachineBeingProcessed
                        //MachineBeingProcessed.intermediate.version = MachineBeingProcessed.gearbox.version;
                        //Modelhelper.CopyPropertiesTo<EARPSInterfaceService.Models.Gearbox, EARPSInterfaceService.Models.Intermediate>(MachineBeingProcessed.gearbox, MachineBeingProcessed.intermediate);

                        machinesinareacounter++; // Fix
                        skipmachine = false;
                        var tempmachinename = MachineBeingProcessed.name;
                        tempmachinename = tempmachinename.Trim();
                        var component_present_driver = false;
                        var component_present_coupling1 = false;
                        var component_present_intermediate = false;
                        var component_present_coupling2 = false;
                        var component_present_driven = false;
                        var machinedrivertype = "";
                        var machinedriversubtype = "";
                        double interimrpm = 0; // fix rpm ratio speed
                        double machinedriverrpm = 0; // fix rpm ratio speed
                        var machineintermediatetype = "";
                        var machineintermediatesubtype = "";
                        var machinedriventype = "";
                        var machinedrivensubtype = "";
                        double machinedrivenrpm = 0; // fix rpm ratio speed

                        SAAIDILibrary.Models.Machine newmachine = new SAAIDILibrary.Models.Machine();
                        MID newMID = new SAAIDILibrary.Models.MID();

                        ComponentInfo componentinfodriver = new ComponentInfo();
                        ComponentInfo componentinfointermediate = new ComponentInfo();
                        ComponentInfo componentinfodriven = new ComponentInfo();
                        ComponentInfo componentinfoorient = new ComponentInfo();
                        if (MachineBeingProcessed.machineComponentMap != null)
                        {
                            EARPSInterfaceServiceLogger.AddLogEntry($"Processing Started MachineName: {MachineBeingProcessed.name} in AreaName: {AreaBeingProcessed.name}; ctr: {machinesinareacounter}", null);

                            var componentmap = from cm in MachineBeingProcessed.machineComponentMap select cm;
                            foreach (var MachineCMBeingProcessed in componentmap)
                            {
                                var componentstuffkey = MachineCMBeingProcessed.key;
                                if (String.Compare(componentstuffkey, "Driver", comparisonType: StringComparison.OrdinalIgnoreCase) == 0)
                                {
                                    componentinfodriver = ProcessComponentMap.TranslateComponentMap(MachineCMBeingProcessed, MachineBeingProcessed.name, levelname);
                                };
                                if (String.Compare(componentstuffkey, "Intermediate", comparisonType: StringComparison.OrdinalIgnoreCase) == 0)
                                {
                                    componentinfointermediate = ProcessComponentMap.TranslateComponentMap(MachineCMBeingProcessed, MachineBeingProcessed.name, levelname);
                                };
                                if (String.Compare(componentstuffkey, "Driven", comparisonType: StringComparison.OrdinalIgnoreCase) == 0)
                                {
                                    componentinfodriven = ProcessComponentMap.TranslateComponentMap(MachineCMBeingProcessed, MachineBeingProcessed.name, levelname);
                                };
                                if (String.Compare(componentstuffkey, "Orientation Information", comparisonType: StringComparison.OrdinalIgnoreCase) == 0)
                                {
                                    componentinfoorient = ProcessComponentMap.TranslateComponentMap(MachineCMBeingProcessed, MachineBeingProcessed.name, levelname);
                                };
                            }
                            if (componentinfodriver == null && componentinfointermediate == null && componentinfodriven == null)
                            {
                                skipmachine = true;
                                  ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); AreaName: {AreaBeingProcessed.name}; No componentmap found skipping machine.");
                            }
                        }
                        else
                        {
                            skipmachine = true;
                             ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); No componentmap found skipping machine.");
                        };
                        
                        if (MachineBeingProcessed.driver != null)
                        {
                            EARPSInterfaceServiceLogger.AddLogEntry($"Driver rpm: {MachineBeingProcessed.driver.rpm} : {MachineBeingProcessed.name}", null);// fix rpm ratio speed
                        }
                        if (componentinfodriver != null)
                        {
                            EARPSInterfaceServiceLogger.AddLogEntry($"Driver CM motorspeed: {componentinfodriver.motorspeed} : {MachineBeingProcessed.name}", null);
                            EARPSInterfaceServiceLogger.AddLogEntry($"Driver CM speed: {componentinfodriver.speed} : {MachineBeingProcessed.name}", null);
                            EARPSInterfaceServiceLogger.AddLogEntry($"CM Driver: {componentinfodriver.componentstuff} : {MachineBeingProcessed.name}", null);
                        }
                        //if (MachineBeingProcessed.intermediate != null)
                        if (MachineBeingProcessed.gearbox != null)
                        {
                            EARPSInterfaceServiceLogger.AddLogEntry($"Intermediate ratio: {MachineBeingProcessed.gearbox.ratio} : {MachineBeingProcessed.name}", null);
                        }
                        if (componentinfointermediate != null) 
                        {
                            EARPSInterfaceServiceLogger.AddLogEntry($"Intermediate CM ratios: {componentinfointermediate.ratios} : {MachineBeingProcessed.name}", null);
                            EARPSInterfaceServiceLogger.AddLogEntry($"CM Intermediate: {componentinfointermediate.componentstuff} : {MachineBeingProcessed.name}", null);
                        }
                        if (MachineBeingProcessed.driven != null)
                        {
                            EARPSInterfaceServiceLogger.AddLogEntry($"Driven rpm: {MachineBeingProcessed.driven.rpmOutput} : {MachineBeingProcessed.name}", null);// fix rpm ratio speed
                        }
                        if (componentinfodriven != null)
                        {
                            EARPSInterfaceServiceLogger.AddLogEntry($"Driven CM rpm: {componentinfodriven.driven} : {MachineBeingProcessed.name}", null);// fix rpm ratio speed
                            EARPSInterfaceServiceLogger.AddLogEntry($"CM Driven: {componentinfodriven.componentstuff} : {MachineBeingProcessed.name}",null);
                        }

                        /*
                        if (MachineBeingProcessed.name != "insert machinename")
                        {
                            skipmachine = true;
                             ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); Machine in error found skipping machine.");
                        }
                        */
                        if (skipmachine == false)
                        {
                            // Create an MID and its components for the machine
                            // First call MIDDerivationLibrary to determine the MID's components

                            var wthgearbox = MachineBeingProcessed.gearbox;
                            //var oldintermediate = MachineBeingProcessed.intermediate;

                            var midcomp = new MIDCodeCreatorRequest();
                            midcomp.machineComponentsForMIDgeneration = new MachineComponentsForMIDgeneration();

                            //Set up some defaults
                            double machinenominalspeed = 0;// fix rpm ratio speed
                            string machineorientation = MachineBeingProcessed.machineOrientation.Substring(0);
                            string machineorientationfull = MachineBeingProcessed.machineOrientation;

                            if (MachineBeingProcessed.driver != null && componentinfodriver.Driver != null && !(String.Equals(componentinfodriver.Driver, "not monitored", StringComparison.OrdinalIgnoreCase)))
                            {
                                //Populate driver Component Data
                                component_present_driver = true;
                                midcomp.machineComponentsForMIDgeneration.driver = new MIDCodeGeneration.Models.DriverModels.Driver();
                                midcomp.machineComponentsForMIDgeneration.driver.drivers = new MIDCodeGeneration.Models.DriverModels.Drivers();
                                //midcomp.machineComponentsForMIDgeneration.driver.drivers.motor = new MIDCodeGeneration.Models.DriverModels.Motor();

                                midcomp.machineComponentsForMIDgeneration.driver.componentType = "driver";
                                midcomp.machineComponentsForMIDgeneration.driver.locations = componentinfodriver.Driver_locations_int;
                                midcomp.machineComponentsForMIDgeneration.driver.rpm = MachineBeingProcessed.driver.rpm;// fix rpm ratio speed
                                machinenominalspeed = MachineBeingProcessed.driver.rpm;// fix rpm ratio speed
                                machinedrivenrpm = machinenominalspeed;// fix rpm ratio speed
                                if (machinenominalspeed == 0)// fix rpm ratio speed
                                {
                                    machinenominalspeed = 1800;// fix rpm ratio speed
                                    ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); rpm = 0 - defaulted to 1800");
                                }
                                machinedriverrpm = machinenominalspeed;// fix rpm ratio speed
                                interimrpm = machinenominalspeed; // fix rpm ratio speed
                                if (midcomp.machineComponentsForMIDgeneration.driver.locations == 2)
                                {
                                    midcomp.machineComponentsForMIDgeneration.driver.driverLocationNDE = true;
                                    midcomp.machineComponentsForMIDgeneration.driver.driverLocationDE = true;
                                }
                                else
                                {
                                    midcomp.machineComponentsForMIDgeneration.driver.driverLocationNDE = String.IsNullOrEmpty(componentinfodriver.Driver_locationType) ? false : componentinfodriver.Driver_locationType == "NDE" ? true : false;
                                    midcomp.machineComponentsForMIDgeneration.driver.driverLocationDE = String.IsNullOrEmpty(componentinfodriver.Driver_locationType) ? false : componentinfodriver.Driver_locationType == "DE" ? true : false;
                                    if (midcomp.machineComponentsForMIDgeneration.driver.locations == 0)
                                    {
                                        if (midcomp.machineComponentsForMIDgeneration.driver.driverLocationNDE == true)
                                        {
                                            midcomp.machineComponentsForMIDgeneration.driver.locations += 1;
                                        }
                                        if (midcomp.machineComponentsForMIDgeneration.driver.driverLocationDE == true)
                                        {
                                            midcomp.machineComponentsForMIDgeneration.driver.locations += 1;
                                        }
                                    }
                                }
                                midcomp.machineComponentsForMIDgeneration.driver.driverType = String.IsNullOrEmpty(componentinfodriver.Driver) ? null : componentinfodriver.Driver;
                                machinedrivertype = midcomp.machineComponentsForMIDgeneration.driver.driverType;

                                //if (String.Equals(midcomp.machineComponentsForMIDgeneration.driver.driverType, "motor", StringComparison.OrdinalIgnoreCase))
                                if (String.Equals(componentinfodriver.Driver, "motor", StringComparison.OrdinalIgnoreCase))
                                {
                                    // Driver - motor
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.motor = new MIDCodeGeneration.Models.DriverModels.Motor();

                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.motor.vfd = new MIDCodeGeneration.Models.DriverModels.VFD();
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.motor.motorDrive = String.IsNullOrEmpty(componentinfodriver.drive) ? null : componentinfodriver.drive;
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.motor.motorFan = componentinfodriver.motorfan_boolean;
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.motor.motorBallBearings = componentinfodriver.motorbb_boolean;
                                    if (componentinfodriven.driven == "not monitored") // driven is present but not monitored
                                    {
                                        midcomp.machineComponentsForMIDgeneration.driver.drivers.motor.drivenBallBearings = false;
                                        midcomp.machineComponentsForMIDgeneration.driver.drivers.motor.drivenBalanceable = false;
                                    }
                                    else
                                    {
                                        midcomp.machineComponentsForMIDgeneration.driver.drivers.motor.drivenBallBearings = componentinfodriven.drivenbb_boolean;
                                        midcomp.machineComponentsForMIDgeneration.driver.drivers.motor.drivenBalanceable = componentinfodriven.drivenbal_boolean;
                                    }
                                    MIDCodeGeneration.Models.CommonModels.ExtraFaultData motorexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.motor.extraFaultData = motorexfd;
                                    motorexfd.motorbars = componentinfodriver.motorbars_int;
                                    motorexfd.motorfanblades = componentinfodriver.fanBlades_int;

                                    if (String.IsNullOrEmpty(componentinfodriver.motorpoles) != true)
                                    {
                                        // Driver - motor VFD motorpoles
                                        midcomp.machineComponentsForMIDgeneration.driver.drivers.motor.vfd.motorPoles = String.IsNullOrEmpty(componentinfodriver.motorpoles) ? 0 : int.Parse(componentinfodriver.motorpoles); // motorpoles
                                        machinedriversubtype = "vfd";
                                    }
                                    else
                                    {
                                        midcomp.machineComponentsForMIDgeneration.driver.drivers.motor.vfd = null;
                                    }
                                }
                                if (String.Equals(componentinfodriver.Driver, "diesel", StringComparison.OrdinalIgnoreCase))
                                {
                                    // Driver - Diesel
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.diesel = new MIDCodeGeneration.DriverModels.Diesel();
                                    midcomp.machineComponentsForMIDgeneration.driver.driverType = "diesel";
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.diesel.cylinders = componentinfodriver.cyl_int;
                                    //MIDCodeGeneration.Models.CommonModels.ExtraFaultData dieselexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                }
                                if (String.Equals(componentinfodriver.Driver, "Turbine", StringComparison.OrdinalIgnoreCase))
                                {
                                    // Driver - Turbine
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.turbine = new MIDCodeGeneration.Models.DriverModels.Turbine();
                                    midcomp.machineComponentsForMIDgeneration.driver.driverType = "turbine";

                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.turbine.turbineReductionGear = componentinfodriver.redgear_boolean;
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.turbine.turbineRotorSupported = componentinfodriver.turbinesupported_boolean;
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.turbine.turbineBallBearing = componentinfodriver.turbinebb_boolean;
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.turbine.turbineThrustBearing = componentinfodriver.turbineTbrg_boolean;
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.turbine.turbineThrustBearingIsBall = componentinfodriver.Tbrgbb_boolean;
                                    MIDCodeGeneration.Models.CommonModels.ExtraFaultData turbineexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                    //fix cyl??? turbineexfd.turbineblades = componentinfodriver.cyl_int;
                                    turbineexfd.turbineblades = null; //fix
                                    midcomp.machineComponentsForMIDgeneration.driver.drivers.turbine.extraFaultData = turbineexfd;
                                }
                                if (componentinfodriver.isCloseCoupled_boolean == true)
                                {
                                    MIDCodeGenerationLibrary.Models.DriverModels.CloseCoupled closedcoupleddata = new MIDCodeGenerationLibrary.Models.DriverModels.CloseCoupled();
                                    closedcoupleddata.closeCoupled = true;
                                    closedcoupleddata.closeCoupledNumber = componentinfodriver.closeCoupledLocations_int;
                                    closedcoupleddata.closeCoupledType = componentinfodriver.closeCoupledType;
                                    closedcoupleddata.closeCoupledDriven = componentinfodriver.closeCoupledDriven;
                                    closedcoupleddata.drivenBallBearings = componentinfodriver.closeCoupleddrivenBallBearings_boolean;
                                    if (componentinfodriver.Driver == "motor")
                                    {
                                        midcomp.machineComponentsForMIDgeneration.driver.drivers.motor.closeCoupled = closedcoupleddata;
                                    }
                                    if (componentinfodriver.Driver == "turbine")
                                    {
                                        midcomp.machineComponentsForMIDgeneration.driver.drivers.turbine.closeCoupled = closedcoupleddata;
                                    }
                                }
                                interimrpm = machinedriverrpm; // fix rpm ratio speed - what about closedcoupled?
                            }
                            if (componentinfodriver.cpl1 != null && !(String.Equals(componentinfodriver.cpl1, "nocoupling", StringComparison.OrdinalIgnoreCase)))
                            {
                                //Populate coupling1 Component Data
                                component_present_coupling1 = true;
                                midcomp.machineComponentsForMIDgeneration.coupling1 = new MIDCodeGeneration.Models.CouplingModels.Coupling1();
                                midcomp.machineComponentsForMIDgeneration.coupling1.componentType = "coupling";

                                midcomp.machineComponentsForMIDgeneration.coupling1.couplingPosition = 1; //fix
                                midcomp.machineComponentsForMIDgeneration.coupling1.couplingType = String.IsNullOrEmpty(componentinfodriver.cpl1) ? null : componentinfodriver.cpl1;
                                midcomp.machineComponentsForMIDgeneration.coupling1.speedratio = String.IsNullOrEmpty(componentinfodriver.ratios) ? 1 : int.Parse(componentinfodriver.ratios); // fix rpm ratio speed
                                // midcomp.machineComponentsForMIDgeneration.coupling1.locations = String.IsNullOrEmpty(componentinfodriver.locations) ? 0 : int.Parse(componentinfodriver.locations); // fix
                            }
                            //if (MachineBeingProcessed.intermediate != null && componentinfointermediate.intermediate != null && !(String.Equals(componentinfointermediate.intermediate, "not monitored", StringComparison.OrdinalIgnoreCase)))
                            if (MachineBeingProcessed.gearbox != null && componentinfointermediate.intermediate != null && !(String.Equals(componentinfointermediate.intermediate, "not monitored", StringComparison.OrdinalIgnoreCase)))
                            {
                                //Populate intermediate Component Data
                                component_present_intermediate = true;
                                midcomp.machineComponentsForMIDgeneration.intermediate = new MIDCodeGeneration.Models.IntermediateModels.Intermediate();
                                midcomp.machineComponentsForMIDgeneration.intermediate.intermediates = new MIDCodeGeneration.Models.IntermediateModels.Intermediates();
                                midcomp.machineComponentsForMIDgeneration.intermediate.componentType = "intermediate";
                                midcomp.machineComponentsForMIDgeneration.intermediate.locations = componentinfointermediate.NOL_int;
                                midcomp.machineComponentsForMIDgeneration.intermediate.speedratio = 1; // fix rpm ratio speed
                                midcomp.machineComponentsForMIDgeneration.intermediate.intermediateType = String.IsNullOrEmpty(componentinfointermediate.intermediate) ? null : componentinfointermediate.intermediate;
                                if (String.Equals(componentinfointermediate.intermediate, "AccDrGr", StringComparison.OrdinalIgnoreCase))
                                {
                                    // Intermediate - AccDrGr
                                    machineintermediatetype = componentinfointermediate.intermediate;
                                    midcomp.machineComponentsForMIDgeneration.intermediate.intermediates.AccDrGr = new MIDCodeGeneration.Models.IntermediateModels.AccDrGr();
                                    midcomp.machineComponentsForMIDgeneration.intermediate.intermediates.AccDrGr.drivenBy = "driver"; // fix
                                }
                                if (String.Equals(componentinfointermediate.intermediate, "AOP", StringComparison.OrdinalIgnoreCase))
                                {
                                    // Intermediate - AOP
                                    machineintermediatetype = componentinfointermediate.intermediate;
                                    midcomp.machineComponentsForMIDgeneration.intermediate.intermediates.AOP = new MIDCodeGeneration.Models.IntermediateModels.AOP();
                                    midcomp.machineComponentsForMIDgeneration.intermediate.intermediates.AOP.drivenBy = "driver"; // fix
                                }
                                if (String.Equals(componentinfointermediate.intermediate, "gearbox", StringComparison.OrdinalIgnoreCase))
                                {
                                    // Intermediate - gearbox
                                    machineintermediatetype = componentinfointermediate.intermediate;
                                    midcomp.machineComponentsForMIDgeneration.intermediate.intermediates.gearbox = new MIDCodeGeneration.Models.IntermediateModels.Gearbox();
                                    midcomp.machineComponentsForMIDgeneration.intermediate.intermediates.gearbox.speedChangesMax = componentinfointermediate.ISC_int; // fix rpm ratio speed
                                }
                            }
                            if (componentinfointermediate.cpl2 != null && !(String.Equals(componentinfointermediate.cpl2, "nocoupling", StringComparison.OrdinalIgnoreCase)))
                            {
                                //Populate coupling2 Component Data
                                component_present_coupling2 = true;
                                midcomp.machineComponentsForMIDgeneration.coupling2 = new MIDCodeGeneration.Models.CouplingModels.Coupling2();
                                midcomp.machineComponentsForMIDgeneration.coupling2.componentType = "coupling";
                                midcomp.machineComponentsForMIDgeneration.coupling2.couplingPosition = 2; //fix
                                midcomp.machineComponentsForMIDgeneration.coupling2.couplingType = String.IsNullOrEmpty(componentinfointermediate.cpl2) ? null : componentinfointermediate.cpl2;
                                midcomp.machineComponentsForMIDgeneration.coupling2.speedratio = (Decimal)1.0000; // fix rpm ratio speed
                                //midcomp.machineComponentsForMIDgeneration.coupling2.speedratio = String.IsNullOrEmpty(componentinfointermediate.ratios) ? 0 : int.Parse(componentinfointermediate.ratios); // fix rpm ratio speed
                                // midcomp.machineComponentsForMIDgeneration.coupling1.locations = String.IsNullOrEmpty(componentinfointermediate.locations) ? 0 : int.Parse(componentinfointermediate.locations); // fix rpm ratio speed
                            }
                            if (MachineBeingProcessed.driven != null && componentinfodriven.Driven != null && !(String.Equals(componentinfodriven.Driven, "not monitored", StringComparison.OrdinalIgnoreCase)))
                            {
                                //Populate driven Component Data
                                component_present_driven = true;
                                machinedrivenrpm = MachineBeingProcessed.driven.rpmOutput;// fix rpm ratio speed 
                                midcomp.machineComponentsForMIDgeneration.driven = new MIDCodeGeneration.Models.DrivenModels.Driven();
                                midcomp.machineComponentsForMIDgeneration.driven.drivens = new MIDCodeGeneration.Models.DrivenModels.Drivens();

                                midcomp.machineComponentsForMIDgeneration.driven.componentType = "driven";
                                midcomp.machineComponentsForMIDgeneration.driven.locations = String.IsNullOrEmpty(componentinfodriven.Driven_locations) ? 0 : int.Parse(componentinfodriven.Driven_locations);
                                if (midcomp.machineComponentsForMIDgeneration.driven.locations == 2)
                                {
                                    midcomp.machineComponentsForMIDgeneration.driven.drivenLocationNDE = true;
                                    midcomp.machineComponentsForMIDgeneration.driven.drivenLocationDE = true;
                                }
                                else
                                {
                                    midcomp.machineComponentsForMIDgeneration.driven.drivenLocationNDE = String.IsNullOrEmpty(componentinfodriven.Driven_locationType) ? false : componentinfodriven.Driven_locationType == "NDE" ? true : false;
                                    midcomp.machineComponentsForMIDgeneration.driven.drivenLocationDE = String.IsNullOrEmpty(componentinfodriven.Driven_locationType) ? false : componentinfodriven.Driven_locationType == "DE" ? true : false;
                                }
                                midcomp.machineComponentsForMIDgeneration.driven.rpm = MachineBeingProcessed.driven.rpmOutput;// fix rpm ratio speed
                                //midcomp.machineComponentsForMIDgeneration.intermediate.intermediateType = String.IsNullOrEmpty(componentinfointermediate.intermediate) ? null : componentinfointermediate.intermediate;
                                if (String.Equals(componentinfodriven.Driven, "pump", StringComparison.OrdinalIgnoreCase))
                                {
                                    //Driven pump
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.pump = new MIDCodeGeneration.Models.DrivenModels.Pump();
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes = new MIDCodeGeneration.Models.DrivenModels.PumpTypes();
                                    midcomp.machineComponentsForMIDgeneration.driven.drivenType = componentinfodriven.Driven;
                                    machinedriventype = midcomp.machineComponentsForMIDgeneration.driven.drivenType;
                                    if (String.Equals(componentinfodriven.drivenc, "centrifugalpump", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven centrifugal pump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpCentrifugal = new MIDCodeGeneration.Models.DrivenModels.PumpCentrifugal();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType = "centrifugal";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpCentrifugal.rotorOverhung = String.IsNullOrEmpty(componentinfodriven.rotorOH) ? false : componentinfodriven.rotorOH == "Yes" ? true : false;
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpCentrifugal.centrifugalPumpHasBallBearings = String.IsNullOrEmpty(componentinfodriven.drivenbb) ? false : componentinfodriven.drivenbb == "Yes" ? true : false;
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpCentrifugal.thrustBearing = String.IsNullOrEmpty(componentinfodriven.DriverTbrg) ? null : componentinfodriven.DriverTbrg;

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData pumpcntrfglexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpCentrifugal.extraFaultData = null;
                                        pumpcntrfglexfd.pumpvanes = String.IsNullOrEmpty(componentinfodriven.vanes) ? 0 : int.Parse(componentinfodriven.vanes);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpCentrifugal.extraFaultData = pumpcntrfglexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "propellerpump", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven propeller pump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpPropeller = new MIDCodeGeneration.Models.DrivenModels.PumpPropeller();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType = "propeller";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpPropeller.propellerpumpHasBallBearings = true; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData pumppropellerexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpPropeller.extraFaultData = null;
                                        pumppropellerexfd.pumpvanes = String.IsNullOrEmpty(componentinfodriven.vanes) ? 0 : int.Parse(componentinfodriven.vanes);
                                        pumppropellerexfd.pumpblades = String.IsNullOrEmpty(componentinfodriven.fanBlades) ? 0 : int.Parse(componentinfodriven.fanBlades);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpPropeller.extraFaultData = pumppropellerexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "rotarythread", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven RotaryThread pump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryThread = new MIDCodeGeneration.Models.DrivenModels.PumpRotaryThread();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType = "rotarythread";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryThread.rotaryThreadPumpHasBallBearings = true; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData pumprotarythreadexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryThread.extraFaultData = null;
                                        pumprotarythreadexfd.pumpthreads = String.IsNullOrEmpty(componentinfodriven.Driven_Threads) ? 0 : int.Parse(componentinfodriven.Driven_Threads);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryThread.extraFaultData = pumprotarythreadexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "gearpump", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven Gear pump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpGear = new MIDCodeGeneration.Models.DrivenModels.PumpGear();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType = "gear";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpGear.gearPumpHasBallBearings = true; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData pumpgearexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpGear.extraFaultData = null;
                                        pumpgearexfd.pumpvanes = String.IsNullOrEmpty(componentinfodriven.vanes) ? 0 : int.Parse(componentinfodriven.vanes);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpGear.extraFaultData = pumpgearexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "rotaryscrewpump", StringComparison.OrdinalIgnoreCase) ||
                                        String.Equals(componentinfodriven.drivenc, "screwpump", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven RotaryScrew pump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryScrew = new MIDCodeGeneration.Models.DrivenModels.PumpRotaryScrew();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType = "screw";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryScrew.screwPumpHasBallBearings = componentinfodriven.drivenbb_boolean; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData pumprotaryscrewexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryScrew.extraFaultData = null;
                                        pumprotaryscrewexfd.pumpthreads = String.IsNullOrEmpty(componentinfodriven.Driven_Threads) ? 0 : int.Parse(componentinfodriven.Driven_Threads);
                                        //pumprotaryscrewexfd.idlerthreads = String.IsNullOrEmpty(componentinfodriven.idlerthreads) ? 0 : int.Parse(componentinfodriven.idlerthreads);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryScrew.extraFaultData = pumprotaryscrewexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "rotaryslidingvanepump", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven RotarySlidingVane pump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotarySlidingVane = new MIDCodeGeneration.Models.DrivenModels.PumpRotarySlidingVane();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType = "slidingvane";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotarySlidingVane.slidingVanePumpHasBallBearings = true; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData pumprotaryslidingvaneexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotarySlidingVane.extraFaultData = null;
                                        pumprotaryslidingvaneexfd.pumpvanes = String.IsNullOrEmpty(componentinfodriven.vanes) ? 0 : int.Parse(componentinfodriven.vanes);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotarySlidingVane.extraFaultData = pumprotaryslidingvaneexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "axialrecip", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven RotaryAxial pump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryAxialRecip = new MIDCodeGeneration.Models.DrivenModels.PumpRotaryAxialRecip();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType = "axialrecip";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryAxialRecip.attachedOilPump = true; //fix
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryAxialRecip.axialRecipPumpHasBallBearings = true; //fix
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryAxialRecip.thrustBearing = "Journal"; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData pumprotaryaxialrecipexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryAxialRecip.extraFaultData = null;
                                        pumprotaryaxialrecipexfd.pumpvanes = String.IsNullOrEmpty(componentinfodriven.vanes) ? 0 : int.Parse(componentinfodriven.vanes);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryAxialRecip.extraFaultData = pumprotaryaxialrecipexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "radialrecip", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven RotaryRadialRecip pump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryRadialRecip = new MIDCodeGeneration.Models.DrivenModels.PumpRotaryRadialRecip();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType = "radialrecip";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpType;

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData pumprotaryreadialrecipexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryAxialRecip.extraFaultData = null;
                                        // fix pumprotaryreadialrecipexfd.pumppistons = String.IsNullOrEmpty(componentinfodriven.pistons) ? 0 : int.Parse(componentinfodriven.pistons);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.pump.pumpTypes.pumpRotaryAxialRecip.extraFaultData = pumprotaryreadialrecipexfd;
                                    }
                                }
                                if (String.Equals(componentinfodriven.Driven, "compressor", StringComparison.OrdinalIgnoreCase))
                                {
                                    //Driven compressor
                                    //Driven pump
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor = new MIDCodeGeneration.Models.DrivenModels.Compressor();
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes = new MIDCodeGeneration.Models.DrivenModels.CompressorTypes();
                                    midcomp.machineComponentsForMIDgeneration.driven.drivenType = componentinfodriven.Driven;
                                    machinedriventype = midcomp.machineComponentsForMIDgeneration.driven.drivenType;
                                    if (String.Equals(componentinfodriven.drivenc, "Centrifugal Compressor", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven centrifugal compressor
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorCentrifugal = new MIDCodeGeneration.Models.DrivenModels.CompressorCentrifugal();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorType = "centrifugal";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorCentrifugal.impellerOnMainShaft = true; //fix
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorCentrifugal.centrifugalCompressorHasBallBearings = true; //fix
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorCentrifugal.thrustBearing = "yes"; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData compressorcentrifugalexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorCentrifugal.extraFaultData = null;
                                        compressorcentrifugalexfd.compressorvanes = String.IsNullOrEmpty(componentinfodriven.vanes) ? 0 : int.Parse(componentinfodriven.vanes);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorCentrifugal.extraFaultData = compressorcentrifugalexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "Reciprocating Compressor", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven Reciprocating compressor
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorReciporcating = new MIDCodeGeneration.Models.DrivenModels.CompressorReciporcating();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorType = "reciprocating";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorReciporcating.crankHasIntermediateBearing = true; //fix
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorReciporcating.reciprocatingCompressorHasBallBearings = true; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData compressorreciporcatingexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorReciporcating.extraFaultData = null;
                                        //fix compressorreciporcatingexfd.compressorpistons = String.IsNullOrEmpty(componentinfodriven.pistons) ? 0 : int.Parse(componentinfodriven.pistons);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorReciporcating.extraFaultData = compressorreciporcatingexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "Screw Compressor", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven Screw compressor
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorScrew = new MIDCodeGeneration.Models.DrivenModels.CompressorScrew();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorType = "screw";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorScrew.screwCompressorHasBallBearings = true; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData compressorscrewexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorScrew.extraFaultData = null;
                                        compressorscrewexfd.compressorthreads = String.IsNullOrEmpty(componentinfodriven.Driven_Threads) ? 0 : int.Parse(componentinfodriven.Driven_Threads); //fix
                                        compressorscrewexfd.idlerthreads = String.IsNullOrEmpty(componentinfodriven.Idler_threads) ? 0 : int.Parse(componentinfodriven.Idler_threads); //fix
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorScrew.extraFaultData = compressorscrewexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "ScrewTwin Compressor", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven ScrewTwin compressor
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorScrewTwin = new MIDCodeGeneration.Models.DrivenModels.CompressorScrewTwin();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorType = "screwtwin";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorScrewTwin.screwTwinCompressorHasBallBearingsOnHPSide = true; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData compressorscrewexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorScrew.extraFaultData = null;
                                        compressorscrewexfd.compressorthreads1 = String.IsNullOrEmpty(componentinfodriven.Driven_Threads) ? 0 : int.Parse(componentinfodriven.Driven_Threads); //fix
                                        compressorscrewexfd.compressorthreads2 = String.IsNullOrEmpty(componentinfodriven.Driven_Threads) ? 0 : int.Parse(componentinfodriven.Driven_Threads); //fix
                                        compressorscrewexfd.idlerthreads1 = String.IsNullOrEmpty(componentinfodriven.Idler_threads) ? 0 : int.Parse(componentinfodriven.Idler_threads); //fix
                                        compressorscrewexfd.idlerthreads2 = String.IsNullOrEmpty(componentinfodriven.Idler_threads) ? 0 : int.Parse(componentinfodriven.Idler_threads); //fix
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.compressor.compressorTypes.compressorScrew.extraFaultData = compressorscrewexfd;
                                    }
                                }
                                if (String.Equals(componentinfodriven.Driven, "fan_or_blower", StringComparison.OrdinalIgnoreCase))
                                {
                                    //Driven fan_or_blower
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower = new MIDCodeGeneration.Models.DrivenModels.FanOrBlower();
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes = new MIDCodeGeneration.Models.DrivenModels.FanOrBlowerTypes();
                                    midcomp.machineComponentsForMIDgeneration.driven.drivenType = componentinfodriven.Driven;
                                    machinedriventype = midcomp.machineComponentsForMIDgeneration.driven.drivenType;
                                    if (String.Equals(componentinfodriven.drivenc, "lobed", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven lobed fan_or_blower
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerLobed = new MIDCodeGeneration.Models.DrivenModels.FanOrBlowerLobed();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerType = "lobed";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerLobed.lobedFanOrBlowerHasBallBearings = true; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData fan_or_blowerlobeddexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerLobed.extraFaultData = null;
                                        fan_or_blowerlobeddexfd.blowerlobes = String.IsNullOrEmpty(componentinfodriven.input_lobes) ? 0 : int.Parse(componentinfodriven.input_lobes);
                                        fan_or_blowerlobeddexfd.idlerlobes = String.IsNullOrEmpty(componentinfodriven.Idler_lobes) ? 0 : int.Parse(componentinfodriven.Idler_lobes);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerLobed.extraFaultData = fan_or_blowerlobeddexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "overhungrotor", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven Overhung fan_or_blower
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerOverhungRotor = new MIDCodeGeneration.Models.DrivenModels.FanOrBlowerOverhungRotor();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerType = "overhungrotor";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerOverhungRotor.fanStages = true; //fix
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerOverhungRotor.overhungRotorFanOrBlowerHasBearings = true; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData fan_or_bloweroverhungrotorexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerOverhungRotor.extraFaultData = null;
                                        fan_or_bloweroverhungrotorexfd.fanblades = String.IsNullOrEmpty(componentinfodriven.fanBlades) ? 0 : int.Parse(componentinfodriven.fanBlades);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerOverhungRotor.extraFaultData = fan_or_bloweroverhungrotorexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "supportedrotor", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven supportedrotor fan_or_blower
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerSupportedRotor = new MIDCodeGeneration.Models.DrivenModels.FanOrBlowerSupportedRotor();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerType = "supportedrotor";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerType;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerSupportedRotor.supportedRotorFanOrBlowerHasBearings = true; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData fan_or_blowersupportedrotorexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        //fix midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerSupportedRotor.fanStages = true;
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerSupportedRotor.extraFaultData = null;
                                        fan_or_blowersupportedrotorexfd.fanblades = String.IsNullOrEmpty(componentinfodriven.fanBlades) ? 0 : int.Parse(componentinfodriven.fanBlades); //fix
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.fan_or_blower.fan_or_blowerTypes.fan_or_blowerSupportedRotor.extraFaultData = fan_or_blowersupportedrotorexfd;
                                    }
                                }
                                if (String.Equals(componentinfodriven.Driven, "purifier_centrifuge", StringComparison.OrdinalIgnoreCase))
                                {
                                    //Driven purifier_centrifuge
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.purifier_centrifuge = new MIDCodeGeneration.Models.DrivenModels.PurifierCentrifuge();
                                    midcomp.machineComponentsForMIDgeneration.driven.drivenType = componentinfodriven.Driven;
                                    machinedriventype = midcomp.machineComponentsForMIDgeneration.driven.drivenType;
                                    machinedrivensubtype = "";
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.purifier_centrifuge.purifierDrivenBy = componentinfodriven.drivenc; // fix use drivenc
                                }
                                if (String.Equals(componentinfodriven.Driven, "decanter", StringComparison.OrdinalIgnoreCase))
                                {
                                    //Driven decanter
                                    midcomp.machineComponentsForMIDgeneration.driven.drivenType = componentinfodriven.Driven;
                                    machinedriventype = midcomp.machineComponentsForMIDgeneration.driven.drivenType;
                                    machinedrivensubtype = "";
                                }
                                if (String.Equals(componentinfodriven.Driven, "generator", StringComparison.OrdinalIgnoreCase))
                                {
                                    //Driven generator
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.generator = new MIDCodeGeneration.Models.DrivenModels.Generator();
                                    midcomp.machineComponentsForMIDgeneration.driven.drivenType = componentinfodriven.Driven;
                                    machinedriventype = midcomp.machineComponentsForMIDgeneration.driven.drivenType;
                                    machinedrivensubtype = "";

                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.generator.bearingType = String.IsNullOrEmpty(componentinfodriven.drivenbrgs) ? null : componentinfodriven.drivenbrgs;
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.generator.exciter = String.IsNullOrEmpty(componentinfodriven.exc) ? false : componentinfodriven.exc == "Yes" ? true : false;
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.generator.exciterOverhungOrSupported = String.IsNullOrEmpty(componentinfodriven.excOH) ? null : componentinfodriven.excOH;
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.generator.drivenBy = String.IsNullOrEmpty(componentinfodriven.gendriver) ? null : componentinfodriven.gendriver;
                                    // midcomp.machineComponentsForMIDgeneration.driven.drivens.generator.drivenBy = "NOTdieselengine"; //fix

                                    //MIDCodeGeneration.Models.CommonModels.ExtraFaultData generatorexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                    //midcomp.machineComponentsForMIDgeneration.driven.drivens.generator.extraFaultData = null;
                                    //generatorexfd.generatorbars = 2;  //not available from RPS per Dan White 20210816
                                    //midcomp.machineComponentsForMIDgeneration.driven.drivens.generator.extraFaultData = generatorexfd;
                                }
                                if (String.Equals(componentinfodriven.Driven, "vacuumpump", StringComparison.OrdinalIgnoreCase))
                                {
                                    //Driven vacuumpump
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump = new MIDCodeGeneration.Models.DrivenModels.Vacuumpump();
                                    midcomp.machineComponentsForMIDgeneration.driven.drivenType = componentinfodriven.Driven;
                                    machinedriventype = midcomp.machineComponentsForMIDgeneration.driven.drivenType;
                                    if (String.Equals(componentinfodriven.drivenc, "Centrifugal vacuumpump", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven Centrifugal vacuumpump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpCentrifugal = new MIDCodeGeneration.Models.DrivenModels.VacuumpumpCentrifugal();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumptype = "centrifugal";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumptype;

                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpCentrifugal.rotorOverhung = String.IsNullOrEmpty(componentinfodriven.rotorOH) ? false : componentinfodriven.rotorOH == "Yes" ? true : false;
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpCentrifugal.impellerOnMainShaft = String.IsNullOrEmpty(componentinfodriven.drivenbb) ? false : componentinfodriven.drivenbb == "Yes" ? true : false;
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpCentrifugal.bearingsType
                                            = String.IsNullOrEmpty(componentinfodriven.DriverTbrg) ? null : componentinfodriven.DriverTbrg.Substring(0, 3) == "Yes" ? componentinfodriven.DriverTbrg.Substring(5) : null;  //Fix - what about "journal", "ball", "no", "yes"
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpCentrifugal.thrustBearing = "journal"; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData vpumpcntrfglexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpCentrifugal.extraFaultData = null;
                                        vpumpcntrfglexfd.pumpvanes = String.IsNullOrEmpty(componentinfodriven.vanes) ? 0 : int.Parse(componentinfodriven.vanes);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpCentrifugal.extraFaultData = vpumpcntrfglexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "axialrecip vacuumpump", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven axialrecip vacuumpump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpAxialRecip = new MIDCodeGeneration.Models.DrivenModels.VacuumpumpAxialRecip();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumptype = "axialrecip";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumptype;

                                        // fix midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpAxialRecip.attachedOilPump = String.IsNullOrEmpty(componentinfodriven.attachedoilpump) ? false : componentinfodriven.attachedoilpump == "Yes" ? true : false;
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpAxialRecip.bearingsType = "ballbearings"; //fix
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpAxialRecip.thrustBearing = "journalthrust"; //fix

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData vpumpaxialrecipexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpAxialRecip.extraFaultData = null;
                                        //fix vpumpaxialrecipexfd.pumppistons = String.IsNullOrEmpty(componentinfodriven.pistons) ? 0 : int.Parse(componentinfodriven.pistons);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpAxialRecip.extraFaultData = vpumpaxialrecipexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "radialrecip vacuumpump", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven radialrecip vacuumpump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpRadialRecip = new MIDCodeGeneration.Models.DrivenModels.VacuumpumpRadialRecip();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumptype = "radialrecip";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumptype;

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData vpumpradialrecipexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpRadialRecip.extraFaultData = null;
                                        //fix vpumpradialrecipexfd.pumppistons = String.IsNullOrEmpty(componentinfodriven.pistons) ? 0 : int.Parse(componentinfodriven.pistons);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpRadialRecip.extraFaultData = vpumpradialrecipexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "reciprocating vacuumpump", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven reciprocating vacuumpump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpReciprocating = new MIDCodeGeneration.Models.DrivenModels.VacuumpumpReciprocating();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumptype = "reciprocating";
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpReciprocating.bearingsType = "crankshaftjournalbearingsatendonly";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumptype;

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData vpumpreciprocatingexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpReciprocating.extraFaultData = null;
                                        //fix vpumpreciprocatingexfd.pumppistons = String.IsNullOrEmpty(componentinfodriven.pistons) ? 0 : int.Parse(componentinfodriven.pistons);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpReciprocating.extraFaultData = vpumpreciprocatingexfd;
                                    }
                                    if (String.Equals(componentinfodriven.drivenc, "lobed vacuumpump", StringComparison.OrdinalIgnoreCase))
                                    {
                                        //Driven lobed vacuumpump
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpLobed = new MIDCodeGeneration.Models.DrivenModels.VacuumpumpLobed();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumptype = "lobed";
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpLobed.bearingsType = "ballbearings";
                                        machinedrivensubtype = midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumptype;

                                        MIDCodeGeneration.Models.CommonModels.ExtraFaultData vpumplobedexfd = new MIDCodeGeneration.Models.CommonModels.ExtraFaultData();
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpLobed.extraFaultData = null;
                                        //fix vpumplobedexfd.pumplobes = String.IsNullOrEmpty(componentinfodriven.lobes) ? 0 : int.Parse(componentinfodriven.lobes);
                                        //fix vpumplobedexfd.idlerlobes = String.IsNullOrEmpty(componentinfodriven.idlerlobes) ? 0 : int.Parse(componentinfodriven.idlerlobes);
                                        midcomp.machineComponentsForMIDgeneration.driven.drivens.vacuumpump.vacuumpumpTypes.vacuumpumpLobed.extraFaultData = vpumplobedexfd;
                                    }
                                }
                                if (String.Equals(componentinfodriven.Driven, "spindle_or_shaft_or_bearing", StringComparison.OrdinalIgnoreCase))
                                {
                                    //Driven spindle_or_shaft_or_bearing
                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.spindle_or_shaft_or_bearing = new MIDCodeGeneration.Models.DrivenModels.SpindleOrShaftOrBearing();
                                    midcomp.machineComponentsForMIDgeneration.driven.drivenType = componentinfodriven.Driven;
                                    machinedriventype = midcomp.machineComponentsForMIDgeneration.driven.drivenType;
                                    machinedrivensubtype = "";

                                    midcomp.machineComponentsForMIDgeneration.driven.drivens.spindle_or_shaft_or_bearing.spindleShaftBearing = "spindle"; //fix
                                }
                            }

                            // Call MIDCodeGeneration API
                            MIDCodeDetails midcoderesults = new MIDCodeDetails();
                            MIDCodeGenerationLibrary.Helpers.WebAPIClientHelper.BaseUrl = "https://azdevweb.wptss.com"; //jcm fix 
                            MIDCodeGenerationLibrary.Helpers.WebAPIClientHelper.UriPrefix = "MIDDerivation";
                            MIDCodeGenerationLibrary.Helpers.WebAPIClientHelper.InitializeClient();
                            
                            midcoderesults = await ProcessPostMIDCodeGenerationLibrary.PostMIDCodeGeneration(midcomp, customerId, MachineBeingProcessed.name, levelname, levelnametop);

                            //verify returned components
                            if (midcoderesults == null)
                            {
                                 ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); MIDDerivation did not return any data.  No MID, machine or location created.");
                            }
                            else
                            {

                                var numberoferrorsincomponents = 0;
                                if (component_present_driver == true && (midcoderesults.driver == null || midcoderesults.driver.ComponentCode.Length == 0))
                            {
                                numberoferrorsincomponents++;
                                 ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); MIDDerivation did not return an expected Driver.");
                            }
                                if (component_present_coupling1 == true && (midcoderesults.coupling1 == null || midcoderesults.coupling1.ComponentCode.Length == 0))
                            {
                                numberoferrorsincomponents++;
                                 ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); MIDDerivation did not return an expected Coupling1.");
                            }
                                if (component_present_intermediate == true && (midcoderesults.intermediate == null || midcoderesults.intermediate.ComponentCode.Length == 0))
                            {
                                numberoferrorsincomponents++;
                                 ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); MIDDerivation did not return an expected Intermediate.");
                            }
                                if (component_present_coupling2 == true && (midcoderesults.coupling2 == null || midcoderesults.coupling2.ComponentCode.Length == 0))
                            {
                                numberoferrorsincomponents++;
                                ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); AreaName: {AreaBeingProcessed.name}; MIDDerivation did not return an expected Coupling2.");
                            }
                                if (component_present_driven == true && (midcoderesults.driven == null || midcoderesults.driven.ComponentCode.Length == 0))
                            {
                                numberoferrorsincomponents++;
                                 ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); MIDDerivation did not return an expected Driven.");
                            }
                                if (midcoderesults.faultCodeMatrix == null)
                            {
                                numberoferrorsincomponents++;
                                 ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); MIDDerivation did not return a faultCodeMatrix.");
                            }

                                // ProcessMID - begin

                                // Add new MID
                                newMID.MIDID = 0;
                                newMID.SpeedUnitID = 2; // fix rpm ratio speed
                                newMID.MIDRating = 100;
                                //newMID.MIDNumber = (short)pullplant.midStart; 
                                if (nextMIDNumber != 0) { newMID.MIDNumber = nextMIDNumber++;  };
                                newMID.MIDName = MachineBeingProcessed.name;
                                newMID.MIDNominalSpeed = (float)machinenominalspeed; //Driver RPM --// fix rpm ratio speed 
                                newMID.Orientation = machineorientation; //fix H or V
                                double gbrationumerator = 1;
                                double gbratiodemoninator = 1;
                                string gbratiospeedchange = "nochange";
                                if (MachineBeingProcessed.gearbox != null && MachineBeingProcessed.gearbox.ratio != null)
                                {
                                    if (MachineBeingProcessed.gearbox.ratio.Length > 0)
                                    {
                                        List<LocationRatios> LocRatioList = ProcessSelections.ParseRatio("{R1 = " + MachineBeingProcessed.gearbox.ratio.Replace(":", "/") + " }");
                                        if (LocRatioList.Count > 0)
                                        {
                                            gbrationumerator = LocRatioList[0].numerator;
                                            gbratiodemoninator = LocRatioList[0].denominator;
                                            gbratiospeedchange = LocRatioList[0].speedchange;
                                        }
                                    }
                                }
                                EARPSInterfaceServiceLogger.AddLogEntry($"{componentinfodriver.cpl1rationumerator} / {componentinfodriver.cpl1ratiodenominator} = {componentinfodriver.cpl1rationumerator / componentinfodriver.cpl1ratiodenominator}", null);
                                EARPSInterfaceServiceLogger.AddLogEntry($"{gbrationumerator} / {gbratiodemoninator} = {gbrationumerator / gbratiodemoninator}", null);
                                EARPSInterfaceServiceLogger.AddLogEntry($"{componentinfointermediate.LOC1rationumerator} / {componentinfointermediate.LOC1ratiodenominator} = {componentinfointermediate.LOC1rationumerator / componentinfointermediate.LOC1ratiodenominator}", null);
                                EARPSInterfaceServiceLogger.AddLogEntry($"{componentinfointermediate.LOC2rationumerator} / {componentinfointermediate.LOC2ratiodenominator} = {componentinfointermediate.LOC2rationumerator / componentinfointermediate.LOC2ratiodenominator}", null);
                                EARPSInterfaceServiceLogger.AddLogEntry($"{componentinfointermediate.LOC3rationumerator} / {componentinfointermediate.LOC3ratiodenominator} = {componentinfointermediate.LOC3rationumerator / componentinfointermediate.LOC3ratiodenominator}", null);
                                EARPSInterfaceServiceLogger.AddLogEntry($"{componentinfointermediate.LOC4rationumerator} / {componentinfointermediate.LOC4ratiodenominator} = {componentinfointermediate.LOC4rationumerator / componentinfointermediate.LOC4ratiodenominator}", null);
                                EARPSInterfaceServiceLogger.AddLogEntry($"{machinedrivenrpm} / {machinenominalspeed} = {machinedrivenrpm / machinenominalspeed}", null);

                                bool speedchangedbyratio = false;
                                if (gbrationumerator == componentinfointermediate.LOC1rationumerator && gbratiodemoninator == componentinfointermediate.LOC1ratiodenominator)
                                {
                                    newMID.SecondarySpeedRatio = (
                                        ((float)componentinfodriver.cpl1rationumerator / (float)componentinfodriver.cpl1ratiodenominator) *
                                        ((float)componentinfointermediate.LOC1rationumerator / (float)componentinfointermediate.LOC1ratiodenominator) *
                                        ((float)componentinfointermediate.LOC2rationumerator / (float)componentinfointermediate.LOC2ratiodenominator) *
                                        ((float)componentinfointermediate.LOC3rationumerator / (float)componentinfointermediate.LOC3ratiodenominator) *
                                        ((float)componentinfointermediate.LOC4rationumerator / (float)componentinfointermediate.LOC4ratiodenominator)
                                        )
                                        ;// fix rpm ratio speed
                                }
                                else
                                {
                                    newMID.SecondarySpeedRatio = (
                                        ((float)componentinfodriver.cpl1rationumerator / (float)componentinfodriver.cpl1ratiodenominator) *
                                        ((float)gbrationumerator / (float)gbratiodemoninator) *
                                        ((float)componentinfointermediate.LOC1rationumerator / (float)componentinfointermediate.LOC1ratiodenominator) *
                                        ((float)componentinfointermediate.LOC2rationumerator / (float)componentinfointermediate.LOC2ratiodenominator) *
                                        ((float)componentinfointermediate.LOC3rationumerator / (float)componentinfointermediate.LOC3ratiodenominator) *
                                        ((float)componentinfointermediate.LOC4rationumerator / (float)componentinfointermediate.LOC4ratiodenominator)
                                        )
                                        ;// fix rpm ratio speed
                                }
                                if (newMID.SecondarySpeedRatio == 1)
                                {
                                    if (machinedrivenrpm > 0 && machinenominalspeed > 0)
                                    {
                                        speedchangedbyratio = false;
                                        var tempsr = (float)ProcessSelections.calculateChangedSpeed("overall", (int)machinedrivenrpm, (int)machinenominalspeed, 1, 1, ref speedchangedbyratio);// fix rpm ratio speed
                                        EARPSInterfaceServiceLogger.AddLogEntry($"{tempsr} {speedchangedbyratio}", null);
                                        if (speedchangedbyratio == true)
                                        {
                                            newMID.SecondarySpeedRatio = ((float)machinedrivenrpm / (float)machinenominalspeed);
                                        }
                                    }
                                }
                                EARPSInterfaceServiceLogger.AddLogEntry($"{newMID.SecondarySpeedRatio}", null);
                                newMID.Locked = 0; 

                                //FaultFrequenciesList for each Fault in input
                                List<FaultFrequency> ffList = new List<FaultFrequency>();
                                for (var i = 0; i < midcoderesults.faultCodeMatrix.rows.Count; i++)
                                {
                                    ffList.Add(new FaultFrequency
                                    {
                                        //FaultFrequencyLookupID = 0, //fix
                                        //MIDID = 0, //fix
                                        FaultCode = midcoderesults.faultCodeMatrix.rows[i].frequencyCode, //fix from input
                                        FaultRatio = Convert.ToDouble(midcoderesults.faultCodeMatrix.rows[i].faultcode), //fix from input
                                        SecondaryShaft = "0", //fix from input??
                                        FinalRatio = Convert.ToDouble(midcoderesults.faultCodeMatrix.rows[i].faultcode) //fix from input
                                    }); ;
                                }
                                newMID.FaultFrequenciesList = ffList;

                                //ComponentsList for each Component
                                machinelocs componentpickups = new machinelocs();
                                componentpickups = ProcessPickupCodes.SetupPickupCodes(midcomp, componentinfointermediate);
                                    
                                if (midcomp.machineComponentsForMIDgeneration.driver != null && componentpickups.drvrpickupcode.Length == 0)
                            {
                                 ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); Pickup codes are empty for Driver.");
                            }
                                if (midcomp.machineComponentsForMIDgeneration.coupling1 != null && componentpickups.cpl1pickupcode.Length == 0)
                            {
                                 ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); Pickup codes are empty for Coupling1.");
                            }
                                if (midcomp.machineComponentsForMIDgeneration.intermediate != null && componentpickups.intmpickupcode.Length == 0)
                            {
                                 ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); Pickup codes are empty for Driver.");
                            }
                                if (midcomp.machineComponentsForMIDgeneration.coupling2 != null && componentpickups.cpl2pickupcode.Length == 0)
                            {
                                 ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); Pickup codes are empty for Coupling2.");
                            }
                                if (midcomp.machineComponentsForMIDgeneration.driven != null && componentpickups.drvnpickupcode.Length == 0)
                            {
                                 ProcessExceptions.WriteExceptionLine(levelnametop, $"MachineName: {MachineBeingProcessed.name} (Area: {AreaBeingProcessed.name}); Pickup codes are empty for Driven.");
                            }

                                List<Component> componentList = new List<Component>();
                                short cntcomponents = 1;
                            
                                if (!(midcoderesults.driver is null) && (midcoderesults.driver.ComponentCode.Length > 0))
                            {
                                //DriveRec
                                SAAIDILibrary.Models.Drive newdrive = new SAAIDILibrary.Models.Drive();
                                //newMID.Drive.DriveID = 0; //fix
                                //newMID.Drive.MIDID = 0; //fix
                                newdrive.DiameterOne = 0; //fix
                                newdrive.DiameterTwo = 0; //fix
                                newdrive.Distance = 0; //fix
                                newdrive.Sprockets = 0; //fix
                                newdrive.CreatedBySiteID = 1;                        //newMID.Drive.CreatedBySiteID
                                newdrive.ModifiedBySiteID = 1;                        //newMID.Drive.ModifiedBySiteID
                                newMID.Drive = newdrive;

                                ComponentPickupCodesInCompRec joespickupcodes = new ComponentPickupCodesInCompRec();
                                ComponentPickupCodesInCompRec danspickupcodes = new ComponentPickupCodesInCompRec();
                                joespickupcodes = LoadData.LoadPickupsdata(midcoderesults.driver.PickupCode);
                                EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; JOESPUs driver PUs: {midcoderesults.driver.PickupCode}; PU1: {joespickupcodes.PickupOne}; PU2: {joespickupcodes.PickupTwo}; PU3: {joespickupcodes.PickupThree}; PU4: {joespickupcodes.PickupFour}", null);
                                //danspickupcodes = LoadData.LoadPickupsdata(componentpickups.drvrpickupcode);
                                //EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; DANSPUs driver PUs: {componentpickups.drvrpickupcode}; PU1: {danspickupcodes.PickupOne}; PU2: {danspickupcodes.PickupTwo}; PU3: {danspickupcodes.PickupThree}; PU4: {danspickupcodes.PickupFour}", null);

                                componentList.Add(new Component
                                {
                                    //ComponentID = 0, //fix
                                    //MIDID = 0, //fix
                                    ComponentCode = Decimal.Parse(midcoderesults.driver.ComponentCode), //fix
                                    ComponentOrder = cntcomponents++, //1 through 5 in order: driver,coupling1,intermediate,coupling2,driven
                                    //PickupCode = midcoderesults.driver.PickupCode,
                                    //PickupOne = joespickupcodes.PickupOne,
                                    //PickupTwo = joespickupcodes.PickupTwo,
                                    //PickupThree = joespickupcodes.PickupThree,
                                    //PickupFour = joespickupcodes.PickupFour,
                                    PickupCode = componentpickups.drvrpickupcode,
                                    PickupOne = componentpickups.drvrpickup1,
                                    PickupTwo = componentpickups.drvrpickup2,
                                    PickupThree = componentpickups.drvrpickup3,
                                    PickupFour = componentpickups.drvrpickup4,
                                    CreatedBySiteID = 1,
                                    ModifiedBySiteID = 1
                                });;;
                            }
                                if (!(midcoderesults.coupling1 is null) && (midcoderesults.coupling1.ComponentCode.Length > 0))
                            {
                                ComponentPickupCodesInCompRec joespickupcodes = new ComponentPickupCodesInCompRec();
                                ComponentPickupCodesInCompRec danspickupcodes = new ComponentPickupCodesInCompRec();
                                joespickupcodes = LoadData.LoadPickupsdata(midcoderesults.coupling1.PickupCode);
                                EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; JOESPUs coupling1 PUs: {midcoderesults.coupling1.PickupCode}; PU1: {joespickupcodes.PickupOne}; PU2: {joespickupcodes.PickupTwo}; PU3: {joespickupcodes.PickupThree}; PU4: {joespickupcodes.PickupFour}", null);
                                //danspickupcodes = LoadData.LoadPickupsdata(componentpickups.cpl1pickupcode);
                                //EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; DANSPUs coupling1 PUs: {componentpickups.cpl1pickupcode}; PU1: {danspickupcodes.PickupOne}; PU2: {danspickupcodes.PickupTwo}; PU3: {danspickupcodes.PickupThree}; PU4: {danspickupcodes.PickupFour}", null);

                                componentList.Add(new Component
                                {
                                    //ComponentID = 0, //fix
                                    //MIDID = 0, //fix
                                    ComponentCode = Decimal.Parse(midcoderesults.coupling1.ComponentCode), //fix
                                    ComponentOrder = cntcomponents++, //1 through 5 in order: driver,coupling1,intermediate,coupling2,driven
                                    //PickupCode = midcoderesults.coupling1.PickupCode,
                                    //PickupOne = joespickupcodes.PickupOne,
                                    //PickupTwo = joespickupcodes.PickupTwo,
                                    //PickupThree = joespickupcodes.PickupThree,
                                    //PickupFour = joespickupcodes.PickupFour,
                                    PickupCode = componentpickups.cpl1pickupcode,
                                    PickupOne = componentpickups.cpl1pickup1,
                                    PickupTwo = componentpickups.cpl1pickup2,
                                    PickupThree = componentpickups.cpl1pickup3,
                                    PickupFour = componentpickups.cpl1pickup4,
                                    CreatedBySiteID = 1,
                                    ModifiedBySiteID = 1
                                });
                            }
                                if (!(midcoderesults.intermediate is null) && (midcoderesults.intermediate.ComponentCode.Length > 0))
                            {
                                //GearboxRec  //fix - determine if it is a gearbox from the pullplant data
                                SAAIDILibrary.Models.Gearbox newgearbox = new SAAIDILibrary.Models.Gearbox();
                                // fix this if the above is deleted SAAIDILibrary.Models.Gearbox newgearbox = new SAAIDILibrary.Models.Gearbox();
                                //newMID.Gearbox.GearboxID = 0; //fix
                                //newMID.Gearbox.MIDID = 0; //fix
                                newgearbox.GearboxID = 0;
                                newgearbox.MIDID = 0;
                                newgearbox.StageOneInput = 0; //fix
                                newgearbox.StageOneOutput = 0; //fix
                                newgearbox.StageTwoInput = 0; //fix
                                newgearbox.StageTwoOutput = 0; //fix
                                newgearbox.StageThreeInput = 0; //fix
                                newgearbox.StageThreeOutput = 0; //fix
                                newgearbox.StageFourInput = 0; //fix
                                newgearbox.StageFourOutput = 0; //fix
                                newgearbox.CreatedBySiteID = 0;
                                newgearbox.ModifiedBySiteID = 0;
                                //newMID.Gearbox.RecordLastUpdated

                                newMID.Gearbox = newgearbox;

                                ComponentPickupCodesInCompRec joespickupcodes = new ComponentPickupCodesInCompRec();
                                ComponentPickupCodesInCompRec danspickupcodes = new ComponentPickupCodesInCompRec();
                                joespickupcodes = LoadData.LoadPickupsdata(midcoderesults.intermediate.PickupCode);
                                EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; JOESPUs intermediate PUs: {midcoderesults.intermediate.PickupCode}; PU1: {joespickupcodes.PickupOne}; PU2: {joespickupcodes.PickupTwo}; PU3: {joespickupcodes.PickupThree}; PU4: {joespickupcodes.PickupFour}", null);
                                //danspickupcodes = LoadData.LoadPickupsdata(componentpickups.intmpickupcode);
                                //EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; DANSPUs intermediate PUs: {componentpickups.intmpickupcode}; PU1: {danspickupcodes.PickupOne}; PU2: {danspickupcodes.PickupTwo}; PU3: {danspickupcodes.PickupThree}; PU4: {danspickupcodes.PickupFour}", null);

                                componentList.Add(new Component
                                {
                                    //ComponentID = 0, //fix
                                    //MIDID = 0, //fix
                                    ComponentCode = Decimal.Parse(midcoderesults.intermediate.ComponentCode), //fix
                                    ComponentOrder = cntcomponents++, //1 through 5 in order: driver,coupling1,intermediate,coupling2,driven
                                    //PickupCode = midcoderesults.intermediate.PickupCode,
                                    //PickupOne = joespickupcodes.PickupOne,
                                    //PickupTwo = joespickupcodes.PickupTwo,
                                    //PickupThree = joespickupcodes.PickupThree,
                                    //PickupFour = joespickupcodes.PickupFour,
                                    PickupCode = componentpickups.intmpickupcode,
                                    PickupOne = componentpickups.intmpickup1,
                                    PickupTwo = componentpickups.intmpickup2,
                                    PickupThree = componentpickups.intmpickup3,
                                    PickupFour = componentpickups.intmpickup4,
                                    CreatedBySiteID = 1,
                                    ModifiedBySiteID = 1
                                });
                            }
                                if (!(midcoderesults.coupling2 is null) && (midcoderesults.coupling2.ComponentCode.Length > 0))
                            {
                                ComponentPickupCodesInCompRec joespickupcodes = new ComponentPickupCodesInCompRec();
                                ComponentPickupCodesInCompRec danspickupcodes = new ComponentPickupCodesInCompRec();
                                joespickupcodes = LoadData.LoadPickupsdata(midcoderesults.coupling2.PickupCode);
                                EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; JOESPUs coupling2 PUs: {midcoderesults.coupling2.PickupCode}; PU1: {joespickupcodes.PickupOne}; PU2: {joespickupcodes.PickupTwo}; PU3: {joespickupcodes.PickupThree}; PU4: {joespickupcodes.PickupFour}", null);
                                //danspickupcodes = LoadData.LoadPickupsdata(componentpickups.cpl2pickupcode);
                                //EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; DANSPUs coupling2 PUs: {componentpickups.cpl2pickupcode}; PU1: {danspickupcodes.PickupOne}; PU2: {danspickupcodes.PickupTwo}; PU3: {danspickupcodes.PickupThree}; PU4: {danspickupcodes.PickupFour}", null);

                                componentList.Add(new Component
                                {
                                    //ComponentID = 0, //fix
                                    //MIDID = 0, //fix
                                    ComponentCode = Decimal.Parse(midcoderesults.coupling2.ComponentCode), //fix
                                    ComponentOrder = cntcomponents++, //1 through 5 in order: driver,coupling1,intermediate,coupling2,driven
                                    //PickupCode = midcoderesults.coupling2.PickupCode,
                                    //PickupOne = joespickupcodes.PickupOne,
                                    //PickupTwo = joespickupcodes.PickupTwo,
                                    //PickupThree = joespickupcodes.PickupThree,
                                    //PickupFour = joespickupcodes.PickupFour,
                                    PickupCode = componentpickups.cpl2pickupcode,
                                    PickupOne = componentpickups.cpl2pickup1,
                                    PickupTwo = componentpickups.cpl2pickup2,
                                    PickupThree = componentpickups.cpl2pickup3,
                                    PickupFour = componentpickups.cpl2pickup4,
                                    CreatedBySiteID = 1,
                                    ModifiedBySiteID = 1
                                });
                            }
                                if (!(midcoderesults.driven is null) && (midcoderesults.driven.ComponentCode.Length > 0))
                            {
                                ComponentPickupCodesInCompRec joespickupcodes = new ComponentPickupCodesInCompRec();
                                ComponentPickupCodesInCompRec danspickupcodes = new ComponentPickupCodesInCompRec();
                                joespickupcodes = LoadData.LoadPickupsdata(midcoderesults.driven.PickupCode);
                                EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; JOESPUs driven PUs: {midcoderesults.driven.PickupCode}; PU1: {joespickupcodes.PickupOne}; PU2: {joespickupcodes.PickupTwo}; PU3: {joespickupcodes.PickupThree}; PU4: {joespickupcodes.PickupFour}", null);
                                //danspickupcodes = LoadData.LoadPickupsdata(componentpickups.drvnpickupcode);
                                //.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; DANSPUs driven PUs: {componentpickups.drvnpickupcode}; PU1: {danspickupcodes.PickupOne}; PU2: {danspickupcodes.PickupTwo}; PU3: {danspickupcodes.PickupThree}; PU4: {danspickupcodes.PickupFour}", null);

                                componentList.Add(new Component
                                {
                                    //ComponentID = 0, //fix
                                    //MIDID = 0, //fix
                                    ComponentCode = Decimal.Parse(midcoderesults.driven.ComponentCode), //fix
                                    ComponentOrder = cntcomponents++, //1 through 5 in order: driver,coupling1,intermediate,coupling2,driven
                                    //PickupCode = midcoderesults.driven.PickupCode,
                                    //PickupOne = joespickupcodes.PickupOne,
                                    //PickupTwo = joespickupcodes.PickupTwo,
                                    //PickupThree = joespickupcodes.PickupThree,
                                    //PickupFour = joespickupcodes.PickupFour,
                                    PickupCode = componentpickups.drvnpickupcode,
                                    PickupOne = componentpickups.drvnpickup1,
                                    PickupTwo = componentpickups.drvnpickup2,
                                    PickupThree = componentpickups.drvnpickup3,
                                    PickupFour = componentpickups.drvnpickup4,
                                    CreatedBySiteID = 1,
                                    ModifiedBySiteID = 1
                                });
                            }
                                newMID.ComponentsList = componentList;

                                var postMIDResponse = await ProcessPostMID.PostMID(newMID, customerId, levelname);
                                EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; newMID: {postMIDResponse.data.MIDID}", null);

                                // ProcessMID - end

                                //Process Drawing for a Machine
                                var drawingidposted = 0;
                                if (MachineBeingProcessed.attachments != null)
                                {
                                    DrawingRequest newdrawing = new DrawingRequest();
                                    var drawingattachment = from da in MachineBeingProcessed.attachments select da;
                                    foreach (var MachineDABeingProcessed in drawingattachment)
                                    {
                                        if (MachineDABeingProcessed.type.ToUpper() == "OVERALL")
                                        {
                                            newdrawing.drawingUUID = MachineDABeingProcessed.id;
                                            newdrawing.drawingName = MachineBeingProcessed.name + " Overall";
                                            newdrawing.drawingType = (short)(MachineDABeingProcessed.version);
                                            newdrawing.originalFiletype = MachineDABeingProcessed.mimeType.Substring(1);
                                            newdrawing.originalFileName = MachineDABeingProcessed.name + "." + MachineDABeingProcessed.mimeType;
                                            var imagebloburi = MachineDABeingProcessed.uri.Replace("https://walkaround.blob.core.windows.net/", "").Replace(ServiceSettings.azureBlogContainer + "/", "").Replace("%20", " ").Replace("%23", "#");

                                            EARPSInterfaceServiceLogger.AddLogEntry($"Photo size (from json): {MachineDABeingProcessed.size}", null);

                                            // Write the unaltered picture into the database 1 - begin

                                            EARPSInterfaceServiceLogger.AddLogEntry($"PhotoImage1", null);

                                            // No change in image storage
                                            string drawingimagestr1 = Helper.Base64EncodeBytes(await ProcessBlobs.GetBlob(ServiceSettings.azureBlogAccountKey, ServiceSettings.azureBlogContainer, imagebloburi));
                                            byte[] bytearrayofblob1 = await ProcessBlobs.GetBlob(ServiceSettings.azureBlogAccountKey, ServiceSettings.azureBlogContainer, imagebloburi);
                                            Bitmap bitmapofblob1 = new Bitmap(new MemoryStream(bytearrayofblob1));
                                            bitmapofblob1.Save(@"jsonfiles\" + levelname + @"\blobimage1NoQuality.jpg");
                                            int drawingSize1 = drawingimagestr1.Length;
                                            EARPSInterfaceServiceLogger.AddLogEntry($"PhotoLength of string after conversion of byte array to encoded string original: {drawingSize1}", null);

                                            newdrawing.originalFileName = MachineDABeingProcessed.name + "_1"+ "." + MachineDABeingProcessed.mimeType;
                                            newdrawing.drawingName = MachineBeingProcessed.name + " Overall_1";
                                            newdrawing.drawingimage = drawingimagestr1;
                                            newdrawing.drawingSize = newdrawing.drawingimage.Length;
                                            
                                            var postdrawingresult1 = await ProcessPostDrawing.PostDrawing(newdrawing, customerId, levelname);
                                            
                                            drawingidposted = postdrawingresult1.data.DrawingID;
                                            EARPSInterfaceServiceLogger.AddLogEntry($"drawing1 ID >> {postdrawingresult1.data.DrawingID}; length >> {newdrawing.drawingSize}", null);

                                            //Write the unaltered picture into the database 1 - end

                                            // Write the altered picture into the database 2 - begin

                                            EARPSInterfaceServiceLogger.AddLogEntry($"PhotoImage2", null);

                                            // change image storage
                                            byte[] bytearrayofblob2 = await ProcessBlobs.GetBlob(ServiceSettings.azureBlogAccountKey, ServiceSettings.azureBlogContainer, imagebloburi);
                                            EARPSInterfaceServiceLogger.AddLogEntry($"Photo2Length of byte array original: {bytearrayofblob2.Length}", null);
                                            Bitmap bitmapofblob2 = new Bitmap(new MemoryStream(bytearrayofblob2));
                                            //Bitmap bitmapofblobchgd = bitmapofblob;
                                            Bitmap bitmapofblobchgd2 = ProcessPhotos.ProcessPhoto(bitmapofblob2);
                                            ImageConverter imgCon = new ImageConverter();
                                            byte[] bytearrayofblobchgd2 = (byte[])imgCon.ConvertTo(bitmapofblobchgd2, typeof(byte[]));
                                            EARPSInterfaceServiceLogger.AddLogEntry($"Photo2Length of byte array altered: {bytearrayofblobchgd2.Length}", null);
                                            string drawingimagestr2 = Helper.Base64EncodeBytes(bytearrayofblobchgd2);
                                            EARPSInterfaceServiceLogger.AddLogEntry($"Photo2Length of string after conversion of byte array to encoded string altered: {drawingimagestr2.Length}", null);
                                            int drawingSize2 = drawingimagestr2.Length;

                                            newdrawing.originalFileName = MachineDABeingProcessed.name + "_2" + "." + MachineDABeingProcessed.mimeType;
                                            newdrawing.drawingName = MachineBeingProcessed.name + " Overall_2";
                                            newdrawing.drawingimage = drawingimagestr2;
                                            newdrawing.drawingSize = newdrawing.drawingimage.Length;
                                            newdrawing.drawingUUID = Guid.Parse("76f44f60bd1d4a509012d2a7d948dd55");

                                            /*
                                            var postdrawingresult2 = await ProcessPostDrawing.PostDrawing(newdrawing, customerId, levelname);
                                            
                                            drawingidposted = postdrawingresult2.data.DrawingID;
                                            EARPSInterfaceServiceLogger.AddLogEntry($"drawing2 ID >> {postdrawingresult2.data.DrawingID}; length >> {newdrawing.drawingSize}", null);
                                            EARPSInterfaceServiceLogger.AddLogEntry($"DrawingName: {postdrawingresult2.data.DrawingName}; DrawingID: {postdrawingresult2.data.DrawingID}; DrawingUUID: {postdrawingresult2.data.DrawingUUID};", null);
                                            */

                                            // Write the altered picture into the database 2 - end

                                            //newdrawing.drawingimage = drawingimage1;
                                            //newdrawing.drawingSize = drawingSize1;


                                            //Bitmap bitmapphoto = new Bitmap( new MemoryStream(await ProcessBlobs.GetBlob(ServiceSettings.azureBlogAccountKey, ServiceSettings.azureBlogContainer, imagebloburi)));
                                            //Bitmap bitmapphotosmaller = ProcessPhotos.ProcessPhoto(bitmapphoto);
                                            //ImageConverter imgCon = new ImageConverter();
                                            //byte[] bytearrayofphotosmaller = (byte[])imgCon.ConvertTo(bitmapphotosmaller, typeof(byte[])); 
                                            //newdrawing.drawingimage = Helper.Base64EncodeBytes(bytearrayofphotosmaller);
                                            //newdrawing.drawingSize = newdrawing.drawingimage.Length;

                                            //bitmapofblob2.Save(@"jsonfiles\" + levelname + @"\blobimage2NoQuality.jpg");
                                            bitmapofblobchgd2.Save(@"jsonfiles\" + levelname + @"\blobimage2chgdNoQuality.jpg");
                                            EARPSInterfaceServiceLogger.AddLogEntry($"Photo bitmap size (original): {bitmapofblob2.Size}", null);
                                            EARPSInterfaceServiceLogger.AddLogEntry($"Photo bitmap size (changed): {bitmapofblobchgd2.Size}", null);

                                            EARPSInterfaceServiceLogger.AddLogEntry($"Photo saved ({bitmapofblob2.Size})" + @"jsonfiles\" + levelname + @"\blobimage2NoQuality.jpg", null);
                                            EARPSInterfaceServiceLogger.AddLogEntry($"Photo saved " + @"jsonfiles\" + levelname + @"\blobimagechgd2NoQuality.jpg", null);

                                            // Write the altered and re-read picture into the database 3 - begin

                                            ImageCodecInfo myImageCodecInfo;
                                            Encoder myEncoder;
                                            EncoderParameter myEncoderParameter;
                                            EncoderParameters myEncoderParameters;
                                            myEncoderParameters = new EncoderParameters(1);

                                            myImageCodecInfo = ProcessPhotos.GetEncoderInfo("image/jpeg");
                                            myEncoder = Encoder.Quality;
                                            myEncoderParameter = new EncoderParameter(myEncoder, 75L);
                                            myEncoderParameters.Param[0] = myEncoderParameter;

                                            bitmapofblobchgd2.Save(@"jsonfiles\" + levelname + @"\blobimagechgd3WithQuality.jpg", myImageCodecInfo, myEncoderParameters);
                                            EARPSInterfaceServiceLogger.AddLogEntry($"Photo saved ({bitmapofblob2.Size})" + @"jsonfiles\" + levelname + @"\blobimageWithQuality.jpg", null);
                                            EARPSInterfaceServiceLogger.AddLogEntry($"Photo saved ({bitmapofblobchgd2.Size})" + @"jsonfiles\" + levelname + @"\blobimagechgdWithQuality.jpg", null);

                                            // Read and Write the altered picture into the database - begin
                                            string drawingimagenew = Helper.Base64EncodeBytes(ProcessBlobs.GetPicFile(@"jsonfiles\" + levelname + @"\blobimage2chgdNoQuality.jpg"));

                                            newdrawing.originalFileName = MachineDABeingProcessed.name + "_3" + "." + MachineDABeingProcessed.mimeType;
                                            newdrawing.drawingName = MachineBeingProcessed.name + " Overall_3";
                                            newdrawing.drawingimage = drawingimagenew;
                                            newdrawing.drawingSize = newdrawing.drawingimage.Length;

                                            //var postdrawingresult3 = await ProcessPostDrawing.PostDrawing(newdrawing, customerId, levelname);
                                            newdrawing.drawingName = MachineBeingProcessed.name + " Overall";
                                            // Write the unaltered picture into the database - end

                                            newdrawing.drawingimage = drawingimagestr2;
                                            newdrawing.drawingSize = newdrawing.drawingimage.Length;
                                            newdrawing.drawingUUID = Guid.Parse("76f44f60bd1d4a509012d2a7d948dd66");

                                            /*
                                            var postdrawingresult3 = await ProcessPostDrawing.PostDrawing(newdrawing, customerId, levelname);
                                            
                                            drawingidposted = postdrawingresult3.data.DrawingID;
                                            EARPSInterfaceServiceLogger.AddLogEntry($"drawing3 ID >> {postdrawingresult3.data.DrawingID}; length >> {newdrawing.drawingSize}", null);
                                            EARPSInterfaceServiceLogger.AddLogEntry($"DrawingName: {postdrawingresult3.data.DrawingName}; DrawingID: {postdrawingresult3.data.DrawingID}; DrawingUUID: {postdrawingresult3.data.DrawingUUID};", null);
                                            */

                                            // Write the altered and re-read picture into the database 3 - end

                                        }
                                    }
                                }

                                // Add new machine
                                if (drawingidposted != 0) { newmachine.DrawingID = drawingidposted; }
                                newmachine.MachineID = 0;
                                newmachine.MachineUUID = MachineBeingProcessed.id; // jcm 20220317 Guid.Parse(MachineBeingProcessed.id);
                                newmachine.MIDID = postMIDResponse.data.MIDID;
                                newmachine.MachineName = MachineBeingProcessed.name;
                                newmachine.AreaUUID = AreaBeingProcessed.id;//jcm 20220317
                                newmachine.CMMSLink = String.IsNullOrEmpty(MachineBeingProcessed.cmmsId) ? "" : MachineBeingProcessed.cmmsId;
                                newmachine.NominalSpeed = (float)machinenominalspeed; // Driver RPM // fix rpm ratio speed use ???
                                if (newmachine.NominalSpeed == 0) { newmachine.NominalSpeed = 2222; }// fix rpm ratio speed

                                var postmachineresult = await ProcessPostMachine.PostMachine(newmachine, customerId, levelname);
                                EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}", null);

                                //Process MachineComponentMapping
                                //SAAIDILibrary.Models.MachineComponentMapping newmachinecomponentmapping = new SAAIDILibrary.Models.MachineComponentMapping();
                                //newmachinecomponentmapping.MachineID = postmachineresult.data.MachineID;
                                //newmachinecomponentmapping.Name = "";
                                //if (component_present_driver == true)
                                //{
                                //    newmachinecomponentmapping.ComponentCode = Decimal.Parse(midcoderesults.driver.ComponentCode);
                                //    // fix var postmachinecomponentmappingresult = await ProcessPostMachineComponentMapping.PostMachineComponentMapping(newmachinecomponentmapping, customerId, levelname);
                                //    // fix EARPSInterfaceServiceLogger.AddLogEntry($"DrawingName: {postdrawingresult.data.DrawingName}; DrawingID: {postdrawingresult.data.DrawingID}; DrawingUUID: {postdrawingresult.data.DrawingUUID};", null);
                                //}
                                //if (component_present_intermediate == true)
                                //{
                                //    newmachinecomponentmapping.ComponentCode = Decimal.Parse(midcoderesults.intermediate.ComponentCode);
                                //    // fix var postmachinecomponentmappingresult = await ProcessPostMachineComponentMapping.PostMachineComponentMapping(newmachinecomponentmapping, customerId, levelname);
                                //    // fix EARPSInterfaceServiceLogger.AddLogEntry($"DrawingName: {postdrawingresult.data.DrawingName}; DrawingID: {postdrawingresult.data.DrawingID}; DrawingUUID: {postdrawingresult.data.DrawingUUID};", null);
                                //}
                                //if (component_present_driven == true)
                                //{
                                //    newmachinecomponentmapping.ComponentCode = Decimal.Parse(midcoderesults.driven.ComponentCode);
                                //    // fix var postmachinecomponentmappingresult = await ProcessPostMachineComponentMapping.PostMachineComponentMapping(newmachinecomponentmapping, customerId, levelname);
                                //    // fix EARPSInterfaceServiceLogger.AddLogEntry($"DrawingName: {postdrawingresult.data.DrawingName}; DrawingID: {postdrawingresult.data.DrawingID}; DrawingUUID: {postdrawingresult.data.DrawingUUID};", null);
                                //}


                                //Process Locations in a Machine 

                                //levelname = levelname + @"\locations";
                                levelname = DebuggingHelper.Changelevelname(levelname, @"\locations", "add");
                                short machinelocations = 0;
                                speedchangedbyratio = false;
                                if (component_present_driver == true && midcomp.machineComponentsForMIDgeneration.driver.driverLocationNDE == true) // position 1
                                {
                                    EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; location: driverLocationNDE; interimrpm: {interimrpm}", null);
                                    var postlocationresult_driverLocationNDE = await CreateLocationToPost.BuildtoPostLocation(
                                        CompLoc,
                                        LocLOR,
                                        "Driver",
                                        machinedrivertype,
                                        "",
                                        "driverLocationNDE",
                                        (int)interimrpm,// fix rpm ratio speed
                                        "",
                                        0,
                                        ++machinelocations,
                                        componentpickups.drvrpickup1,
                                        componentinfoorient.Driver_NDEOrientation,
                                        MachineBeingProcessed.id, // jcm 20220317 Guid.Parse(MachineBeingProcessed.id)
                                        MachineBeingProcessed.name,
                                        customerId,
                                        PlantUnits, 
                                        PlantLineFrequency, 
                                        componentpickups, 
                                        levelname,
                                        levelnametop
                                        );
                                }
                                if (component_present_driver == true && midcomp.machineComponentsForMIDgeneration.driver.driverLocationDE == true) // position 2
                                {
                                    EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; location: driverLocationDE; interimrpm: {interimrpm}", null);
                                    short locationno;
                                    if (machinelocations > 0 ) { locationno = componentpickups.drvrpickup2; } else { locationno = componentpickups.drvrpickup1; }
                                    var postlocationresult_driverLocationDE = await CreateLocationToPost.BuildtoPostLocation(
                                        CompLoc,
                                        LocLOR,
                                        "Driver",
                                        machinedrivertype,
                                        "",
                                        "driverLocationDE",
                                        (int)interimrpm,// fix rpm ratio speed
                                        "",
                                        0,
                                        ++machinelocations,
                                        locationno,
                                        componentinfoorient.Driver_DEOrientation, //fix
                                        MachineBeingProcessed.id, // jcm 20220317 Guid.Parse(MachineBeingProcessed.id),
                                        MachineBeingProcessed.name,
                                        customerId,
                                        PlantUnits, 
                                        PlantLineFrequency, 
                                        componentpickups,
                                        levelname,
                                        levelnametop
                                        );
                                };
                                if (component_present_coupling1 == true)
                                {
                                    interimrpm = (int)ProcessSelections.calculateChangedSpeed("coupling1", (int)interimrpm, (int)interimrpm, componentinfodriver.cpl1rationumerator, componentinfodriver.cpl1ratiodenominator, ref speedchangedbyratio);// fix rpm ratio speed
                                    EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; location: coupling1; interimrpm: {interimrpm}", null);
                                };
                                double locnumerator = 1;
                                double locdemoninator = 1;
                                string locspeedchange = "nochange";
                                if (MachineBeingProcessed.gearbox != null && MachineBeingProcessed.gearbox.ratio != null )
                                {
                                    if (MachineBeingProcessed.gearbox.ratio.Length > 0)
                                    {
                                        List<LocationRatios> LocRatioList = ProcessSelections.ParseRatio("{R1 = " + MachineBeingProcessed.gearbox.ratio.Replace(":", "/") + " }");
                                        if (LocRatioList.Count > 0)
                                        {
                                            gbrationumerator = LocRatioList[0].numerator;
                                            gbratiodemoninator = LocRatioList[0].denominator;
                                            gbratiospeedchange = LocRatioList[0].speedchange;
                                            locnumerator = gbrationumerator;
                                            locdemoninator = gbratiodemoninator;
                                            locspeedchange = gbratiospeedchange;
                                        }
                                    }
                                }
                                if (component_present_intermediate == true && componentpickups.intm_loc_01 != 0)
                                {
                                    if (componentinfointermediate.LOC1speedchange != "nochange")
                                    {
                                        locnumerator = componentinfointermediate.LOC1rationumerator;
                                        locdemoninator = componentinfointermediate.LOC1ratiodenominator;
                                        locspeedchange = componentinfointermediate.LOC1speedchange;
                                    }
                                    interimrpm = (int)ProcessSelections.calculateChangedSpeed("LOC1", (int)interimrpm, (int)interimrpm, locnumerator, locdemoninator, ref speedchangedbyratio);// fix rpm ratio speed
                                    EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; location: intm_loc_01; interimrpm: {interimrpm}; locnumerator: {locnumerator}; locdemoninator: {locdemoninator};", null);
                                    var postlocationresult_intermpos1 = await CreateLocationToPost.BuildtoPostLocation(
                                        CompLoc,
                                        LocLOR,
                                        "Intermediate",
                                        machineintermediatetype,
                                        machineintermediatesubtype,
                                        "intermpos1",
                                        (int)interimrpm,// fix rpm ratio speed
                                        componentinfointermediate.LOC1speedchange,// fix rpm ratio speed
                                        componentinfointermediate.ISC_int,
                                        ++machinelocations,
                                        componentpickups.intmpickup1,
                                        componentinfoorient.LOC1Orientation, //fix
                                        MachineBeingProcessed.id, // jcm 20220317 Guid.Parse(MachineBeingProcessed.id),
                                        MachineBeingProcessed.name,
                                        customerId,
                                        PlantUnits,
                                        PlantLineFrequency,
                                        componentpickups,
                                        levelname,
                                        levelnametop
                                        );
                                }
                                if (component_present_intermediate == true && componentpickups.intm_loc_02 != 0)
                                {
                                    if (componentinfointermediate.LOC2speedchange != "nochange")
                                    {
                                        locnumerator = componentinfointermediate.LOC2rationumerator;
                                        locdemoninator = componentinfointermediate.LOC2ratiodenominator;
                                        locspeedchange = componentinfointermediate.LOC2speedchange;
                                    }
                                    interimrpm = (int)ProcessSelections.calculateChangedSpeed("LOC2", (int)interimrpm, (int)interimrpm, locnumerator, locdemoninator, ref speedchangedbyratio);// fix rpm ratio speed
                                    EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; location: intm_loc_02; interimrpm: {interimrpm}; locnumerator: {locnumerator}; locdemoninator: {locdemoninator};", null);
                                    var postlocationresult_intermpos2 = await CreateLocationToPost.BuildtoPostLocation(
                                        CompLoc,
                                        LocLOR,
                                        "Intermediate",
                                        machineintermediatetype,
                                        machineintermediatesubtype,
                                        "intermpos2",
                                        (int)interimrpm,// fix rpm ratio speed
                                        componentinfointermediate.LOC2speedchange,// fix rpm ratio speed
                                        componentinfointermediate.ISC_int,
                                        ++machinelocations,
                                        componentpickups.intmpickup2,
                                        componentinfoorient.LOC2Orientation, //fix 
                                        MachineBeingProcessed.id, // jcm 20220317 Guid.Parse(MachineBeingProcessed.id),
                                        MachineBeingProcessed.name,
                                        customerId,
                                        PlantUnits,
                                        PlantLineFrequency,
                                        componentpickups,
                                        levelname,
                                        levelnametop
                                        );
                                }
                                if (component_present_intermediate == true && componentpickups.intm_loc_03 != 0)
                                {
                                    if (componentinfointermediate.LOC3speedchange != "nochange")
                                    {
                                        locnumerator = componentinfointermediate.LOC3rationumerator;
                                        locdemoninator = componentinfointermediate.LOC3ratiodenominator;
                                        locspeedchange = componentinfointermediate.LOC3speedchange;
                                    }
                                    interimrpm = (int)ProcessSelections.calculateChangedSpeed("LOC3", (int)interimrpm, (int)interimrpm, locnumerator, locdemoninator, ref speedchangedbyratio);// fix rpm ratio speed
                                    EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; location: intm_loc_03; interimrpm: {interimrpm} locnumerator: { locnumerator}; locdemoninator: { locdemoninator};", null);
                                    var postlocationresult_intermpos3 = await CreateLocationToPost.BuildtoPostLocation(
                                        CompLoc,
                                        LocLOR,
                                        "Intermediate",
                                        machineintermediatetype,
                                        machineintermediatesubtype,
                                        "intermpos3",
                                        (int)interimrpm,// fix rpm ratio speed
                                        componentinfointermediate.LOC3speedchange,// fix rpm ratio speed
                                        componentinfointermediate.ISC_int,
                                        ++machinelocations,
                                        componentpickups.intmpickup3,
                                        componentinfoorient.LOC3Orientation, //fix 
                                        MachineBeingProcessed.id, // jcm 20220317 Guid.Parse(MachineBeingProcessed.id),
                                        MachineBeingProcessed.name,
                                        customerId,
                                        PlantUnits,
                                        PlantLineFrequency,
                                        componentpickups,
                                        levelname,
                                        levelnametop
                                        );
                                }
                                if (component_present_intermediate == true && componentpickups.intm_loc_04 != 0)
                                {
                                    if (componentinfointermediate.LOC4speedchange != "nochange")
                                    {
                                        locnumerator = componentinfointermediate.LOC4rationumerator;
                                        locdemoninator = componentinfointermediate.LOC4ratiodenominator;
                                        locspeedchange = componentinfointermediate.LOC4speedchange;
                                    }
                                    interimrpm = (int)ProcessSelections.calculateChangedSpeed("LOC4", (int)interimrpm, (int)interimrpm, locnumerator, locdemoninator, ref speedchangedbyratio);// fix rpm ratio speed
                                    EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; location: intm_loc_04; interimrpm: {interimrpm} locnumerator: { locnumerator}; locdemoninator: { locdemoninator}; ", null);
                                    var postlocationresult_intermpos4 = await CreateLocationToPost.BuildtoPostLocation(
                                        CompLoc,
                                        LocLOR,
                                        "Intermediate",
                                        machineintermediatetype,
                                        machineintermediatesubtype,
                                        "intermpos4",
                                        (int)interimrpm,// fix rpm ratio speed
                                        componentinfointermediate.LOC4speedchange,// fix rpm ratio speed
                                        componentinfointermediate.ISC_int,
                                        ++machinelocations,
                                        componentpickups.intmpickup4,
                                        componentinfoorient.LOC4Orientation, //fix 
                                        MachineBeingProcessed.id, // jcm 20220317 Guid.Parse(MachineBeingProcessed.id),
                                        MachineBeingProcessed.name,
                                        customerId,
                                        PlantUnits,
                                        PlantLineFrequency,
                                        componentpickups,
                                        levelname,
                                        levelnametop
                                        );
                                }
                                if (component_present_driven == true && midcomp.machineComponentsForMIDgeneration.driven.drivenLocationDE == true) // position 3
                                {
                                    if (MachineBeingProcessed.driven.rpmOutput != 0 && speedchangedbyratio == false)
                                    {
                                        interimrpm = (int)ProcessSelections.calculateChangedSpeed("drivenLocationDE", (int)interimrpm, (int)MachineBeingProcessed.driven.rpmOutput, 1, 1, ref speedchangedbyratio);// fix rpm ratio speed
                                    }
                                    EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; location: drivenLocationDE; interimrpm: {interimrpm}", null);
                                    var postlocationresult_drivenLocationDE = await CreateLocationToPost.BuildtoPostLocation(
                                        CompLoc,
                                        LocLOR,
                                        "Driven",
                                        machinedriventype,
                                        machinedrivensubtype,
                                        "drivenLocationDE",
                                        (int)interimrpm,// fix rpm ratio speed
                                        "",
                                        0,
                                        ++machinelocations,
                                        componentpickups.drvnpickup1,
                                        componentinfoorient.Driven_DEOrientation, //fix
                                        MachineBeingProcessed.id, // jcm 20220317 Guid.Parse(MachineBeingProcessed.id),
                                        MachineBeingProcessed.name,
                                        customerId,
                                        PlantUnits,
                                        PlantLineFrequency,
                                        componentpickups,
                                        levelname,
                                        levelnametop
                                        );
                                };
                                if (component_present_driven == true && midcomp.machineComponentsForMIDgeneration.driven.drivenLocationNDE == true) // position 4
                                {
                                    if (MachineBeingProcessed.driven.rpmOutput != 0 && speedchangedbyratio == false)
                                    {
                                        interimrpm = (int)ProcessSelections.calculateChangedSpeed("drivenLocationNDE", (int)interimrpm, (int)MachineBeingProcessed.driven.rpmOutput, 1, 1, ref speedchangedbyratio);// fix rpm ratio speed
                                    }
                                    EARPSInterfaceServiceLogger.AddLogEntry($"AreaUUIid: {AreaBeingProcessed.id}; MachineUUID: {MachineBeingProcessed.id}; location: drivenLocationNDE; interimrpm: {interimrpm}", null);
                                    var postlocationresult_drivenLocationNDE = await CreateLocationToPost.BuildtoPostLocation(
                                        CompLoc,
                                        LocLOR,
                                        "Driven",
                                        machinedriventype,
                                        machinedrivensubtype,
                                        "drivenLocationNDE",
                                        (int)interimrpm,// fix rpm ratio speed
                                        "",
                                        0,
                                        ++machinelocations,
                                        componentpickups.drvnpickup2,
                                        componentinfoorient.Driven_NDEOrientation, //fix
                                        MachineBeingProcessed.id, // jcm 20220317 Guid.Parse(MachineBeingProcessed.id),
                                        MachineBeingProcessed.name,
                                        customerId,
                                        PlantUnits,
                                        PlantLineFrequency,
                                        componentpickups,
                                        levelname,
                                        levelnametop
                                        );
                                };

                                levelname = DebuggingHelper.Changelevelname(levelname, @"\locations", "remove");
                            }
                        }
                        else
                        {
                            EARPSInterfaceServiceLogger.AddLogEntry($"Machine: {MachineBeingProcessed.id}; PROCESSED in AreaName: {AreaBeingProcessed.name}", null);
                            machinesinareaskippedcounter++;
                            EARPSInterfaceServiceLogger.AddLogEntry($"Skipped Machine: {machinesinareaskippedcounter}: {MachineBeingProcessed.name} in AreaName: {AreaBeingProcessed.name}", null);
                        }
                        EARPSInterfaceServiceLogger.AddLogEntry($"Processing Finished MachineName: {MachineBeingProcessed.name} in AreaName: {AreaBeingProcessed.name}; ctr: {machinesinareacounter}", null); // Fix
                        levelname = DebuggingHelper.Changelevelname(levelname, @"\" + MachineBeingProcessed.name, "remove");
                    }  // end of machine
                    levelname = DebuggingHelper.Changelevelname(levelname, @"\" + AreaBeingProcessed.name, "remove");
                } // end of area
            } // end of plant
            catch (Exception ex)
            {
                //EARPSInterfaceServiceLogger.AddLogEntry($"Process a Plant; customerName: {customerName}, plantUUID: {plantUUID}, jobid: {jobid}", ex);
                throw new Exception("Error in Processing Plant",ex);
            }

            //Send Status
            //var sendresponseresults = await ProcessResponse.PostResponse(customerName, tenantID, plantName, plantUUID, jobid, jobStatus);
            //EARPSInterfaceServiceLogger.AddLogEntry($"Processing Finished Pull Request: {customerName} ({tenantID}); plantUUID: {plantUUID}; jobID: {jobid}; result: {sendresponseresults} ", null); // Fix
            return jobStatus;
        }
    }
}


