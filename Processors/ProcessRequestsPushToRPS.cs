﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using SAAIDILibrary.Models;
using EARPSInterfaceService.Models;
using EARPSInterfaceService.Processors;
using System.Linq;
using System.Net.Http;
using EARPSInterfaceService.Logger;
using MIDCodeGeneration.Models.APIResponse;
using MIDCodeGenerationLibrary.Processes;
using MIDCodeGenerationLibrary.Models;
using MIDCodeGeneration.Models.DrivenModels;
using Newtonsoft.Json;
using EARPSInterfaceService.Helpers;
using System.IO;
using System.Net;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using Encoder = System.Drawing.Imaging.Encoder;
using Microsoft.AspNetCore.Mvc;
using MIDCodeGeneration.Models.CodeDeconstructionModels;
//using Customer = EARPSInterfaceService.Models.Customer;

namespace EARPSInterfaceService.Processors
{
    class ProcessRequestPushToRPS
    {
        public static MIDCodeCreatorRequest ComponentsRoot { get; private set; }
        public static int machinesinareacounter = 0;
        public static int machinesinareaskippedcounter = 0;
        public static Boolean skipmachine = false;
        public static short nextMIDNumber = 0;
        public static string sensorTypePlant;
        public static string sensorTypeArea;

        public static async Task<string> PushToRPS(Guid jobid, string customerName, int outgoingcustomer, Guid customerId, string plantName, Guid plantUUID, string jobtype, int incomingCustomerID, string jobStatus, string levelname)
        {
            string levelnametop = levelname;
            var jobstatus = jobStatus;

            MachineComponentsForMIDgeneration midcoderesults = new MachineComponentsForMIDgeneration();
            MIDCodeGenerationLibrary.Helpers.WebAPIClientHelper.BaseUrl = "https://azdevweb.wptss.com"; //jcm fix 
            MIDCodeGenerationLibrary.Helpers.WebAPIClientHelper.UriPrefix = "MIDDerivation";
            MIDCodeGenerationLibrary.Helpers.WebAPIClientHelper.InitializeClient();

            List<MIDCodeGeneration.Models.DetailModels.Driver> middetailresultsdriver = new List<MIDCodeGeneration.Models.DetailModels.Driver>();
            middetailresultsdriver = await ProcessPostMIDCodeGenerationLibrary.GetComponentDetailsDriver("", 0, customerId, "InitialLoad", levelname, levelnametop);
            List<MIDCodeGeneration.Models.DetailModels.Coupling1> middetailresultscoupling1 = new List<MIDCodeGeneration.Models.DetailModels.Coupling1>();
            middetailresultscoupling1 = await ProcessPostMIDCodeGenerationLibrary.GetComponentDetailsCoupling1("", 0, customerId, "InitialLoad", levelname, levelnametop);
            List<MIDCodeGeneration.Models.DetailModels.Intermediate> middetailresultsintermediate = new List<MIDCodeGeneration.Models.DetailModels.Intermediate>();
            middetailresultsintermediate = await ProcessPostMIDCodeGenerationLibrary.GetComponentDetailsIntermediate("", 0, customerId, "InitialLoad", levelname, levelnametop);
            List<MIDCodeGeneration.Models.DetailModels.Coupling2> middetailresultscoupling2 = new List<MIDCodeGeneration.Models.DetailModels.Coupling2>();
            middetailresultscoupling2 = await ProcessPostMIDCodeGenerationLibrary.GetComponentDetailsCoupling2("", 0, customerId, "InitialLoad", levelname, levelnametop);
            List<MIDCodeGeneration.Models.DetailModels.Driven> middetailresultsdriven = new List<MIDCodeGeneration.Models.DetailModels.Driven>();
            middetailresultsdriven = await ProcessPostMIDCodeGenerationLibrary.GetComponentDetailsDriven("", 0, customerId, "InitialLoad", levelname, levelnametop);

            int outgoingplant = 0;
            var GetIDFromGuidResponse = await ProcessGetIDFromGuid.GetAssetIDFromGuid(customerId, plantUUID, 1, levelname);
            outgoingplant = GetIDFromGuidResponse.assetID;

            if (outgoingplant != 0)
            {

                PushPlant pushplant = new PushPlant();
                SAAIDILibrary.Models.Plant eaPlant = await ProcessGetPlantByID.GetPlantByID(outgoingcustomer, outgoingplant, levelname);
                
                pushplant.id = eaPlant.PlantUUID; //jcm 20220317 Guid
                //pushplant.ea_plantID = eaPlant.PlantID;
                pushplant.name = Helper.RemoveSpecialCharacters(eaPlant.PlantName);
                pushplant.title = eaPlant.PlantName;
                //pushplant.lineFrequency = 60; //jcm 20220317 ??
                pushplant.sensorType = null;
                pushplant.midStart = 1; //jcm 20220317

                SAAIDILibrary.Models.Machine newmachine = new SAAIDILibrary.Models.Machine();

                List<SAAIDILibrary.Models.Area> outgoingareas = await ProcessGetAreas.GetAreas(outgoingcustomer, outgoingplant, eaPlant.PlantName, levelname);
                List<EARPSInterfaceService.Models.AreaPush> pushplantareas = new List<EARPSInterfaceService.Models.AreaPush>();
                pushplant.areas = pushplantareas;
                foreach (SAAIDILibrary.Models.Area area in outgoingareas)
                {
                    var outgoingarea = area.AreaID;
                    levelname = DebuggingHelper.Changelevelname(levelname, @"\" + area.AreaName, "add");
                    List<SAAIDILibrary.Models.Machine> outgoingmachines = await ProcessGetMachines.GetMachines(outgoingcustomer, outgoingarea, area.AreaName, levelname);
                    List<EARPSInterfaceService.Models.MachinePush> pushplantareamachines = new List<EARPSInterfaceService.Models.MachinePush>();
                    List<EARPSInterfaceService.Models.LocationsInformationPush> pushplantareamachinelocations = new List<EARPSInterfaceService.Models.LocationsInformationPush>();
                    EARPSInterfaceService.Models.AreaPush pushplantarea = new EARPSInterfaceService.Models.AreaPush();
                    pushplantarea.id = area.AreaUUID;
                    pushplantarea.name = Helper.RemoveSpecialCharacters(area.AreaName);

                    pushplantareas.Add(new Models.AreaPush 
                    { 
                        id = pushplantarea.id, 
                        //ea_areaID = pushplantarea.ea_areaID,
                        name = pushplantarea.name,
                        machines = pushplantareamachines,
                        sensorType = null
                    });

                    EARPSInterfaceServiceLogger.AddLogEntry($">Area: {outgoingarea} {area.AreaName}", null);

                    foreach (SAAIDILibrary.Models.Machine machine in outgoingmachines)
                    {
                        levelname = DebuggingHelper.Changelevelname(levelname, @"\" + machine.MachineName, "add");
                        EARPSInterfaceServiceLogger.AddLogEntry($">>Machine: {machine.MachineID} {machine.MachineName} ", null);
                        SAAIDILibrary.Models.MID mid = await ProcessGetMIDBymidId.GetMID(incomingCustomerID, int.Parse(machine.MIDID.ToString()), machine.MachineID, machine.MachineName, levelname);

                        string midname = mid.MIDName;

                        var middecon = new MIDCodeDeconstructionRequest();
                        middecon.machineComponentsForMIDdeconstruction = new MachineComponentsForMIDdeconstruction();
                        middecon.machineComponentsForMIDdeconstruction.Driver = new MIDCodeGeneration.Models.CommonModels.Codes();
                        middecon.machineComponentsForMIDdeconstruction.Coupling1 = new MIDCodeGeneration.Models.CommonModels.Codes();
                        middecon.machineComponentsForMIDdeconstruction.Intermediate = new MIDCodeGeneration.Models.CommonModels.Codes();
                        middecon.machineComponentsForMIDdeconstruction.Coupling2 = new MIDCodeGeneration.Models.CommonModels.Codes();
                        middecon.machineComponentsForMIDdeconstruction.Driven = new MIDCodeGeneration.Models.CommonModels.Codes();
                        middecon.machineComponentsForMIDdeconstruction.FaultCodeMatrix = new MIDCodeGeneration.Models.CommonModels.FaultCodeMatrix();

                        middecon.machineComponentsForMIDdeconstruction.Driver.ComponentCode = "46.12";
                        middecon.machineComponentsForMIDdeconstruction.Driver.PickupCode = "1,2,7";
                        middecon.machineComponentsForMIDdeconstruction.Coupling1.ComponentCode = "4.11";
                        middecon.machineComponentsForMIDdeconstruction.Coupling1.PickupCode = "2,7";
                        middecon.machineComponentsForMIDdeconstruction.Coupling1.SpeedRatio = (decimal)0.2760;
                        midcoderesults = await ProcessPostMIDCodeGenerationLibrary.PostMIDCodeDeconstruction(middecon, customerId, machine.MachineName, levelname, levelnametop);


                        List<MIDCodeGeneration.Models.DetailModels.Driver> middetailresultsdriverchk = new List<MIDCodeGeneration.Models.DetailModels.Driver>();
                        middetailresultsdriverchk = null;
                        List<MIDCodeGeneration.Models.DetailModels.Coupling1> middetailresultscoupling1chk = new List<MIDCodeGeneration.Models.DetailModels.Coupling1>();
                        middetailresultscoupling1chk = null;
                        List<MIDCodeGeneration.Models.DetailModels.Intermediate> middetailresultsintermediatechk = new List<MIDCodeGeneration.Models.DetailModels.Intermediate>();
                        middetailresultsintermediatechk = null;
                        List<MIDCodeGeneration.Models.DetailModels.Coupling2> middetailresultscoupling2chk = new List<MIDCodeGeneration.Models.DetailModels.Coupling2>();
                        middetailresultscoupling2chk = null;
                        List<MIDCodeGeneration.Models.DetailModels.Driven> middetailresultsdrivenchk = new List<MIDCodeGeneration.Models.DetailModels.Driven>();
                        middetailresultsdrivenchk = null;

                        List<MachineComponents> machinecompslist = new List<MachineComponents>();
                        MachineComponents machinecomps = new MachineComponents();
                        
                        int pickupscount = 0;
                        EARPSInterfaceServiceLogger.AddLogEntry($">>>MID: {mid.MIDID} {mid.MIDName}", null);
                        for (var i = 0; i < mid.ComponentsList.Count; i++)
                        {
                            EARPSInterfaceServiceLogger.AddLogEntry($">>>Components: {i}; order: {mid.ComponentsList[i].ComponentOrder} - cc: {mid.ComponentsList[i].ComponentCode}; pu1: {mid.ComponentsList[i].PickupOne}; pu2: {mid.ComponentsList[i].PickupTwo}; pu3: {mid.ComponentsList[i].PickupThree}; pu4: {mid.ComponentsList[i].PickupFour}", null);
                            machinecomps.ComponentOrder = mid.ComponentsList[i].ComponentOrder;
                            machinecomps.ComponentCode = mid.ComponentsList[i].ComponentCode;
                            machinecomps.PickupOne = mid.ComponentsList[i].PickupOne;
                            machinecomps.PickupTwo = mid.ComponentsList[i].PickupTwo;
                            machinecomps.PickupThree = mid.ComponentsList[i].PickupThree;
                            machinecomps.PickupFour = mid.ComponentsList[i].PickupFour;
                            if (mid.ComponentsList[i].PickupOne != 0) { machinecomps.Pickupscount++; }
                            if (mid.ComponentsList[i].PickupTwo != 0) { machinecomps.Pickupscount++; }
                            if (mid.ComponentsList[i].PickupThree != 0) { machinecomps.Pickupscount++; }
                            if (mid.ComponentsList[i].PickupFour != 0) { machinecomps.Pickupscount++; }

                            pickupscount = pickupscount + 4;

                            var ccfound = false;
                            if (ccfound == false && middetailresultsdriverchk == null)
                            { 
                                middetailresultsdriverchk = middetailresultsdriver.Where(x => x.componentCode == mid.ComponentsList[i].ComponentCode).ToList();

                                if (middetailresultsdriverchk.Any())
                                {
                                    machinecomps.ComponentType = Helper.UppercaseFirst(middetailresultsdriverchk[0].componentType);
                                    machinecomps.ComponentSubType = Helper.UppercaseFirst(middetailresultsdriverchk[0].driverType);
                                    if (machinecomps.PickupOne != 0) { machinecomps.PickupOneString = "NDE"; };
                                    if (machinecomps.PickupTwo != 0) { machinecomps.PickupTwoString = "DE"; };
                                    ccfound = true;
                                }
                                else
                                {
                                    middetailresultsdriverchk = null;
                                }
                            }
                            if (ccfound == false && middetailresultscoupling1chk == null)
                            {
                                middetailresultscoupling1chk = middetailresultscoupling1.Where(x => x.componentCode == mid.ComponentsList[i].ComponentCode).ToList();

                                if (middetailresultscoupling1chk.Any())
                                {
                                    machinecomps.ComponentType = Helper.UppercaseFirst(middetailresultscoupling1chk[0].componentType);
                                    machinecomps.ComponentSubType = Helper.UppercaseFirst(middetailresultscoupling1chk[0].couplingType);
                                    machinecomps.CouplingPosition = (int)middetailresultscoupling1chk[0].couplingPosition;
                                    machinecomps.CoupledComponentType1 = middetailresultscoupling1chk[0].coupledComponentType1;
                                    machinecomps.CoupledComponentType2 = middetailresultscoupling1chk[0].coupledComponentType2;
                                    machinecomps.PickupOneString = "";
                                    machinecomps.PickupTwoString = "";
                                    machinecomps.PickupThreeString = "";
                                    machinecomps.PickupFourString = "";
                                    ccfound = true;
                                }
                                else
                                {
                                    middetailresultscoupling1chk = null;
                                }
                            }
                            if (ccfound == false && middetailresultsintermediatechk == null)
                            {
                                middetailresultsintermediatechk = middetailresultsintermediate.Where(x => x.componentCode == mid.ComponentsList[i].ComponentCode).ToList();

                                if (middetailresultsintermediatechk.Any())
                                {
                                    machinecomps.ComponentType = Helper.UppercaseFirst(middetailresultsintermediatechk[0].componentType);
                                    machinecomps.ComponentSubType = Helper.UppercaseFirst(middetailresultsintermediatechk[0].drivenBy);
                                    if (machinecomps.PickupOne != 0) { machinecomps.PickupOneString = $"Location {machinecomps.PickupOne}"; };
                                    if (machinecomps.PickupTwo != 0) { machinecomps.PickupTwoString = $"Location {machinecomps.PickupTwo}"; };
                                    if (machinecomps.PickupThree != 0) { machinecomps.PickupThreeString = $"Location {machinecomps.PickupThree}"; };
                                    if (machinecomps.PickupFour != 0) { machinecomps.PickupFourString = $"Location {machinecomps.PickupFour}"; };
                                    ccfound = true;
                                }
                                else
                                {
                                    middetailresultsintermediatechk = null;
                                }
                            }
                            if (ccfound == false && middetailresultscoupling2chk == null)
                            {
                                middetailresultscoupling2chk = middetailresultscoupling2.Where(x => x.componentCode == mid.ComponentsList[i].ComponentCode).ToList();

                                if (middetailresultscoupling2chk.Any())
                                {
                                    machinecomps.ComponentType = Helper.UppercaseFirst(middetailresultscoupling2chk[0].componentType);
                                    machinecomps.ComponentSubType = Helper.UppercaseFirst(middetailresultscoupling2chk[0].couplingType);
                                    machinecomps.CouplingPosition = (int)middetailresultscoupling2chk[0].couplingPosition;
                                    machinecomps.CoupledComponentType1 = middetailresultscoupling2chk[0].coupledComponentType1;
                                    machinecomps.CoupledComponentType2 = middetailresultscoupling2chk[0].coupledComponentType2;
                                    machinecomps.PickupOneString = "";
                                    machinecomps.PickupTwoString = "";
                                    machinecomps.PickupThreeString = "";
                                    machinecomps.PickupFourString = "";
                                    ccfound = true;
                                }
                                else
                                {
                                    middetailresultscoupling2chk = null;
                                }
                            }
                            if (ccfound == false && middetailresultsdrivenchk == null)
                            {
                                middetailresultsdrivenchk = middetailresultsdriven.Where(x => x.componentCode == mid.ComponentsList[i].ComponentCode).ToList();

                                if (middetailresultsdrivenchk.Any())
                                {
                                    machinecomps.ComponentType = Helper.UppercaseFirst(middetailresultsdrivenchk[0].componentType);
                                    machinecomps.ComponentSubType = Helper.UppercaseFirst(middetailresultsdrivenchk[0].drivenType);
                                    if (machinecomps.PickupOne != 0) { machinecomps.PickupOneString = "DE"; };
                                    if (machinecomps.PickupTwo != 0) { machinecomps.PickupTwoString = "NDE"; };
                                    ccfound = true;
                                }
                                else
                                {
                                    middetailresultsdrivenchk = null;
                                }
                            }
                            if (ccfound == false)
                            {
                                EARPSInterfaceServiceLogger.AddLogEntry($">>>ComponentCode not found: {mid.ComponentsList[i].ComponentCode}", null);
                            }
                            else
                            {
                                machinecompslist.Add(new MachineComponents { 
                                    ComponentOrder = machinecomps.ComponentOrder,
                                    ComponentCode = machinecomps.ComponentCode,
                                    PickupOne = machinecomps.PickupOne,
                                    PickupOneString = machinecomps.PickupOneString,
                                    PickupTwo = machinecomps.PickupTwo,
                                    PickupTwoString = machinecomps.PickupTwoString,    
                                    PickupThree = machinecomps.PickupThree,
                                    PickupThreeString = machinecomps.PickupThreeString,
                                    PickupFour = machinecomps.PickupFour,
                                    PickupFourString = machinecomps.PickupFourString,
                                    Pickupscount = machinecomps.Pickupscount,
                                    ComponentType = machinecomps.ComponentType,
                                    ComponentSubType = machinecomps.ComponentSubType,
                                    CouplingPosition = machinecomps.CouplingPosition,
                                    CoupledComponentType1 = machinecomps.CoupledComponentType1,
                                    CoupledComponentType2 = machinecomps.CoupledComponentType2
                                });

                            }
                        }
                        foreach (MachineComponents machinecmp in machinecompslist)
                        {
                            EARPSInterfaceServiceLogger.AddLogEntry($">>>MachineComponents>> ord: {machinecmp.ComponentOrder}; cc: {machinecmp.ComponentCode}; pu1: {machinecmp.PickupOne}; pus1: {machinecmp.PickupOneString}; pu2: {machinecmp.PickupTwo}; pus2: {machinecmp.PickupTwoString}; pu3: {machinecmp.PickupThree}; pus3: {machinecmp.PickupThreeString}; pu4: {machinecmp.PickupFour} pus4: {machinecmp.PickupFourString}; pucnt: {machinecmp.Pickupscount}; ctype: {machinecmp.ComponentType}; cstype: {machinecmp.ComponentSubType}; cp: {machinecmp.CouplingPosition}; cct1: {machinecmp.CoupledComponentType1}; cct2: {machinecmp.CoupledComponentType2}", null);
                            DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(machinecmp, new JsonSerializerOptions { WriteIndented = true }), levelname, $"machinecomponent_{(decimal)machinecmp.ComponentCode}");
                        }

                        if (middetailresultsdriverchk != null)
                        {
                            DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(middetailresultsdriverchk[0], new JsonSerializerOptions { WriteIndented = true }), levelname, $"cc_{(decimal)middetailresultsdriverchk[0].componentCode}");
                            EARPSInterfaceServiceLogger.AddLogEntry($"---driver: {(decimal)middetailresultsdriverchk[0].componentCode}; comptype: {middetailresultsdriverchk[0].componentType}; drvrtype: {middetailresultsdriverchk[0].driverType}", null);
                        }
                        if (middetailresultscoupling1chk != null)
                        {
                            DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(middetailresultscoupling1chk[0], new JsonSerializerOptions { WriteIndented = true }), levelname, $"cc_{(decimal)middetailresultscoupling1chk[0].componentCode}");
                            EARPSInterfaceServiceLogger.AddLogEntry($"---coupling1: {(decimal)middetailresultscoupling1chk[0].componentCode}; comptype: {middetailresultscoupling1chk[0].componentType}; pos: {middetailresultscoupling1chk[0].couplingPosition}; comp1: {middetailresultscoupling1chk[0].coupledComponentType1}; comp2: {middetailresultscoupling1chk[0].coupledComponentType2}", null);
                        }
                        if (middetailresultsintermediatechk != null)
                        {
                            DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(middetailresultsintermediatechk[0], new JsonSerializerOptions { WriteIndented = true }), levelname, $"cc_{(decimal)middetailresultsintermediatechk[0].componentCode}");
                            EARPSInterfaceServiceLogger.AddLogEntry($"---intermediate: {(decimal)middetailresultsintermediatechk[0].componentCode}; comptype: {middetailresultsintermediatechk[0].componentType}", null);
                        }
                        if (middetailresultscoupling2chk != null)
                        {
                            DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(middetailresultscoupling2chk[0], new JsonSerializerOptions { WriteIndented = true }), levelname, $"cc_{(decimal)middetailresultscoupling2chk[0].componentCode}");
                            EARPSInterfaceServiceLogger.AddLogEntry($"----coupling2: {(decimal)middetailresultscoupling2chk[0].componentCode}; comptype: {middetailresultscoupling2chk[0].componentType}; pos: {middetailresultscoupling2chk[0].couplingPosition}; comp1: {middetailresultscoupling2chk[0].coupledComponentType1}; comp2: {middetailresultscoupling2chk[0].coupledComponentType2}", null);
                        }
                        if (middetailresultsdrivenchk != null)
                        {
                            DebuggingHelper.WriteJsonToFile(System.Text.Json.JsonSerializer.Serialize(middetailresultsdrivenchk[0], new JsonSerializerOptions { WriteIndented = true }), levelname, $"cc_{(decimal)middetailresultsdrivenchk[0].componentCode}");
                            EARPSInterfaceServiceLogger.AddLogEntry($"---driven: {(decimal)middetailresultsdrivenchk[0].componentCode}; comptype: {middetailresultsdrivenchk[0].componentType}", null);
                        }
                        string sensortypeconverted = "portable";
                        switch (machine.DataCollectionSourceTypeID) { case 1: sensortypeconverted = "portable"; break; case 2: sensortypeconverted = "online"; break; case 3: sensortypeconverted = "wireless"; break; }
                        if (sensorTypePlant != "wireless" && sensortypeconverted == "wireless")
                        {
                            sensorTypePlant = "wireless";
                        }
                        else
                        {
                            sensorTypePlant = sensortypeconverted;
                        }
                        if (sensorTypeArea != "wireless" && sensortypeconverted == "wireless")
                        {
                            sensorTypeArea = "wireless";
                        }
                        else
                        {
                            sensorTypeArea = sensortypeconverted;
                        }
                        List<EARPSInterfaceService.Models.MachineComponentMapPush> pushplantmachinecomponentmap = new List<EARPSInterfaceService.Models.MachineComponentMapPush>();
                        var outgoingmachine = machine.MachineID;
                        EARPSInterfaceService.Models.MachinePush pushplantareamachine2 = new EARPSInterfaceService.Models.MachinePush();
                        //pushplantareamachine2.ea_machineID = machine.MachineID;
                        pushplantareamachine2.id = machine.MachineUUID;
                        pushplantareamachine2.guard = false;
                        pushplantareamachine2.elevated = false;
                        pushplantareamachine2.ppe = false;
                        pushplantareamachine2.ooc = false;
                        pushplantareamachine2.cmmsId = machine.CMMSLink;
                        pushplantareamachine2.sensorType = sensortypeconverted; //jcm 20220317 fill in
                        pushplantareamachine2.manualMIDBuild = true;
                        pushplantareamachine2.name = Helper.RemoveSpecialCharacters(machine.MachineName);
                        pushplantareamachine2.conflicted = false;
                        switch (mid.Orientation) { case "H": pushplantareamachine2.machineOrientation = "Horizontal"; break; case "V": pushplantareamachine2.machineOrientation = "Vertical"; break; default: pushplantareamachine2.machineOrientation = null; break; };

                        pushplantareamachines.Add(new Models.MachinePush 
                        {
                            //ea_machineID = pushplantareamachine2.ea_machineID,
                            id = pushplantareamachine2.id,
                            guard = pushplantareamachine2.guard,
                            elevated = pushplantareamachine2.elevated,
                            ppe = pushplantareamachine2.ppe,
                            ooc = pushplantareamachine2.ooc,
                            cmmsId = pushplantareamachine2.cmmsId,
                            assetId = pushplantareamachine2.assetId,    
                            machineOrientation = pushplantareamachine2.machineOrientation, //jcm 20220317 fill in

                            driver = null, //jcm 20220317 fill in
                            gearbox = null, //jcm 20220317 fill in
                            driven = null, //jcm 20220317 fill in
                            foundation = null,
                            environment = null,

                            comment = pushplantareamachine2.comment,
                            sensorType = pushplantareamachine2.sensorType, //jcm 20220317 fill in
                            manualMIDBuild = pushplantareamachine2.manualMIDBuild,
                            name = pushplantareamachine2.name,

                            locationsInformation = pushplantareamachinelocations,
                        });

                        pushplantmachinecomponentmap.Add(new Models.MachineComponentMapPush
                        {
                            id = null,
                            serialNumber = 0,
                            version = 0,
                            //machine = pushplantareamachine2,
                            importCSV = false,
                            key = "Driver",
                            value = ""
                        });
                        pushplantmachinecomponentmap.Add(new Models.MachineComponentMapPush
                        {
                            id = null,
                            serialNumber = 0,
                            version = 0,
                            //machine = pushplantareamachine2,
                            importCSV = false,
                            key = "GearBox",
                            value = ""
                        });
                        pushplantmachinecomponentmap.Add(new Models.MachineComponentMapPush
                        {
                            id = null,
                            serialNumber = 0,
                            version = 0,
                            //machine = pushplantareamachine2,
                            importCSV = false,
                            key = "Driven",
                            value = ""
                        });

                        int componentcount = 0;
                        int currentcomponent = 0;
                        pushplantareamachinelocations.Clear();
                        List<SAAIDILibrary.Models.Location> outgoingmachinelocations = await ProcessGetLocations.GetLocations(outgoingcustomer, outgoingmachine, machine.MachineName, levelname);
                        outgoingmachinelocations = outgoingmachinelocations.OrderBy(x => x.PositionNumber).ToList();
                        foreach (SAAIDILibrary.Models.Location location in outgoingmachinelocations)
                        {
                            levelname = DebuggingHelper.Changelevelname(levelname, @"\" + location.LocationName, "add");
                            var outgoinglocation = location.LocationID;

                            string currentlocationposition = null;
                            string currentcomponenttype = null;
                            string currentcomponentsubtype = null;
                            componentcount++;
                            for (int i = currentcomponent; i < machinecompslist.Count; i++)
                            {
                                if (
                                    (machinecompslist[i].PickupOne == location.PositionNumber) ||
                                    (machinecompslist[i].PickupTwo == location.PositionNumber) ||
                                    (machinecompslist[i].PickupThree == location.PositionNumber) ||
                                    (machinecompslist[i].PickupFour == location.PositionNumber)
                                    )
                                {
                                    currentcomponent = i;
                                    break;
                                }
                            }
                            if (machinecompslist[currentcomponent].PickupOne == location.PositionNumber)
                            {
                                currentlocationposition = machinecompslist[currentcomponent].PickupOneString;
                            };
                            if (machinecompslist[currentcomponent].PickupTwo == location.PositionNumber)
                            {
                                currentlocationposition = machinecompslist[currentcomponent].PickupTwoString;
                            };
                            if (machinecompslist[currentcomponent].PickupThree == location.PositionNumber)
                            {
                                currentlocationposition = machinecompslist[currentcomponent].PickupThreeString;
                            };
                            if (machinecompslist[currentcomponent].PickupFour == location.PositionNumber)
                            {
                                currentlocationposition = machinecompslist[currentcomponent].PickupFourString;
                            };

                            currentcomponenttype = machinecompslist[currentcomponent].ComponentType;
                            currentcomponentsubtype = machinecompslist[currentcomponent].ComponentSubType;

                            EARPSInterfaceServiceLogger.AddLogEntry($">>>>Location: {location.LocationID} {location.LocationName} Orientation: {location.Orientation} POS#: {location.PositionNumber}", null);
                            pushplantareamachinelocations.Add(new Models.LocationsInformationPush 
                            {
                                id = location.LocationUUID,
                                locationType = currentcomponentsubtype, 
                                componentType = currentcomponenttype, 
                                position = currentlocationposition, 
                                brgNo = $"brg {location.PositionNumber.ToString()}", 
                                orientation = location.Orientation,
                                scanned = false,
                                sensorSerialNumber = null,
                            });

                            levelname = DebuggingHelper.Changelevelname(levelname, @"\locations", "remove");
                        }
                        levelname = DebuggingHelper.Changelevelname(levelname, @"\" + machine.MachineName, "remove");
                    }
                    pushplantarea.sensorType = sensorTypePlant;
                    levelname = DebuggingHelper.Changelevelname(levelname, @"\" + area.AreaName, "remove");
                }

                pushplant.sensorType = sensorTypePlant;
                EARPSInterfaceService.Models.PushPlantResponse pushresponse = await ProcessAPIPushPlant.PostPushPlant(pushplant, customerName, customerId, plantUUID, jobid, levelname);
            }

            EARPSInterfaceServiceLogger.AddLogEntry($"Processing Finished Pushing Plant: {plantName} ", null);
            return jobStatus;
        }
    }
}


