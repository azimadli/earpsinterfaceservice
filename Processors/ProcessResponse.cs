﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using EARPSInterfaceService.Models;
using System.Web;
using EARPSInterfaceService.Helpers;
using Alba;
using System.Text.Json;
using System.Net.Http.Headers;
using SAAIDILibrary.Models;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService.Processors
{
    class ProcessResponse
    {
        public static async Task<PullPlantResponse> PostResponse(string CustomerName, Guid customerid, string plantName, Guid plantUUID, Guid jobID, string jobStatus)
        {
            var url = $"{ServiceSettings.apiRPSuriBase}{ServiceSettings.apiRPSuriBase2}pull/customer/plant/{ customerid }";
            var jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };
            EARPSInterfaceService.Models.JobStatus jobstatusresponse = new EARPSInterfaceService.Models.JobStatus();
            jobstatusresponse.exportStatus = jobStatus;
            jobstatusresponse.plantID = plantUUID;
            jobstatusresponse.jobId = jobID;

            DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(jobstatusresponse, new JsonSerializerOptions { WriteIndented = true }), "pullrequestresponse", plantName);

            using (HttpResponseMessage response = await APIRPSHelper.ApiRPSClient.PostAsJsonAsync(url, jobstatusresponse))
            {
               if (response.IsSuccessStatusCode)
                {
                    PullPlantResponse responsepullplant = await response.Content.ReadAsAsync<EARPSInterfaceService.Models.PullPlantResponse>();
                    DebuggingHelper.WriteJsonToFile(JsonSerializer.Serialize(responsepullplant.message), "pullresponse", ("postPushPlantResponse" + plantName));

                    EARPSInterfaceServiceLogger.AddLogEntry($"tenantID: {customerid} Plant pushed: {plantName}; status: {responsepullplant.status}; {responsepullplant.message};", null);
                    return responsepullplant;

                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                };
            }
        }
    }
 }
