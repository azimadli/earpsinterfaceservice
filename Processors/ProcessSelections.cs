﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EARPSInterfaceService.Logger;
using EARPSInterfaceService.Models;

namespace EARPSInterfaceService.Processors
{
    public class ProcessSelections
    {
        public static ComponentLocations SelectComponentLocation(List<ComponentLocations> comploc, string comptype, string comptypesub, string comptypesub2, string compspeedchange, int compspeedtimes, int location)
        {
            EARPSInterfaceServiceLogger.AddLogEntry($"comptype: {comptype}; compsubtype: {comptypesub}; compsubtype2: {comptypesub2}; compspeedchange: {compspeedchange}; compspeedtimes: {compspeedtimes}; location: {location}", null);
            var compspeedchange_internal = compspeedchange;
            if (compspeedchange_internal == "nochange")
            {
                compspeedchange_internal = "decrease";
            }
            ComponentLocations currentcomponent = new ComponentLocations();
            try
            {
                currentcomponent = comploc.Where(x =>
                x.componentType.ToLower() == comptype.ToLower() &&
                x.componentTypeSub.ToLower() == comptypesub.ToLower() &&
                x.componentTypeSub2.ToLower() == comptypesub2.ToLower() &&
                x.speedchangedirection.ToLower() == compspeedchange_internal.ToLower() &&
                x.speedchangenumberoftimes == compspeedtimes &&
                x.location == location).ToList().First();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //throw new Exception("Error in SelectComponentLocation", ex);
            }
            return currentcomponent;
        }
        public static LocationLOR SelectLocationLOR(List<LocationLOR> LocLOR, string loclorkey, double speed)
        {
            LocationLOR currentlocLOR = new LocationLOR();
            try
            {
                currentlocLOR = LocLOR.Where(x => x.lorKey.ToUpper() == loclorkey.ToUpper()).ToList().First();

                currentlocLOR.lowrangeFMAX = (speed * 2.5 * currentlocLOR.lowrangefactorFMAX) / 60;
                if (loclorkey == "G")
                {
                    currentlocLOR.lowrangefactorLOR = Math.Round(((currentlocLOR.lowrangefactorFMAX / 302) * 1.5), 0) * 1600;
                    if (currentlocLOR.lowrangefactorLOR == 0) { currentlocLOR.lowrangefactorLOR = 1; }
                }
                currentlocLOR.lowrangeLOR = FindNextLOR(Convert.ToInt32((currentlocLOR.lowrangeFMAX * 100) / currentlocLOR.lowrangefactorLOR));
                currentlocLOR.highrangeFMAX = (speed * 2.5 * currentlocLOR.highrangefactorFMAX) / 60;
                currentlocLOR.highrangeLOR = FindNextLOR(Convert.ToInt32((currentlocLOR.highrangeFMAX * 100) / currentlocLOR.highrangefactorLOR));
                currentlocLOR.demodlowrangeFMAX = (speed * 2.5 * currentlocLOR.demodlowrangefactorFMAX) / 60;
                currentlocLOR.demodlowrangeLOR = FindNextLOR(Convert.ToInt32((currentlocLOR.demodlowrangeFMAX * 100) / currentlocLOR.demodlowrangefactorLOR));

            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex);
                throw new Exception("Error in SelectLocationLOR", ex);
            }
            return currentlocLOR;
        }
        private static int FindNextLOR(int currentLOR)
        {
            int baseval = 200;
            int finalLOR = 0;
            //EARPSInterfaceServiceLogger.AddLogEntry($"currentLOR: {currentLOR}", null);
            int maxpower = 7;
            for (var i = 0; i < maxpower; i++)
            {
                finalLOR = (int)(baseval * (Math.Pow(2, i)));
                //EARPSInterfaceServiceLogger.AddLogEntry($" current: {currentLOR}; i: {i}; multiplierLOR: {Math.Pow(2, i)}; finalLOR: {finalLOR}", null);
                if ((currentLOR <= finalLOR) == true) { i = maxpower; } else { finalLOR = currentLOR; };
            }
            EARPSInterfaceServiceLogger.AddLogEntry($"finalLOR: {finalLOR}", null);
            return finalLOR;
        }
        public static List<LocationRatios> ParseRatio(string ratios)
        {
            List<LocationRatios> LocRatioList = new List<LocationRatios>();
            int i = 0;
            while (i < (ratios.Length - 1))
            {
                string ratiovalue;

                int rationumber;
                int ratiobegin;
                int ratioend;

                double rationumerator;
                int rationumeratorbegin;
                int rationumeratorend;

                double ratiodenominator;
                int ratiodenominatorbegin;
                int ratiodenominatorend;

                string changeinspeed;

                ratiobegin = ratios.IndexOf("{", i);
                ratioend = ratios.IndexOf("}", i + 1);
                ratiovalue = ratios.Substring(ratiobegin, (ratioend - ratiobegin + 1));
                rationumber = int.Parse(ratiovalue.Substring(2, 1));

                rationumeratorbegin = ratiovalue.IndexOf("= ", 0) + 2;
                rationumeratorend = ratiovalue.IndexOf(" ", (rationumeratorbegin));
                rationumerator = double.Parse(ratiovalue.Substring(rationumeratorbegin, ((rationumeratorend)) - rationumeratorbegin));

                ratiodenominatorbegin = ratiovalue.IndexOf(" / ", 0) + 3;
                ratiodenominatorend = ratiovalue.IndexOf("}");
                ratiodenominator = double.Parse(ratiovalue.Substring(ratiodenominatorbegin, ((ratiodenominatorend) - (ratiodenominatorbegin))));

                if (ratiodenominator > rationumerator) //Per DanW: "For speed changers: 1: 1.2 would be a speed DECREASER" 9/10/21
                //if (ratiodenominator < rationumerator) //Per DanW: "nitor has it reversed. we are currently looking at the difference in 'diameter'. " 9/10/21 - later
                { 
                    changeinspeed = "increase";                 
                }
                else
                {
                    if (ratiodenominator < rationumerator) 
                    { 
                        changeinspeed = "decrease"; 
                    }
                    else
                    {
                        changeinspeed = "nochange";
                    }
                }

                LocRatioList.Add(new LocationRatios
                {
                    location = rationumber,
                    numerator = rationumerator,
                    denominator = ratiodenominator,
                    speedchange = changeinspeed
                });

                //EARPSInterfaceServiceLogger.AddLogEntry($"Ratio>>> Instance: {rationumber}; {rationumerator} / {ratiodenominator} = {rationumerator / ratiodenominator}", null);
                EARPSInterfaceServiceLogger.AddLogEntry($"Ratio>>> Instance: {rationumber}; {rationumerator} / {ratiodenominator} = {rationumerator / ratiodenominator}", null);
                i = ratioend++;
            }
            return LocRatioList;

        }
        public static int calculateChangedSpeed(
            string location, 
            int inoldRPM, 
            int innewRPM, 
            double ratioNumerator, 
            double ratioDenominator, 
            ref bool speedchangedbyratio
            )
        {
            int result;
            double chgpct = 0;
            if (ratioNumerator != ratioDenominator)
            {
                // Ratio present and may change the speed
                chgpct = Math.Abs(((inoldRPM - (inoldRPM * (ratioNumerator / ratioDenominator))) / inoldRPM) * 100);
                if (chgpct > 5)
                {
                    // Ratio represents a more than 5% change
                    speedchangedbyratio = true;
                    result = Convert.ToInt32(Math.Round((inoldRPM * (ratioNumerator / ratioDenominator))));
                }
                else
                {
                    // No change in speed so return the incoming RPM
                    result = inoldRPM;
                }
            }
            else
            {
                // Ratio NOT present so check RPMs for change
                if (inoldRPM != innewRPM)
                {
                    // RPMs indicate change
                    chgpct = Math.Abs((((double)innewRPM - (double)inoldRPM) / (double)inoldRPM)  * 100);
                }
                if (chgpct > 5)
                {
                    // Change in RPMs represents a more than 5% change
                    speedchangedbyratio = true;
                    result = innewRPM;
                }
                else
                {
                    result = inoldRPM;
                }
            }

            EARPSInterfaceServiceLogger.AddLogEntry("Check for Change in Speed: " +
                $"location: {location}; " +
                $"inRPM: {inoldRPM}; " +
                $"inRPM: {innewRPM}; " + 
                $"ratioNumerator: {ratioNumerator}; " +
                $"ratioDenominator: {ratioDenominator}; " +
                $"speedchangedbyratio: {speedchangedbyratio}; " +
                $"chgpct: {chgpct}%; " +
                $"outRPM: {result};"
                , null);
            return result;
        }
    }
}
