using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration.CommandLine;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.EventLog;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using EARPSInterfaceService.Helpers;
using NLog;
using System.IO;
using System.Text.Json;
using NLog.Extensions.Logging;
using Microsoft.Azure.ServiceBus;
//using Azure.Messaging.ServiceBus;

namespace EARPSInterfaceService
{
    // Versions:
    // Version #  || Version Author || Version Date || Version Description
    // v1.0.0.0   || JMcIntosh      || 01/04/2021   || Original 
    // v1.0.0.1   || JMcIntosh      || 03/09/2021   || Logging to file via NLog 
    // v1.0.0.6   || JMcIntosh      || 03/09/2021   || Added use of AzureServiceBus queue 
    // v1.0.0.6   || JMcIntosh      || 03/09/2021   || Add Plants, Areas, Machines 
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            GlobalDiagnosticsContext.Set("Application", "EARPSInterfaceService");
            GlobalDiagnosticsContext.Set("Version", Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion);
            GlobalDiagnosticsContext.Set("ApplicationVersion", GlobalDiagnosticsContext.Get("Application") + "v" + GlobalDiagnosticsContext.Get("Version"));

            return Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
//                    config.AddCommandLine(args, switchMappings);
                })
                .ConfigureLogging(logging => 
                {
                    logging.ClearProviders();
                    logging.AddConsole();
                    logging.AddNLog();
                    logging.AddEventLog(new EventLogSettings()
                    {
                        LogName = "Application",
                        SourceName = "EARPSInterfaceService"
                    });
                    logging.SetMinimumLevel(LogLevel.Debug);
                })

                .ConfigureServices((hostContext, services) =>
                {
                    IConfiguration configuration = hostContext.Configuration;
                    ServiceSettings.apiEAuriBase = configuration.GetSection("EARPSInterfaceServiceServiceOptions").Get<WorkerOptions>().apiEAuriBase;
                    ServiceSettings.apiEAuriBase2 = configuration.GetSection("EARPSInterfaceServiceServiceOptions").Get<WorkerOptions>().apiEAuriBase2;
                    ServiceSettings.apiRPSuriBase = configuration.GetSection("EARPSInterfaceServiceServiceOptions").Get<WorkerOptions>().apiRPSuriBase;
                    ServiceSettings.apiRPSuriBase2 = configuration.GetSection("EARPSInterfaceServiceServiceOptions").Get<WorkerOptions>().apiRPSuriBase2;
                    ServiceSettings.delayingMaxMinutes = configuration.GetSection("EARPSInterfaceServiceServiceOptions").Get<WorkerOptions>().delayingMaxMinutes;
                    ServiceSettings.azureBlogContainer = configuration.GetSection("EARPSInterfaceServiceServiceOptions").Get<WorkerOptions>().azureBlogContainer;
                    ServiceSettings.azureBlogAccountKey = configuration.GetSection("EARPSInterfaceServiceServiceOptions").Get<WorkerOptions>().azureBlogAccountKey;
                    services.AddSingleton<ILoggerManager, LoggerManager>();
                    services.AddHostedService<Worker>();
                })
                ;
        }

    }
}
