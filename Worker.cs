using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EARPSInterfaceService.Helpers;
using EARPSInterfaceService.Processors;
using SAAIDI.Models;
using EARPSInterfaceService.Models;
using NLog;
using NLog.Fluent;
using Microsoft.Azure.ServiceBus;
//using Azure.Messaging.ServiceBus;
using MIDCodeGenerationLibrary;
using MIDCodeGeneration.Models.APIResponse;
using MIDCodeGenerationLibrary.Processes;
using MIDCodeGenerationLibrary.Models;
using Newtonsoft.Json;
using EARPSInterfaceService.Logger;

namespace EARPSInterfaceService
{
    public class Worker : BackgroundService
    {
        public readonly ILogger<Worker> _logger;
        public readonly WorkerOptions _options;

        //ToDo: read from appsettings
        const string connectionString = "Endpoint=sb://siai-rapidplantsetup.servicebus.windows.net/;SharedAccessKeyName=siairpsstaging;SharedAccessKey=FgaiN8d13J0dbQXGnxdPdK9UJMiULUCz0WQThC2oA7k=";
        const string queueName = "eainputbyapiqueue";
        static IQueueClient queueClient;

        private DateTime beginTime;
        private int delayingTime = 5000;
        private int delayingNext;
        public string atokenEA;
        public string atokenRPS;

        public MIDCodeCreatorRequest ComponentsRoot { get; private set; }

        public Worker(WorkerOptions options)
        {
            this._options = options;
        }

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            var starttime = DateTimeOffset.Now;
            _logger.LogInformation($"Start EARPSInterfaceService Async: {starttime}");

            return base.StartAsync(cancellationToken);
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                beginTime = DateTime.Now;

                var EAvalues = new Dictionary<string, string>{
                    { "username", "dba"},
                    { "password", "vibration"},
                };
                var json = System.Text.Json.JsonSerializer.Serialize(EAvalues);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
                string url = $"{ServiceSettings.apiEAuriBase}/saaidi/authenticate";
                HttpClient EAclient = new HttpClient();
                EAclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                EAclient.BaseAddress = new Uri(ServiceSettings.apiEAuriBase);
                using (HttpResponseMessage response = await EAclient.PostAsync(url, stringContent))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        APIAuthenticate apiauthenticate = await response.Content.ReadAsAsync<APIAuthenticate>();
                        atokenEA = apiauthenticate.data;
                    }
                    else
                    {
                        throw new Exception(response.ReasonPhrase);
                    }
                }

                APIEAHelper.InitializeApiEAClient(atokenEA);

                queueClient = new QueueClient(connectionString, queueName);
                var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
                {
                    MaxConcurrentCalls = 1,
                    AutoComplete = false
                };

                queueClient.RegisterMessageHandler(ProcessMessagesAsync, messageHandlerOptions);

                // ToDo: use cancellationtokensource??
                Console.ReadLine();

                //await queueClient.CloseAsync();

                TimeSpan ts = DateTime.Now - beginTime;
                //                int tsMinutes = ts.Minutes;
                if (ts.Minutes == 0)
                {
                    //delayingNext = delayingMax;
                    delayingNext = ServiceSettings.delayingMaxMinutes;

                }
                else
                {
                    //delayingNext = (ts.Minutes % delayingMaxMinutes) == 0 ? delayingMax : (Math.Abs(delayingMax - (ts.Minutes % delayingMax)));
                    delayingNext = (ts.Minutes % ServiceSettings.delayingMaxMinutes) == 0 ? ServiceSettings.delayingMaxMinutes : (Math.Abs(ServiceSettings.delayingMaxMinutes - (ts.Minutes % ServiceSettings.delayingMaxMinutes)));

                }

                delayingTime = delayingNext * (60 * 1000);

                await Task.Delay(3000, stoppingToken);
            }
        }

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public async Task ProcessMessagesAsync(Message message, CancellationToken token)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
        {

            //Reading the queue message
            var jsonString = Encoding.UTF8.GetString(message.Body);
            JobInfo jobinfo = System.Text.Json.JsonSerializer.Deserialize<JobInfo>(jsonString);
            EARPSInterfaceServiceLogger.AddLogEntry($"JobID: {jobinfo.JobID}\r\nTenantName: {jobinfo.CustomerName}\r\nTenantID: {jobinfo.TenantID}\r\nPlantName: {jobinfo.PlantName}\r\nPlantID: {jobinfo.PlantUUID}\r\nJobType: {jobinfo.Type}", null);
            DebuggingHelper.WriteJsonToFile(jsonString, jobinfo.CustomerName + @"\" + jobinfo.PlantName, $"messageQueueRead_{jobinfo.Type}");

            var RPSvalues = new Dictionary<string, string>{
                    //ToDo: get from appsettings
                    { "username", "rps"},
                    { "password", "rps123"},
                };
            var RPSjson = System.Text.Json.JsonSerializer.Serialize(RPSvalues);
            var RPSstringContent = new StringContent(RPSjson, Encoding.UTF8, "application/json");
            var RPSurl = $"{ServiceSettings.apiRPSuriBase}/rbac/user/login";

            //Get a connection to the RPS APIs
            HttpClient ApiRPSclient = new HttpClient();
            ApiRPSclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            ApiRPSclient.BaseAddress = new Uri(ServiceSettings.apiRPSuriBase);
            
            //Authenticate to RPS and get token
            using (HttpResponseMessage response = await ApiRPSclient.PostAsync(RPSurl, RPSstringContent))
            {
                if (response.IsSuccessStatusCode)
                {
                    APIAuthenticateRPS apiauthenticate = await response.Content.ReadAsAsync<APIAuthenticateRPS>();
                    atokenRPS = apiauthenticate.jwtToken;
                }
                else
                {
                    //EARPSInterfaceServiceLogger.AddLogEntry($"ReasonPhrase: {response.ReasonPhrase}\r\nTenantName: {jobinfo.CustomerName}\r\nTenantID: {jobinfo.TenantID}\r\nPlantName: {jobinfo.PlantName}\r\nPlantID: {jobinfo.PlantUUID}", null);
                    throw new Exception(response.ReasonPhrase);
                }
            }
            
            APIRPSHelper.InitializeApiRPSClient(atokenRPS, jobinfo.CustomerName);

            JobStatus jobstatus = new JobStatus();
            jobstatus.jobId = jobinfo.JobID;
            jobstatus.plantID = jobinfo.PlantUUID;
            jobstatus.exportStatus = "Job Failed. Unknown Error";
            try
            {
                await ProcessRequests.ProcessRequest(
                    jobinfo.JobID,
                    jobinfo.CustomerName,
                    jobinfo.TenantID,
                    jobinfo.PlantName,
                    jobinfo.PlantUUID,
                    jobinfo.Type,
                    jobstatus.exportStatus);
            }
            catch (Exception ex)
            {
                EARPSInterfaceServiceLogger.AddLogEntry($"ProcessRequest: {jobinfo.JobID}", ex);
            }
            EARPSInterfaceServiceLogger.AddLogEntry($"JobID: {jobinfo.JobID} \r\nTenantName: {jobinfo.CustomerName}\r\nTenantID: {jobinfo.TenantID}\r\nPlantName: {jobinfo.PlantName}\r\nPlantID: {jobinfo.PlantUUID}\r\nJobStatus: {jobstatus.exportStatus}",null);

            //Complete the read in queue
//            await queueClient.CompleteAsync(message.SystemProperties.LockToken);
        }

        private Task ExceptionReceivedHandler(ExceptionReceivedEventArgs arg)
        {
            // ToDo: log
            //            throw new NotImplementedException();
            Console.WriteLine($"Message handler exception: { arg.Exception }");
            return Task.CompletedTask;
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            //client.Dispose();
            var stoptime = DateTimeOffset.Now;
            EARPSInterfaceServiceLogger.AddLogEntry($"Stop EARPSInterfaceService Async: {stoptime}",null);
            return base.StopAsync(cancellationToken);
        }
    }
}
